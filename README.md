# Syndication client

## Development

Tooling is managed by [Create React App](https://create-react-app.dev/docs/getting-started).

```bash
$ npm install
$ npm start     # starts a development server
$ npm test      # run tests
$ npm build     # builds a production bundle
```

### Configuration

Configuration options are available on `window.env` and are created by `public/env.js` (gitignored). Sample configuration:

```json
{
  "NODE_ENV": "development",
  "APP_URL": "http://syndication.dev",
  "GQL_ROOT": "http://syndication.dev/graphql",
  "API_ROOT": "http://syndication.dev/api",
  "KEYCLOAK_URL": "http://52.17.30.165:8080",
  "KEYCLOAK_REALM": "syndication",
  "KEYCLOAK_CLIENT_ID": "clientid",
  "SSO_ENABLED": true
}
```

In **production** this file is built at runtime from the containers ENV variables using `public/env-template.js` as a template.

In **development** you need to create this file yourself (see [`public/env-template.js`](../tree/master/public/env-template.js) for details).

These get loaded into a `Config` object (source: [`src/Config.ts`](../tree/master/src/Config.ts)) which gets passed around the application.
