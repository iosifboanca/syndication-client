import React, { Fragment } from 'react';
import { withA11y } from '@storybook/addon-a11y';
import { withKnobs } from '@storybook/addon-knobs';
import { withNotes } from '@storybook/addon-notes';
import { ThemeProvider } from 'styled-components';
import { addDecorator, configure } from '@storybook/react';

import { theme, GlobalStyle } from '../src/ui';

addDecorator(withKnobs);
addDecorator(withA11y);
addDecorator(withNotes);
addDecorator(story => (
  <ThemeProvider theme={theme}>
    <Fragment>
      <GlobalStyle />
      {story()}
    </Fragment>
  </ThemeProvider>
));

configure(require.context('../src/ui', true, /\.stories\.tsx?$/), module);
