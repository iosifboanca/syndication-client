export interface AmbientConfig {
  APP_URL: string;
  GQL_ROOT: string;
  API_ROOT: string;
  KEYCLOAK_URL: string;
  KEYCLOAK_REALM: string;
  KEYCLOAK_CLIENT_ID: string;
}

export interface KeycloakConfig {
  url: string;
  realm: string;
  clientId: string;
}

export class Config {
  url: string;
  gqlUrl: string;
  apiUrl: string;
  keycloak: KeycloakConfig;
  networkThrottle: number;

  constructor(env?: AmbientConfig) {
    if (!env) {
      env = (window as any).env as AmbientConfig;
    }

    this.networkThrottle = 10 * 60 * 1000;
    this.url = env.APP_URL;
    this.apiUrl = env.API_ROOT;
    this.gqlUrl = env.GQL_ROOT;
    this.keycloak = {
      url: env.KEYCLOAK_URL,
      realm: env.KEYCLOAK_REALM,
      clientId: env.KEYCLOAK_CLIENT_ID,
    };
  }
}
