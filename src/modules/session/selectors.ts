import { createSelector } from 'reselect';
import { RootState } from 'typesafe-actions';
import { Session } from 'src/domain';

const root = (state: RootState) => state.session;

export const currentSession = createSelector(
  root,
  state => new Session(state),
);

export const isLoggedIn = createSelector(
  currentSession,
  session => session ? session.isLoggedIn : false
);

