import { isActionOf, RootEpic } from 'typesafe-actions';
import { mergeMapTo, filter, map, ignoreElements, tap } from 'rxjs/operators';

import { SessionRole } from 'src/domain';
import { actions as systemActions } from '../system';
import { login, setSession, fetchAppData, fetchAppDataSkipped } from './actions';

export const initEpic: RootEpic = (action$, _, { auth, graphql }) =>
  action$.pipe(
    filter(isActionOf(systemActions.init)),
    mergeMapTo(
      auth.state$.pipe(
        tap(authState => {
          if (authState.token) {
            graphql.setAuthorization(authState.token);
          }
        }),
        map(authState => setSession(authState.session)),
      ),
    ),
  );

const unauthorizedRoles = [SessionRole.NONE, SessionRole.ANONYMOUS];

export const fetchAppDataEpic: RootEpic = action$ =>
  action$.pipe(
    filter(isActionOf(setSession)),
    map(({ payload }) => {
      const isUnauthorized = payload.roles.some(r => unauthorizedRoles.includes(r));

      if (isUnauthorized) return fetchAppDataSkipped();
      return fetchAppData();
    }),
  );

export const loginEpic: RootEpic = (action$, _, { auth }) =>
  action$.pipe(
    filter(isActionOf(login)),
    tap(() => auth.login()),
    ignoreElements(),
  );
