import { createAction } from 'typesafe-actions';
import { Session } from 'src/domain';

export const setSession = createAction('session/SET_SESSION', (session: Session) => session.dto)();

export const login = createAction('session/LOGIN')();
export const fetchAppData = createAction('session/FETCH_APP_DATA')();
export const fetchAppDataSkipped = createAction('session/FETCH_APP_DATA_SKIPPED')();
