import React from 'react';
import { useSelector } from 'react-redux';
import { currentSession } from '../selectors';

export const UserInfo: React.FC = () => {
  const session = useSelector(currentSession);

  return (
    <div>
      Logged in as {session.name}. You are: {session.roles.join(',')}
    </div>
  );
}
