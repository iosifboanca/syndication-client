import React, { useCallback } from 'react';
import { Button, Flex, Title } from 'src/ui';
import { useSelector, useDispatch } from 'react-redux';

import { login } from '../actions';
import { isLoggedIn } from '../selectors';

export const LoginWidget: React.FC = () => {
  const loggedIn = useSelector(isLoggedIn);
  const dispatch = useDispatch();
  const dispatchLogin = useCallback(() => dispatch(login()), [dispatch]);

  if (loggedIn) {
    return null;
  }

  return (
    <Flex vertical height='100vh' justifyContent='center' alignItems='center'>
      <Title mb={4}>Hindawi Syndication</Title>
      <Button onClick={dispatchLogin} size='medium'>
        Login
      </Button>
    </Flex>
  );
};
