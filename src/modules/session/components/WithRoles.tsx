import React, { Fragment, useMemo } from 'react';
import { useSelector } from 'react-redux';

import { SessionRole } from 'src/domain';
import { currentSession } from '../selectors';

interface Props {
  roles: SessionRole[];
}

export const WithRoles: React.FC<Props> = ({ roles = [], children }) => {
  const { roles: sessionRoles = [] } = useSelector(currentSession);
  const hasRequiredRole = useMemo(() => roles.some(role => sessionRoles.includes(role)), [
    sessionRoles,
    roles,
  ]);

  if (hasRequiredRole) {
    return <Fragment>{children}</Fragment>;
  }

  return null;
};
