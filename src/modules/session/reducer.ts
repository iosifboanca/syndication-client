import { createReducer } from 'typesafe-actions';
import { emptySession } from 'src/domain';

import { setSession } from './actions';

const session = createReducer(emptySession.dto)
  .handleAction(setSession, (state, action) =>
    action.payload
  );

  const root = session;

export default root;
