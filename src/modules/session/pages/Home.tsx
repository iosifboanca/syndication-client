import React from 'react';
import styled from 'styled-components';

import { useNavigation } from 'src/hooks';
import { Icon, Title, Flex, Text, th } from 'src/ui';

export interface HomeProps {}

export const Home: React.FunctionComponent<HomeProps> = () => {
  const { goTo } = useNavigation();

  return (
    <Main vertical px={15} pb={15} flex={1}>
      <Text my={4} fontWeight='bold'>
        DASHBOARD
      </Text>
      <Container
        pt={2}
        flex={1}
        flexWrap='wrap'
        alignItems='flex-start'
        justifyContent='flex-start'
      >
        <Card vertical onClick={goTo('/indexers')}>
          <Icon size={20} name='indexers' color='colors.actionPrimary' />
          <Title mt={4} variant='primary'>
            Indexers
          </Title>
        </Card>
        <Card vertical onClick={goTo('/journals')}>
          <Icon size={20} name='journals' color='colors.actionPrimary' />
          <Title mt={4} variant='primary'>
            Journals
          </Title>
        </Card>
        <Card vertical onClick={goTo('/batches')}>
          <Icon size={20} name='batches' color='colors.actionPrimary' />
          <Title mt={4} variant='primary'>
            Batches
          </Title>
        </Card>
      </Container>
    </Main>
  );
};

// #region styles
const Card = styled(Flex)`
  align-items: center;
  border: 1px solid ${th('colors.furniture')};
  border-radius: ${th('gridUnit')};
  cursor: pointer;
  flex: 0 0 22%;
  justify-content: center;
  height: calc(${th('gridUnit')} * 60);
  margin: calc(${th('gridUnit')} * 4);

  &:hover {
    background-color: ${th('colors.background')};
  }
`;

const Container = styled(Flex)`
  box-shadow: 0 0 8px rgba(51, 51, 51, 0.11);
  border-radius: ${th('gridUnit')};
  background-color: ${th('colors.white')};
`;

const Main = styled(Flex)`
  background-color: ${th('colors.background')};
`;
// #endregion
