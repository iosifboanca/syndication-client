import React from 'react';
import { useSelector } from 'react-redux';

import { Flex, Title, Text } from 'src/ui';

import { currentSession } from '../selectors';

interface Props {}

export const NoRoles: React.FunctionComponent<Props> = () => {
  const { email } = useSelector(currentSession);

  return (
    <Flex vertical flex={1} justifyContent='center' alignItems='center' px={12}>
      <Title variant='primary' mb={2}>
        No roles configured
      </Title>
      <Text mb={2}>The current user ({email}) is not authorized to use Hindawi Syndication.</Text>
      <Text>
        Please send an email to <b>cezar.berea@hindawi.com</b> or <b>aurelia.apetrei@hindawi.com</b>
        .
      </Text>
    </Flex>
  );
};
