import gql from 'graphql-tag';
import { ASTNode } from 'graphql';

export const batchFragment = gql`
  fragment batchFragment on Batch {
    id
    indexerId
    publisherId
    type
    status
  }
`;

export const batchDashboardFragment: ASTNode = gql`
  fragment batchDashboardFragment on Batch {
    id
    type
    status
    parentId
    targetDate
    indexer {
      name
    }
    publisher {
      name
    }
  }
`;

export const syndicationTargetFragment: ASTNode = gql`
  fragment syndicationTargetFragment on SyndicationTarget {
    id
    status
    output {
      level
      timestamp
      message
    }
    metadataRaw
    startDate
    endDate
  }
`;

export const batchDetailsFragment: ASTNode = gql`
  fragment batchDetailsFragment on Batch {
    id
    status
    type
    targetDate
    indexer {
      name
    }
    publisher {
      name
    }
    syndicationTargets {
      ...syndicationTargetFragment
    }
    articles {
      id
      doi
      journalId
      customId
      publicationDate
    }
  }
  ${syndicationTargetFragment}
`;
