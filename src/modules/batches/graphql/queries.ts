import gql from 'graphql-tag';

import { BatchDTO } from 'src/domain';
import { batchDashboardFragment, batchDetailsFragment } from './fragments';

export const getDashboardBatches = gql`
  query batches($batchFilters: BatchFilters) {
    batches(batchFilters: $batchFilters) {
      hasMore
      items {
        ...batchDashboardFragment
      }
    }
  }
  ${batchDashboardFragment}
`;

export interface IGetDashboardBatches {
  batches: {
    hasMore: boolean;
    items: BatchDTO[];
  };
}

export const getSingleBatch = gql`
  query batch($batchId: String!) {
    batch(batchId: $batchId) {
      ...batchDetailsFragment
    }
  }
  ${batchDetailsFragment}
`;

export interface IGetSingleBatch {
  batch: BatchDTO;
}
