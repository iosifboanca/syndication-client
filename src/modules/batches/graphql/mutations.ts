import gql from 'graphql-tag';

import { BatchDTO, SyndicationTargetDTO } from 'src/domain';
import { batchDashboardFragment, syndicationTargetFragment } from './fragments';

export const commitBatchMutation = gql`
  mutation commitBatch($batchId: String!) {
    commitBatch(batchId: $batchId) {
      ...syndicationTargetFragment
    }
  }
  ${syndicationTargetFragment}
`;

export interface ICommitBatchMutation {
  commitBatch: SyndicationTargetDTO;
}

export const cloneBatchMutation = gql`
  mutation cloneBatch($batchId: String!, $targetDate: String!) {
    cloneBatch(batchId: $batchId, targetDate: $targetDate) {
      ...batchDashboardFragment
    }
  }
  ${batchDashboardFragment}
`;

export interface ICloneBatchMutation {
  cloneBatch: BatchDTO;
}

export const resyndicateArticlesMutation = gql`
  mutation resyndicateArticles(
    $targetDate: String!
    $syndicationTargetId: String!
    $articleIds: [String!]!
  ) {
    resyndicateArticles(
      targetDate: $targetDate
      syndicationTargetId: $syndicationTargetId
      articleIds: $articleIds
    ) {
      ...batchDashboardFragment
    }
  }
  ${batchDashboardFragment}
`;

export interface IResyndicateArticles {
  resyndicateArticles: BatchDTO;
}
