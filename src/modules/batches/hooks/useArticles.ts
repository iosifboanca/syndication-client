import { useCallback, useMemo, useState } from 'react';
import { get } from 'lodash';

import { useSearch } from 'src/hooks';
import { Article, ArticleStatus, SyndicationTarget, JournalRecord } from 'src/domain';

type SplitArticles = Record<ArticleStatus, Article[]>;
type SelectionObject = Record<string, boolean>;

interface FilterOption {
  id: string;
  value: ArticleStatus;
  label: string;
}

const createSelectionObject = (articles: Article[]): SelectionObject => {
  return articles.map(a => a.id).reduce((acc, el) => ({ ...acc, [el]: true }), {});
};

const articlesByStatus = (filterValue?: FilterOption, target?: SyndicationTarget) => (
  article: Article,
) => {
  if (!filterValue) return true;
  return article.status(target) === filterValue.value;
};

export function useArticles(
  articles: Article[],
  journals: JournalRecord,
  syndicationTarget?: SyndicationTarget,
) {
  const [inputValue, handleChange] = useSearch();

  const splitArticles = useMemo(() => {
    const parsedArticles: SplitArticles = {
      failed: [],
      resolved: [],
      success: [],
      unknown: [],
    };

    articles.forEach(a => {
      const status = a.status(syndicationTarget);
      parsedArticles[status].push(a);
    });

    return parsedArticles;
  }, [articles, syndicationTarget]);

  const [filterValue, setFilterOption] = useState(undefined);
  const [selectedArticles, updateSelectedArticles] = useState<SelectionObject>({});

  const allArticlesSelection = useMemo(() => createSelectionObject(articles), [articles]);
  const failedArticlesSelection = useMemo(() => createSelectionObject(splitArticles.failed), [
    splitArticles.failed,
  ]);

  const handleArticleSelect = useCallback(
    (article: Article) => () => {
      updateSelectedArticles(s => ({
        ...s,
        [article.id]: !s[article.id],
      }));
    },
    [],
  );

  const handleSelectAllFailed = useCallback(() => {
    updateSelectedArticles(failedArticlesSelection);
  }, [failedArticlesSelection]);

  const masterCheckboxChecked = useMemo(() => Object.values(selectedArticles).some(v => v), [
    selectedArticles,
  ]);

  const handleMasterCheckbox = useCallback(() => {
    if (masterCheckboxChecked) {
      updateSelectedArticles({});
    } else {
      updateSelectedArticles(allArticlesSelection);
    }
  }, [masterCheckboxChecked, allArticlesSelection]);

  const filteredArticles = useMemo(
    () =>
      articles.filter(articlesByStatus(filterValue, syndicationTarget)).filter(a => {
        const whereToSearch = `${a.id}${get(journals[a.journalId], 'code')}`;
        return whereToSearch.toLowerCase().includes(inputValue.trim());
      }),
    [articles, filterValue, syndicationTarget, journals, inputValue],
  );

  return {
    inputValue,
    handleChange,
    splitArticles,
    setFilterOption,
    filteredArticles,
    selectedArticles,
    handleArticleSelect,
    handleMasterCheckbox,
    handleSelectAllFailed,
    masterCheckboxChecked,
  };
}
