import { useCallback } from 'react';
import { useDispatch } from 'react-redux';

import { Batch } from 'src/domain';
import { cloneBatch, resyndicateArticles } from '../actions';

export interface CreateBatchFormValues {
  targetDate?: Date;
}

const createTargetDate = (d?: Date) => (d ? d.toISOString() : new Date().toISOString());

export const useCreateBatch = (
  batch?: Batch,
  syndicationTargetId?: string,
  selectedArticles: Record<string, boolean> = {},
) => {
  const dispatch = useDispatch();

  const handleClone = useCallback(
    (values: CreateBatchFormValues) => {
      if (batch) {
        dispatch(
          cloneBatch.request({
            batchId: batch.id,
            targetDate: createTargetDate(values.targetDate),
          }),
        );
      }
    },
    [dispatch, batch],
  );

  const handleResyndicate = useCallback(
    (values: CreateBatchFormValues) => {
      const articleIds = Object.entries(selectedArticles)
        .filter(([_, selected]) => selected)
        .map(([id]) => id);

      if (batch && syndicationTargetId) {
        dispatch(
          resyndicateArticles.request({
            articleIds,
            syndicationTargetId,
            targetDate: createTargetDate(values.targetDate),
          }),
        );
      }
    },
    [dispatch, batch, selectedArticles, syndicationTargetId],
  );

  return {
    handleClone,
    handleResyndicate,
  };
};
