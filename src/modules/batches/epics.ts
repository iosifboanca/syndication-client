import { from, of } from 'rxjs';
import { isActionOf, RootEpic } from 'typesafe-actions';
import { map, filter, mergeMap, catchError, throttleTime, withLatestFrom } from 'rxjs/operators';

import { actions as modalActions } from 'src/modules/modal';
import { actions as sessionActions } from 'src/modules/session';
import { actions as notificationActions } from 'src/modules/notifications';

import { filterValues } from './selectors';

import {
  getSingleBatch,
  cloneBatchMutation,
  commitBatchMutation,
  getDashboardBatches,
  resyndicateArticlesMutation,
  IGetSingleBatch,
  ICommitBatchMutation,
  IResyndicateArticles,
  IGetDashboardBatches,
  ICloneBatchMutation,
} from './graphql';

import {
  getBatches,
  cloneBatch,
  commitBatch,
  fetchBatches,
  getMoreBatches,
  resyndicateArticles,
  getSingleBatch as getSingleBatchActions,
} from './actions';

const ITEMS_PER_PAGE = 20;

export const fetchBatchesEpic: RootEpic = (action$, state$, { config }) =>
  action$.pipe(
    filter(isActionOf([fetchBatches, sessionActions.fetchAppData])),
    throttleTime(config.networkThrottle),
    withLatestFrom(state$),
    map(([_, state]) => getBatches.request(filterValues(state))),
  );

export const getBatchesEpic: RootEpic = (action$, _, { graphql }) =>
  action$.pipe(
    filter(isActionOf(getBatches.request)),
    mergeMap(action => {
      return from(
        graphql.request<IGetDashboardBatches>(getDashboardBatches, {
          batchFilters: { ...action.payload, limit: ITEMS_PER_PAGE },
        }),
      ).pipe(
        map(r => getBatches.success(r.batches)),
        catchError(error => of(getBatches.failure(error))),
      );
    }),
  );

export const getMoreBatchesEpic: RootEpic = (action$, state$, { graphql }) =>
  action$.pipe(
    filter(isActionOf(getMoreBatches.request)),
    withLatestFrom(state$),
    mergeMap(([action, state]) => {
      return from(
        graphql.request<IGetDashboardBatches>(getDashboardBatches, {
          batchFilters: { ...filterValues(state), limit: 3, offset: action.payload },
        }),
      ).pipe(
        map(r => getMoreBatches.success(r.batches)),
        catchError(error => of(getMoreBatches.failure(error))),
      );
    }),
  );

export const getSingleBatchEpic: RootEpic = (action$, _, { graphql }) =>
  action$.pipe(
    filter(isActionOf(getSingleBatchActions.request)),
    mergeMap(action =>
      from(
        graphql.request<IGetSingleBatch>(getSingleBatch, { batchId: action.payload }),
      ).pipe(
        map(r => getSingleBatchActions.success(r.batch)),
        catchError(error => of(getSingleBatchActions.failure(error))),
      ),
    ),
  );

export const resyndicateArticlesEpic: RootEpic = (action$, _, { graphql }) => {
  return action$.pipe(
    filter(isActionOf(resyndicateArticles.request)),
    mergeMap(action => {
      return from(
        graphql.request<IResyndicateArticles>(resyndicateArticlesMutation, { ...action.payload }),
      ).pipe(
        mergeMap(r => [
          resyndicateArticles.success(r.resyndicateArticles),
          modalActions.hideModal(),
          notificationActions.showSuccessToast({
            id: r.resyndicateArticles.id,
            message: 'Successfully created a new batch.',
            duration: 3000,
          }),
        ]),
        catchError(error => of(resyndicateArticles.failure(error))),
      );
    }),
  );
};

export const cloneBatchEpic: RootEpic = (action$, _, { graphql }) => {
  return action$.pipe(
    filter(isActionOf(cloneBatch.request)),
    mergeMap(action => {
      return from(
        graphql.request<ICloneBatchMutation>(cloneBatchMutation, { ...action.payload }),
      ).pipe(
        mergeMap(r => [
          cloneBatch.success(r.cloneBatch),
          modalActions.hideModal(),
          notificationActions.showSuccessToast({
            id: r.cloneBatch.id,
            message: 'Successfully cloned batch.',
            duration: 3000,
          }),
        ]),
        catchError(error => of(resyndicateArticles.failure(error))),
      );
    }),
  );
};

export const commitBatchEpic: RootEpic = (action$, _, { graphql }) =>
  action$.pipe(
    filter(isActionOf(commitBatch.request)),
    mergeMap(action =>
      from(
        graphql.request<ICommitBatchMutation>(commitBatchMutation, { batchId: action.payload }),
      ).pipe(
        mergeMap(r => [
          commitBatch.success(r.commitBatch),
          modalActions.hideModal(),
          notificationActions.showSuccessToast({
            id: r.commitBatch.id,
            message: 'Batch successfully retried.',
            duration: 3000,
          }),
        ]),
        catchError(error => of(commitBatch.failure(error))),
      ),
    ),
  );
