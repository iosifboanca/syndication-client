import React from 'react';
import styled from 'styled-components';

import { SyndicationTargetOutput, OutputEntryLevel } from 'src/domain';
import { Expander, Text, Flex, ScrollContainer, StatusText, DateParser, th } from 'src/ui';

interface Props {
  output: SyndicationTargetOutput[];
}

export const OutputLog: React.FunctionComponent<Props> = ({ output }) => {
  return (
    <Expander title='Output Log' flex={1}>
      <ScrollContainer px={4} pt={2} pb={0}>
        {output.map((entry, index) => (
          <Entry
            mb={2}
            px={2}
            py={1}
            vertical
            key={index}
            level={entry.level}
            justifyContent='flex-start'
          >
            <DateParser date={entry.timestamp} showMinutes>
              {date => (
                <StatusText
                  size='small'
                  status={entry.level}
                  infoStatuses={[OutputEntryLevel.warn]}
                  warningStatuses={[OutputEntryLevel.error]}
                >
                  {date}
                </StatusText>
              )}
            </DateParser>
            <Flex vertical>
              {entry.message.map(m => (
                <Text key={m}>{m}</Text>
              ))}
            </Flex>
          </Entry>
        ))}
      </ScrollContainer>
    </Expander>
  );
};

// #region styles

const Entry = styled(Flex)<{ level: OutputEntryLevel }>`
  background-color: ${th('colors.background')};
`;
// #endregion
