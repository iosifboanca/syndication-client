import React, { Fragment, useCallback, useState } from 'react';
import { capitalize } from 'lodash';
import styled from 'styled-components';
import { useSelector } from 'react-redux';

import { Batch, BatchType, BatchStatus } from 'src/domain';
import {
  Text,
  Flex,
  Label,
  THead,
  Table,
  Button,
  ActionLink,
  TextButton,
  DateParser,
  StatusText,
  BorderedRow,
  ScrollContainer,
  th,
} from 'src/ui';
import { Modal, useModal } from 'src/modules/modal';

import { useCreateBatch } from '../hooks';
import { CreateBatch } from './CreateBatchModal';
import { getCloneBatchStatus } from '../selectors';

interface Props {
  batches: Batch[];
  hasMore: boolean;
  loading: boolean;
  error: string | undefined;
  getMore(offset: number): void;
  goTo(path: string): () => void;
}

const TableHead = () => (
  <THead height={9} pl={4}>
    <Flex flex={2} justifyContent='flex-start'>
      <Label>ID</Label>
    </Flex>
    <Flex flex={3} justifyContent='flex-start'>
      <Label>Indexer</Label>
    </Flex>
    <Flex flex={3} justifyContent='flex-start'>
      <Label>Publisher</Label>
    </Flex>
    <Flex flex={3} justifyContent='flex-start'>
      <Label>Type</Label>
    </Flex>
    <Flex flex={2} justifyContent='flex-start'>
      <Label>Target Date</Label>
    </Flex>
    <Flex flex={1} justifyContent='flex-start'>
      <Label>Status</Label>
    </Flex>
    <Flex flex={1} pr={4}></Flex>
  </THead>
);

const modalKey = 'clone-batch';
export const BatchesTable: React.FC<Props> = ({ goTo, getMore, hasMore, batches, loading }) => {
  const { error, loading: cloneLoading } = useSelector(getCloneBatchStatus);
  const [batch, selectBatch] = useState<Batch>();
  const [showModal, hideModal] = useModal(modalKey);

  const { handleClone } = useCreateBatch(batch);

  const openClone = useCallback(
    (batchId: string) => (e: React.MouseEvent) => {
      e.stopPropagation();
      selectBatch(batches.find(b => b.id === batchId));
      showModal();
    },
    [batches, showModal, selectBatch],
  );

  const handleGoToParent = useCallback(
    (parentId: string) => (e: React.MouseEvent) => {
      e.stopPropagation();
      goTo(`/batches/${parentId}`)();
    },
    [goTo],
  );

  return (
    <Fragment>
      {batches.length > 0 ? (
        <Table vertical>
          <TableHead />
          <ScrollContainer>
            {batches.map(batch => (
              <BorderedRow
                pl={4}
                py={1}
                key={batch.id}
                title={batch.id}
                onClick={goTo(`/batches/${batch.id}`)}
              >
                <Flex flex={2} justifyContent='flex-start'>
                  <Text pr={2}>{batch.id.split('-')[0]}...</Text>
                </Flex>
                <Flex flex={3} justifyContent='flex-start'>
                  <Text pr={2}>{batch.indexer.name}</Text>
                </Flex>
                <Flex flex={3} justifyContent='flex-start'>
                  <Text pr={2}>{batch.publisher.name}</Text>
                </Flex>
                <Flex flex={3} justifyContent='flex-start'>
                  {batch.type === BatchType.replica ? (
                    <ActionLink onClick={handleGoToParent(batch.parentId)}>
                      Replica (go to parent)
                    </ActionLink>
                  ) : (
                    <Text>{capitalize(batch.type)}</Text>
                  )}
                </Flex>
                <Flex flex={2} justifyContent='flex-start'>
                  <DateParser date={batch.targetDate}>
                    {date => <Text wordBreak='break-all'>{date}</Text>}
                  </DateParser>
                </Flex>
                <Flex flex={1} justifyContent='flex-start'>
                  <StatusText
                    size='small'
                    status={batch.status}
                    warningStatuses={[BatchStatus.error]}
                    infoStatuses={[BatchStatus.inProgress, BatchStatus.draft, BatchStatus.pending]}
                  >
                    {batch.status.toUpperCase()}
                  </StatusText>
                </Flex>
                <Flex flex={1} justifyContent='flex-end' pr={4} onClick={openClone(batch.id)}>
                  <CloneBtn>Clone</CloneBtn>
                </Flex>
              </BorderedRow>
            ))}
            {hasMore && (
              <Flex my={4}>
                <GetMore
                  size='medium'
                  variant='primary'
                  loading={loading}
                  onClick={() => getMore(batches.length)}
                >
                  Get more batches
                </GetMore>
              </Flex>
            )}
          </ScrollContainer>
        </Table>
      ) : (
        <Flex>
          <Text>No batches for the selected filters.</Text>
        </Flex>
      )}
      <Modal modalKey={modalKey}>
        <CreateBatch
          batch={batch}
          error={error}
          title='Clone batch'
          onCancel={hideModal}
          loading={cloneLoading}
          handleSubmit={handleClone}
        />
      </Modal>
    </Fragment>
  );
};

// #region styles
const GetMore = styled(Button)`
  width: calc(${th('gridUnit')} * 80);
`;

const CloneBtn = styled(TextButton)`
  opacity: 0;

  ${BorderedRow}:hover & {
    opacity: 1;
  }
`;

// #endregion
