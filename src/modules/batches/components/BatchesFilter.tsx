import React, { useMemo, useCallback, useState } from 'react';
import { get, isEqual } from 'lodash';

import { Button, Dropdown, Flex, Label, DatePicker } from 'src/ui';
import {
  Indexer,
  BatchType,
  Publisher,
  BatchStatus,
  BatchFilters,
  batchTypeOptions,
  batchStatusOptions,
} from 'src/domain';

interface Props {
  indexers: Indexer[];
  publishers: Publisher[];
  initialValues: BatchFilters;
  setValues(values: BatchFilters): void;
}

export const BatchFilter: React.FunctionComponent<Props> = ({
  indexers,
  publishers,
  setValues,
  initialValues,
}) => {
  const [values, setFilter] = useState(initialValues);

  const indexersOptions = useMemo(
    () => [
      { id: 'none', value: undefined, name: 'None' },
      ...indexers.map(indexer => ({
        id: indexer.id,
        value: indexer.id,
        name: indexer.name,
      })),
    ],
    [indexers],
  );

  const publishersOptions = useMemo(
    () => [
      { id: 'none', value: undefined, name: 'None' },
      ...publishers.map(publisher => ({
        id: publisher.id,
        value: publisher.id,
        name: publisher.name,
      })),
    ],
    [publishers],
  );

  const onDropdownChange = useCallback(
    (key: string) => (v: BatchType | BatchStatus) => {
      const newValues = { ...values, [key]: get(v, 'value') };
      setFilter(newValues);
      setValues(newValues);
    },
    [values, setValues],
  );

  const onDatesChange = useCallback(
    (dates: Date | Date[]) => {
      if (Array.isArray(dates)) {
        const newValues = { ...values, startTargetDate: dates[0], endTargetDate: dates[1] };
        setFilter(newValues);
        setValues(newValues);
      }
    },
    [values, setValues],
  );

  const onClearFilters = useCallback(() => {
    const resetValues: BatchFilters = {
      indexerId: undefined,
      publisherId: undefined,
      type: undefined,
      status: undefined,
      startTargetDate: undefined,
      endTargetDate: undefined,
    };

    if (!isEqual(values, resetValues)) {
      setFilter(resetValues);
      setValues(resetValues);
    }
  }, [values, setValues]);

  return (
    <Flex mb={6} justifyContent='space-between'>
      <Flex vertical flex={2} mr={2}>
        <Label>Filter by Indexer</Label>
        <Dropdown
          optionLabel='name'
          clearOptionId='none'
          options={indexersOptions}
          onChange={onDropdownChange('indexerId')}
          value={indexersOptions.find(o => o.value === values.indexerId)}
        />
      </Flex>

      <Flex vertical flex={2} mr={2}>
        <Label>Filter by Publisher</Label>
        <Dropdown
          optionLabel='name'
          clearOptionId='none'
          options={publishersOptions}
          onChange={onDropdownChange('publisherId')}
          value={publishersOptions.find(o => o.value === values.publisherId)}
        />
      </Flex>

      <Flex vertical flex={2} mr={2}>
        <Label>Filter by date</Label>
        <DatePicker
          selectRange
          selectDate={onDatesChange}
          value={
            values.startTargetDate && values.endTargetDate
              ? [values.startTargetDate, values.endTargetDate]
              : undefined
          }
        />
      </Flex>

      <Flex vertical flex={1} mr={2}>
        <Label>Filter by Type</Label>
        <Dropdown
          optionLabel='name'
          clearOptionId='none'
          options={batchTypeOptions}
          onChange={onDropdownChange('type')}
          value={batchTypeOptions.find(o => o.value === values.type)}
        />
      </Flex>

      <Flex vertical flex={1} mr={2}>
        <Label>Filter by Status</Label>
        <Dropdown
          maxItems={7}
          optionLabel='name'
          clearOptionId='none'
          options={batchStatusOptions}
          onChange={onDropdownChange('status')}
          value={batchStatusOptions.find(o => o.value === values.status)}
        />
      </Flex>

      <Button size='medium' variant='secondary' alignSelf='flex-end' onClick={onClearFilters}>
        Clear
      </Button>
    </Flex>
  );
};
