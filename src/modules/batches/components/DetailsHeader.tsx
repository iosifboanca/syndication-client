import React, { Fragment } from 'react';
import { upperCase, capitalize } from 'lodash';

import { Batch, BatchStatus } from 'src/domain';
import { DateParser, Label, Flex, StatusText, Text, Title } from 'src/ui';

export interface DetailsHeaderProps {
  batch: Batch;
}

export const DetailsHeader: React.FunctionComponent<DetailsHeaderProps> = ({ batch }) => {
  return (
    <Fragment>
      <Title variant='small'>Batch details</Title>
      <Flex mt={4}>
        <Flex vertical flex={3}>
          <Label>Indexer</Label>
          <Text>{batch.indexer.name}</Text>
        </Flex>
        <Flex flex={2}>
          <Flex />
          <Flex ml={4} vertical>
            <Label>Status</Label>
            <StatusText
              status={batch.status}
              warningStatuses={[BatchStatus.error]}
              infoStatuses={[BatchStatus.inProgress, BatchStatus.draft, BatchStatus.pending]}
            >
              {upperCase(batch.status)}
            </StatusText>
          </Flex>
        </Flex>
      </Flex>

      <Flex mt={4}>
        <Flex vertical flex={3}>
          <Label>Publisher</Label>
          <Text>{batch.publisher.name}</Text>
        </Flex>

        <Flex flex={2}>
          <Flex vertical>
            <Label>Target</Label>
            <DateParser date={batch.targetDate}>{date => <Text>{date}</Text>}</DateParser>
          </Flex>

          <Flex ml={4} vertical>
            <Label>Type</Label>
            <Text>{capitalize(batch.type)}</Text>
          </Flex>
        </Flex>
      </Flex>
    </Fragment>
  );
};
