import React, { Fragment } from 'react';
import { get } from 'lodash';
import styled from 'styled-components';
import { useSelector } from 'react-redux';

import {
  Batch,
  Article,
  ArticleStatus,
  JournalRecord,
  SyndicationTarget,
  filterByStatusOptions,
} from 'src/domain';
import {
  Flex,
  Text,
  Icon,
  Table,
  Label,
  THead,
  Input,
  Button,
  Dropdown,
  Checkbox,
  Expander,
  StatusText,
  DateParser,
  BorderedRow,
  ScrollContainer,
  th,
} from 'src/ui';
import { Modal, useModal } from 'src/modules/modal';

import { CreateBatch } from './CreateBatchModal';
import { getResyndicationStatus } from '../selectors';
import { useCreateBatch, useArticles } from '../hooks';

type SplitArticles = Record<ArticleStatus, Article[]>;

const RightChildren: React.FC<SplitArticles> = ({ success, failed, resolved, unknown }) => {
  const failedArticles = failed.length;
  const successArticles = success.length;
  const resolvedArticles = resolved.length;
  const unknownArticles = unknown.length;

  return (
    <Flex justifyContent='flex-end'>
      {failedArticles > 0 && (
        <Fragment>
          <Label mr={1}>{failedArticles}</Label>
          <Text>Failed</Text>
        </Fragment>
      )}

      {resolvedArticles > 0 && (
        <Fragment>
          <Label mx={1}>{resolvedArticles}</Label>
          <Text>Resolved</Text>
        </Fragment>
      )}

      {unknownArticles > 0 && (
        <Fragment>
          <Label mx={1}>{unknownArticles}</Label>
          <Text>Unknown</Text>
        </Fragment>
      )}

      {successArticles > 0 && (
        <Fragment>
          <Label mx={1}>{successArticles}</Label>
          <Text>Succeeded</Text>
        </Fragment>
      )}
    </Flex>
  );
};

interface Props {
  batch: Batch;
  journals: JournalRecord;
  selectedTarget?: SyndicationTarget;
}

const modalKey = 'resyndicate-batch';
export const ArticlesTable: React.FunctionComponent<Props> = ({
  batch,
  journals,
  selectedTarget,
}) => {
  const { articles = [] } = batch;
  const [showModal, closeModal] = useModal(modalKey);
  const { error, loading } = useSelector(getResyndicationStatus);

  const {
    inputValue,
    handleChange,
    setFilterOption,
    filteredArticles,
    selectedArticles,
    handleArticleSelect,
    handleMasterCheckbox,
    handleSelectAllFailed,
    masterCheckboxChecked,
    splitArticles: {
      failed: failedArticles,
      unknown: unknownArticles,
      resolved: resolvedArticles,
      success: successfulArticles,
    },
  } = useArticles(batch.articles, journals, selectedTarget);

  const hasArticles = articles.length > 0;
  const hasErrors = failedArticles.length > 0;
  const { handleResyndicate } = useCreateBatch(batch, selectedTarget?.id, selectedArticles);

  return (
    <Fragment>
      <Expander
        mb={4}
        startExpanded
        title='Articles'
        flex={hasArticles ? 2 : 0}
        rightChildren={
          <RightChildren
            failed={failedArticles}
            unknown={unknownArticles}
            resolved={resolvedArticles}
            success={successfulArticles}
          />
        }
      >
        {hasArticles ? (
          <Flex p={4} height='100%' vertical>
            <Flex alignItems='flex-end' mb={2}>
              <Flex justifyContent='flex-start' alignItems='flex-end' flex={2} mr={1}>
                <Flex flex={2}>
                  <Input
                    value={inputValue}
                    onChange={handleChange}
                    placeholder='Search by ID, Journal Code'
                  />
                </Flex>
                <Flex vertical ml={2} flex={1}>
                  <Label>Filter by status</Label>
                  <Dropdown
                    clearOptionId='none'
                    onChange={setFilterOption}
                    options={filterByStatusOptions}
                  />
                </Flex>
              </Flex>
              <Flex justifyContent='flex-end' flex={1} ml={1}>
                {hasErrors && (
                  <Button variant='outline' size='medium' mr={4} onClick={handleSelectAllFailed}>
                    Select Failed
                  </Button>
                )}
                <Button size='medium' onClick={showModal} disabled={!masterCheckboxChecked}>
                  <Icon name='resend' color='white' mr={2} />
                  Re-Syndicate
                </Button>
              </Flex>
            </Flex>
            {filteredArticles.length > 0 ? (
              <Table vertical mt={2}>
                <TableHead
                  selectedTarget={selectedTarget}
                  checked={masterCheckboxChecked}
                  onCheckboxClick={handleMasterCheckbox}
                />
                <ScrollContainer>
                  {filteredArticles.map(article => {
                    const status: ArticleStatus = article.status(selectedTarget);

                    return (
                      <BorderedRow
                        pl={4}
                        py={1}
                        key={article.id}
                        title={article.tooltip(selectedTarget)}
                      >
                        {selectedTarget && (
                          <Flex flex={0} mr={1}>
                            <Checkbox
                              checked={selectedArticles[article.id]}
                              onChange={handleArticleSelect(article)}
                            />
                          </Flex>
                        )}
                        <Flex flex={1} justifyContent='flex-start'>
                          <Text>{article.customId}</Text>
                        </Flex>
                        <Flex flex={2} justifyContent='flex-start'>
                          <Text>{get(journals[article.journalId], 'code')}</Text>
                        </Flex>
                        <Flex flex={2} justifyContent='flex-start'>
                          <DateParser date={article.publicationDate}>
                            {date => <Text>{date}</Text>}
                          </DateParser>
                        </Flex>
                        <Flex flex={1} justifyContent='flex-end' pr={4}>
                          <StatusText
                            status={status}
                            infoStatuses={[ArticleStatus.RESOLVED]}
                            warningStatuses={[ArticleStatus.FAILED]}
                          >
                            {status.toUpperCase()}
                          </StatusText>
                        </Flex>
                      </BorderedRow>
                    );
                  })}
                </ScrollContainer>
              </Table>
            ) : (
              <NoArticlesContainer>
                <Text variant='secondary'>No articles.</Text>
              </NoArticlesContainer>
            )}
          </Flex>
        ) : (
          <Flex>
            <NoArticlesContainer m={2}>
              <Text variant='secondary'>No articles for the selected syndication target.</Text>
            </NoArticlesContainer>
          </Flex>
        )}
      </Expander>
      <Modal modalKey={modalKey}>
        <CreateBatch
          batch={batch}
          error={error}
          loading={loading}
          onCancel={closeModal}
          title='Resyndicate Articles'
          handleSubmit={handleResyndicate}
        />
      </Modal>
    </Fragment>
  );
};

const TableHead: React.FC<{
  checked: boolean;
  onCheckboxClick(e: any): void;
  selectedTarget?: SyndicationTarget;
}> = ({ onCheckboxClick, checked, selectedTarget }) => (
  <THead height={9} pl={4}>
    {selectedTarget && (
      <Flex flex={0} mr={1}>
        <Checkbox onChange={onCheckboxClick} checked={checked} />
      </Flex>
    )}
    <Flex flex={1} justifyContent='flex-start'>
      <Label>ID</Label>
    </Flex>
    <Flex flex={2} justifyContent='flex-start'>
      <Label>Journal Code</Label>
    </Flex>
    <Flex flex={2} justifyContent='flex-start'>
      <Label>Publication Date</Label>
    </Flex>
    <Flex flex={1} justifyContent='flex-end' pr={4}>
      <Label>Status</Label>
    </Flex>
  </THead>
);

const NoArticlesContainer = styled(Flex)`
  background-color: ${th('colors.background')};
  border-radius: ${th('gridUnit')};
  height: calc(${th('gridUnit')} * 20);
`;
