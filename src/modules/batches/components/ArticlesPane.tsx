import React from 'react';
import styled from 'styled-components';

import { Flex, th } from 'src/ui';
import { Batch, JournalRecord, SyndicationTarget } from 'src/domain';

import { OutputLog } from './OutputLog';
import { ArticlesTable } from './ArticlesTable';

interface Props {
  batch: Batch;
  journals: JournalRecord;
  selectedTarget?: SyndicationTarget;
}

export const ArticlesPane: React.FunctionComponent<Props> = ({
  batch,
  journals,
  selectedTarget,
}) => {
  return (
    <Main flex={1} ml={2} p={4} vertical>
      <ArticlesTable batch={batch} journals={journals} selectedTarget={selectedTarget} />

      {selectedTarget && selectedTarget.output ? (
        <OutputLog output={selectedTarget.output} />
      ) : null}
    </Main>
  );
};

// #region styles
const Main = styled(Flex)`
  background-color: ${th('colors.white')};
  border-radius: ${th('gridUnit')};
  box-shadow: 0 0 calc(${th('gridUnit')} * 2) rgba(51, 51, 51, 0.11);
  height: 100%;
`;
// #endregion
