import React, { Fragment } from 'react';
import { Formik } from 'formik';
import styled from 'styled-components';

import {
  Flex,
  Icon,
  Text,
  Title,
  Label,
  Button,
  FormField,
  DatePicker,
  th,
  isRequired,
} from 'src/ui';
import { Batch } from 'src/domain';

import { CreateBatchFormValues } from '../';

interface Props {
  title: string;
  batch?: Batch;
  error?: string;
  loading: boolean;
  onCancel(): void;
  handleSubmit(values: CreateBatchFormValues): void;
}

export const CreateBatch: React.FunctionComponent<Props> = ({
  batch,
  title,
  error,
  loading,
  onCancel,
  handleSubmit,
}) => {
  return (
    <Root alignItems='center' bg='white' vertical p={5}>
      <Flex justifyContent='flex-end'>
        <Icon name='close' onClick={onCancel} color='colors.textPrimary' />
      </Flex>
      <Title>{title}</Title>

      <Flex mt={4} px={4}>
        <Flex vertical>
          <Label>Indexer</Label>
          <Text>{batch?.indexer.name}</Text>
        </Flex>
        <Flex vertical>
          <Label>Publisher</Label>
          <Text>{batch?.publisher.name}</Text>
        </Flex>
      </Flex>

      <Formik onSubmit={handleSubmit} initialValues={{ targetDate: undefined }}>
        {({ values, setFieldValue, handleSubmit }) => (
          <Fragment>
            <FormField
              mt={4}
              px={4}
              required
              name='targetDate'
              validate={isRequired}
              label='Set Target Date'
              component={() => (
                <DateField
                  selected={values.targetDate}
                  onChange={date => setFieldValue('targetDate', date)}
                />
              )}
            />

            {error && (
              <Flex>
                <Text variant='warning'>{error}</Text>
              </Flex>
            )}

            <Flex mt={2} px={4}>
              <Button variant='outline' mr={3} onClick={onCancel}>
                CANCEL
              </Button>
              <Button
                ml={3}
                type='submit'
                variant='primary'
                loading={loading}
                onClick={handleSubmit}
              >
                CLONE
              </Button>
            </Flex>
          </Fragment>
        )}
      </Formik>
    </Root>
  );
};

// #region styles
const Root = styled(Flex)`
  border-radius: ${th('gridUnit')};
  width: calc(${th('gridUnit')} * 132);
`;
// #endregion
const DateField: React.FC<{
  onChange(date: any): void;
  selected: any;
}> = ({ onChange, selected }) => {
  return <DatePicker value={selected} selectDate={onChange} />;
};
