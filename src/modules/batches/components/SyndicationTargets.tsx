import React, { Fragment, useCallback } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { upperCase } from 'lodash';
import styled from 'styled-components';
import { formatDistanceStrict } from 'date-fns';

import {
  Flex,
  Text,
  Title,
  Table,
  THead,
  Label,
  Button,
  StatusText,
  DateParser,
  BorderedRow,
  ScrollContainer,
  ConfirmationModal,
  th,
} from 'src/ui';
import { Modal, useModal } from 'src/modules/modal';
import { SyndicationTarget, SyndicationTargetStatus } from 'src/domain';

import { commitBatch } from '../actions';
import { getCommitBatchStatus } from '../selectors';

const getDuration = (from?: Date, to?: Date) => {
  if (!from || !to) return '-';
  return formatDistanceStrict(from, to);
};

const infoStatuses = [
  SyndicationTargetStatus.pending,
  SyndicationTargetStatus.pickedUp,
  SyndicationTargetStatus.canceled,
];
const warningStatuses = [SyndicationTargetStatus.failed];

interface Props {
  batchId: string;
  selectedTarget?: SyndicationTarget;
  syndicationTargets: SyndicationTarget[];
  selectSyndicationTarget(target: SyndicationTarget): () => void;
}

const modalKey = 'retry-batch';
export const SyndicationTargets: React.FunctionComponent<Props> = ({
  batchId,
  selectedTarget,
  syndicationTargets = [],
  selectSyndicationTarget,
}) => {
  const dispatch = useDispatch();
  const [showModal, hideModal] = useModal(modalKey);
  const { error, loading } = useSelector(getCommitBatchStatus);

  const handleRetry = useCallback(() => {
    dispatch(commitBatch.request(batchId));
  }, [batchId, dispatch]);

  return (
    <Fragment>
      <Title variant='small' mt={6}>
        Syndication targets
      </Title>

      {selectedTarget ? (
        <Table vertical mt={1}>
          <TableHead />
          <ScrollContainer>
            {syndicationTargets.map((target, index) => (
              <TargetRow
                pl={4}
                py={1}
                key={`${target.id}${index}`}
                onClick={selectSyndicationTarget(target)}
                selected={target.id === selectedTarget?.id}
              >
                <Flex justifyContent='flex-start'>
                  <DateParser showMinutes date={target.startDate}>
                    {date => <Text>{date}</Text>}
                  </DateParser>
                </Flex>
                <Flex justifyContent='flex-start'>
                  <DateParser showMinutes date={target.endDate}>
                    {date => <Text>{date}</Text>}
                  </DateParser>
                </Flex>
                <Flex justifyContent='flex-start'>
                  <Text>{getDuration(target.endDate, target.startDate)}</Text>
                </Flex>
                <Flex justifyContent='flex-start'>
                  <StatusText
                    status={target.status}
                    infoStatuses={infoStatuses}
                    warningStatuses={warningStatuses}
                  >
                    {upperCase(target.status)}
                  </StatusText>
                </Flex>
              </TargetRow>
            ))}
          </ScrollContainer>
          <Flex justifyContent='flex-end' mt={4}>
            <Button
              size='medium'
              variant='secondary'
              onClick={showModal}
              disabled={syndicationTargets[0].status !== SyndicationTargetStatus.failed}
            >
              Retry Batch
            </Button>
          </Flex>
        </Table>
      ) : (
        <NoSyndicationContainer mt={1}>
          <Text variant='secondary'>No syndication targets for this batch yet.</Text>
        </NoSyndicationContainer>
      )}

      <Modal modalKey={modalKey}>
        <ConfirmationModal
          error={error}
          loading={loading}
          onCancel={hideModal}
          onAccept={handleRetry}
          acceptButtonLabel='RETRY'
          cancelButtonLabel='CANCEL'
          title='Are you sure you want to retry this batch?'
        />
      </Modal>
    </Fragment>
  );
};

const TableHead = () => (
  <THead height={9} pl={4}>
    <Flex justifyContent='flex-start'>
      <Label>Start Date</Label>
    </Flex>
    <Flex justifyContent='flex-start'>
      <Label>End Date</Label>
    </Flex>
    <Flex justifyContent='flex-start'>
      <Label>Duration</Label>
    </Flex>
    <Flex justifyContent='flex-start'>
      <Label>Status</Label>
    </Flex>
  </THead>
);

// #region styles
const NoSyndicationContainer = styled(Flex)`
  background-color: ${th('colors.background')};
  border-radius: ${th('gridUnit')};
  height: calc(${th('gridUnit')} * 20);
`;

const TargetRow = styled(BorderedRow)<{ selected: boolean }>`
  background-color: ${props => (props.selected ? th('colors.furniture') : 'inherit')};
`;
// #endregion
