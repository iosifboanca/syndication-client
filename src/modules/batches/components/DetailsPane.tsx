import React from 'react';
import styled from 'styled-components';

import { Flex, th } from 'src/ui';
import { Batch, SyndicationTarget } from 'src/domain';

import { DetailsHeader } from './DetailsHeader';
import { SyndicationTargets } from './SyndicationTargets';

export interface DetailsPaneProps {
  batch: Batch;
  selectedTarget: SyndicationTarget | undefined;
  selectSyndicationTarget(target: SyndicationTarget): () => void;
}

export const DetailsPane: React.FunctionComponent<DetailsPaneProps> = ({
  batch,
  selectedTarget,
  selectSyndicationTarget,
}) => {
  return (
    <Main flex={1} mr={2} p={4} vertical>
      <DetailsHeader batch={batch} />
      <SyndicationTargets
        batchId={batch.id}
        selectedTarget={selectedTarget}
        syndicationTargets={batch.syndicationTargets}
        selectSyndicationTarget={selectSyndicationTarget}
      />
    </Main>
  );
};

// #region styles
const Main = styled(Flex)`
  background-color: ${th('colors.white')};
  border-radius: ${th('gridUnit')};
  box-shadow: 0 0 calc(${th('gridUnit')} * 2) rgba(51, 51, 51, 0.11);
  height: 100%;
`;

// #endregion
