import { createSelector } from 'reselect';
import { RootState } from 'typesafe-actions';

import { Batch } from 'src/domain';

const root = (state: RootState) => state.batches;

export const singleBatch = createSelector(root, state =>
  state.batch ? new Batch(state.batch) : null,
);
export const allBatches = createSelector(root, ({ paginatedBatches }) => ({
  hasMore: paginatedBatches.hasMore,
  items: paginatedBatches.items.map(b => new Batch(b)),
}));

export const getBatchesStatus = createSelector(root, s => s.getBatchesStatus);
export const getCloneBatchStatus = createSelector(root, s => s.cloneBatchStatus);
export const getCommitBatchStatus = createSelector(root, s => s.commitBatchStatus);
export const getSingleBatchStatus = createSelector(root, s => s.getSingleBatchStatus);
export const getMoreBatchesStatus = createSelector(root, s => s.getMoreBatchesStatus);
export const getResyndicationStatus = createSelector(root, s => s.resyndicateArticlesStatus);

export const filterValues = createSelector(root, state => state.batchesFilter);
