import React, { useEffect, useCallback } from 'react';
import styled from 'styled-components';
import { useDispatch, useSelector } from 'react-redux';

import { useNavigation } from 'src/hooks';
import { BatchFilters, Batch } from 'src/domain';
import { Breadcrumb, Flex, LoadingComponent, th } from 'src/ui';
import { selectors as indexersSelectors } from 'src/modules/indexers';
import { selectors as publishersSelectors } from 'src/modules/publishers';

import { BatchFilter, BatchesTable } from '../components';
import { fetchBatches, getBatches, getMoreBatches, setBatchFilterValues } from '../actions';
import {
  allBatches,
  getBatchesStatus,
  getMoreBatchesStatus,
  filterValues as filterValuesSelector,
} from '../selectors';

interface Props {
  hasMore: boolean;
  batches: Batch[];
  getMoreError?: string;
  getMoreLoading: boolean;
  getMore(offset: number): void;
  goToDetails(batchId: string): () => void;
}

export const BatchesDashboard: React.FC = () => {
  const { goTo } = useNavigation();
  const dispatch = useDispatch();
  const filterValues = useSelector(filterValuesSelector);
  const indexers = useSelector(indexersSelectors.indexers);
  const { loading, error } = useSelector(getBatchesStatus);
  const { items: batches, hasMore } = useSelector(allBatches);
  const publishers = useSelector(publishersSelectors.publishers);
  const { loading: getMoreLoading, error: getMoreError } = useSelector(getMoreBatchesStatus);

  useEffect(() => {
    dispatch(fetchBatches(filterValues));
    // eslint-disable-next-line
  }, [dispatch]);

  useEffect(() => {
    dispatch(getBatches.request(filterValues));
  }, [dispatch, filterValues]);

  const getMore = useCallback(
    (offset: number) => {
      dispatch(getMoreBatches.request(offset));
    },
    [dispatch],
  );

  const setFilterValues = (values: BatchFilters) => {
    dispatch(setBatchFilterValues(values));
  };

  return (
    <Root>
      <Breadcrumb flex={0} goBack={goTo('/')} backLabel='BATCHES' mb={4} />

      <Main vertical flex={1} p={4}>
        <BatchFilter
          indexers={indexers}
          publishers={publishers}
          setValues={setFilterValues}
          initialValues={filterValues}
        />
        <LoadingComponent loading={loading} error={error}>
          {batches ? (
            <BatchesTable
              goTo={goTo}
              batches={batches}
              hasMore={hasMore}
              getMore={getMore}
              error={getMoreError}
              loading={getMoreLoading}
            />
          ) : null}
        </LoadingComponent>
      </Main>
    </Root>
  );
};

// #region styles
const Root = styled.div`
  background-color: ${th('colors.background')};
  display: flex;
  flex-direction: column;
  flex: 1;
  padding: calc(${th('gridUnit')} * 4) calc(${th('gridUnit')} * 15) calc(${th('gridUnit')} * 8)
    calc(${th('gridUnit')} * 15);
  width: 100%;
`;

const Main = styled(Flex)`
  background-color: ${th('colors.white')};
  border-radius: ${th('gridUnit')};
  box-shadow: 0 0 calc(${th('gridUnit')} * 2) rgba(51, 51, 51, 0.11);
`;
// #endregion
