import React, { useCallback, useEffect, useState } from 'react';
import styled from 'styled-components';
import { useParams } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';

import { useNavigation } from 'src/hooks';
import { Breadcrumb, Flex, LoadingComponent, th } from 'src/ui';
import { Batch, JournalRecord, SyndicationTarget } from 'src/domain';
import { selectors as journalsSelectors } from 'src/modules/journals';

import { getSingleBatch } from '../actions';
import { ArticlesPane, DetailsPane } from '../components';
import { singleBatch, getSingleBatchStatus } from '../selectors';

interface BatchDetailsProps {
  batch: Batch;
}

const Details: React.FunctionComponent<BatchDetailsProps> = ({ batch }) => {
  const journalsRecord: JournalRecord = useSelector(journalsSelectors.journalsObject);

  const [selectedTarget, selectTarget] = useState(0);

  const selectSyndicationTarget = useCallback(
    (target: SyndicationTarget) => () => {
      selectTarget(batch?.syndicationTargets.indexOf(target) || 0);
    },
    [batch],
  );

  return (
    <Flex flex={1}>
      <DetailsPane
        batch={batch}
        selectSyndicationTarget={selectSyndicationTarget}
        selectedTarget={batch.syndicationTargets[selectedTarget]}
      />
      <ArticlesPane
        batch={batch}
        journals={journalsRecord}
        selectedTarget={batch.syndicationTargets[selectedTarget]}
      />
    </Flex>
  );
};

export const BatchDetails: React.FC = () => {
  const dispatch = useDispatch();
  const { batchId } = useParams();
  const { goTo } = useNavigation();
  const batch = useSelector(singleBatch);
  const { loading, error } = useSelector(getSingleBatchStatus);

  useEffect(() => {
    dispatch(getSingleBatch.request(batchId as string));
  }, [batchId, dispatch]);

  return (
    <Root>
      <Breadcrumb
        mb={4}
        flex={0}
        label='BATCHES'
        backLabel='BACK'
        goBack={goTo('/batches')}
        entityName='BATCH DETAILS'
      />
      <LoadingComponent loading={loading} error={error}>
        {batch ? <Details batch={batch} /> : null}
      </LoadingComponent>
    </Root>
  );
};

// #region styles
const Root = styled.div`
  background-color: ${th('colors.background')};
  display: flex;
  flex-direction: column;
  flex: 1;
  padding: calc(${th('gridUnit')} * 4) calc(${th('gridUnit')} * 15) calc(${th('gridUnit')} * 8)
    calc(${th('gridUnit')} * 15);
  width: 100%;
`;
// #endregion
