import { combineReducers } from 'redux';
import { createReducer } from 'typesafe-actions';

import { operationStatusReducerCreator } from 'src/redux/utils';
import { BatchDTO, BatchFilters, PaginatedCollection } from 'src/domain';

import {
  cloneBatch,
  getBatches,
  commitBatch,
  getMoreBatches,
  getSingleBatch,
  resyndicateArticles,
  setBatchFilterValues,
} from './actions';

type TState = PaginatedCollection<BatchDTO>;
type CreateBatchAction =
  | ReturnType<typeof cloneBatch.success>
  | ReturnType<typeof resyndicateArticles.success>;

const batchesInitialState: PaginatedCollection<BatchDTO> = {
  items: [],
  hasMore: false,
};

const addBatch = (state: TState, action: CreateBatchAction): TState => ({
  ...state,
  items: [...state.items, action.payload],
});

const paginatedBatches = createReducer(batchesInitialState)
  .handleAction(getBatches.success, (_, action) => action.payload)
  .handleAction(getMoreBatches.success, (state, action) => ({
    items: [...state.items, ...action.payload.items],
    hasMore: action.payload.hasMore,
  }))
  .handleAction(resyndicateArticles.success, addBatch)
  .handleAction(cloneBatch.success, addBatch);

const cloneBatchStatus = operationStatusReducerCreator(cloneBatch);
const getBatchesStatus = operationStatusReducerCreator(getBatches);
const commitBatchStatus = operationStatusReducerCreator(commitBatch);
const getMoreBatchesStatus = operationStatusReducerCreator(getMoreBatches);
const resyndicateArticlesStatus = operationStatusReducerCreator(resyndicateArticles);

// batches filter
const initialBatchesFilter: BatchFilters = {
  limit: 20,
  offset: 0,
  type: undefined,
  status: undefined,
  indexerId: undefined,
  publisherId: undefined,
  endTargetDate: undefined,
  startTargetDate: undefined,
};

const batchesFilter = createReducer(initialBatchesFilter).handleAction(
  setBatchFilterValues,
  (_, action) => action.payload,
);

const batch = createReducer(null as BatchDTO | null)
  .handleAction(getSingleBatch.success, (_, action) => action.payload)
  .handleAction(commitBatch.success, (state, action) => {
    if (state === null) return null;

    return {
      ...state,
      syndicationTargets: [action.payload, ...state.syndicationTargets],
    };
  });

const getSingleBatchStatus = operationStatusReducerCreator(getSingleBatch);

export default combineReducers({
  batch,
  batchesFilter,
  paginatedBatches,
  getBatchesStatus,
  cloneBatchStatus,
  commitBatchStatus,
  getMoreBatchesStatus,
  getSingleBatchStatus,
  resyndicateArticlesStatus,
});
