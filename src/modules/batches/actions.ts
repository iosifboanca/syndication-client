import { createAction, createAsyncAction } from 'typesafe-actions';

import {
  BatchDTO,
  BatchType,
  BatchStatus,
  BatchFilters,
  CloneBatchInput,
  ResyndicationInput,
  PaginatedCollection,
  SyndicationTargetDTO,
} from 'src/domain';

export const getBatches = createAsyncAction(
  'batches/GET_BATCHES_REQUEST',
  'batches/GET_BATCHES_SUCCESS',
  'batches/GET_BATCHES_FAILURE',
)<BatchFilters, PaginatedCollection<BatchDTO>, Error>();

export const getMoreBatches = createAsyncAction(
  'batches/GET_MORE_BATCHES_REQUEST',
  'batches/GET_MORE_BATCHES_SUCCESS',
  'batches/GET_MORE_BATCHES_FAILURE',
)<number, PaginatedCollection<BatchDTO>, Error>();

export const getSingleBatch = createAsyncAction(
  'batches/GET_SINGLE_BATCH_REQUEST',
  'batches/GET_SINGLE_BATCH_SUCCESS',
  'batches/GET_SINGLE_BATCH_FAILURE',
)<string, BatchDTO, Error>();

export const resyndicateArticles = createAsyncAction(
  'batches/RESYNDICATE_ARTICLES_REQUEST',
  'batches/RESYNDICATE_ARTICLES_SUCCESS',
  'batches/RESYNDICATE_ARTICLES_FAILURE',
)<ResyndicationInput, BatchDTO, Error>();

export const commitBatch = createAsyncAction(
  'batches/COMMIT_BATCH_REQUEST',
  'batches/COMMIT_BATCH_SUCCESS',
  'batches/COMMIT_BATCH_FAILURE',
)<string, SyndicationTargetDTO, Error>();

export const cloneBatch = createAsyncAction(
  'batches/CLONE_BATCH_REQUEST',
  'batches/CLONE_BATCH_SUCCESS',
  'batches/CLONE_BATCH_FAILURE',
)<CloneBatchInput, BatchDTO, Error>();

// batches filter
export const changeSearch = createAction('batches/CHANGE_SEARCH')<string>();
export const changeType = createAction('batches/CHANGE_TYPE')<BatchType | undefined>();
export const changeStatus = createAction('batches/CHANGE_TYPE')<BatchStatus | undefined>();

export const setBatchFilterValues = createAction('batches/SET_FILTER_VALUES')<BatchFilters>();
export const fetchBatches = createAction('batches/FETCH_BATCHES')<BatchFilters>();
