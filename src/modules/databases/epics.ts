import { from, of } from 'rxjs';
import { isActionOf, RootEpic } from 'typesafe-actions';
import { map, filter, mergeMap, catchError, tap, ignoreElements } from 'rxjs/operators';

import { actions } from 'src/modules/modal';
import { actions as notificationActions } from 'src/modules/notifications';
import { createDatabase, getDatabases, updateDatabase, deleteDatabase } from './actions';
import {
  DATABASES,
  CREATE_DATABASE,
  UPDATE_DATABASE,
  deleteDatabaseMutation,
  IDatabases,
  IDeleteDatabase,
  ICreateDatabase,
  IUpdateDatabase,
} from './graphql';

export const createDatabaseEpic: RootEpic = (action$, _, { graphql }) =>
  action$.pipe(
    filter(isActionOf(createDatabase.request)),
    mergeMap(action =>
      from(
        graphql.request<ICreateDatabase>(CREATE_DATABASE, { data: action.payload }),
      ).pipe(
        mergeMap(r => [createDatabase.success(r.createDatabase), actions.hideModal()]),
        catchError(error => of(createDatabase.failure(error))),
      ),
    ),
  );

export const getDatabasesEpic: RootEpic = (action$, _, { graphql }) =>
  action$.pipe(
    filter(isActionOf(getDatabases.request)),
    mergeMap(() =>
      from(graphql.request<IDatabases>(DATABASES)).pipe(
        map(r => getDatabases.success(r.databases)),
        catchError(error => of(getDatabases.failure(error))),
      ),
    ),
  );

export const updateDatabaseEpic: RootEpic = (action$, _, { graphql }) =>
  action$.pipe(
    filter(isActionOf(updateDatabase.request)),
    mergeMap(action => {
      const { id, ...data } = action.payload;
      return from(
        graphql.request<IUpdateDatabase>(UPDATE_DATABASE, { id, data }),
      ).pipe(
        mergeMap(r => [
          updateDatabase.success(r.updateDatabase),
          actions.hideModal(),
          notificationActions.showSuccessToast({
            id: r.updateDatabase.id as string,
            message: 'Successfuly updated the database.',
            duration: 3000,
          }),
        ]),
        catchError(error => of(updateDatabase.failure(error))),
      );
    }),
  );

export const deleteDatabaseEpic: RootEpic = (action$, _, { graphql }) => {
  return action$.pipe(
    filter(isActionOf(deleteDatabase.request)),
    mergeMap(action => {
      return from(
        graphql.request<IDeleteDatabase>(deleteDatabaseMutation, { databaseId: action.payload }),
      ).pipe(
        mergeMap(r => [
          deleteDatabase.success(r.deleteDatabase),
          actions.hideModal(),
          notificationActions.showSuccessToast({
            id: action.payload,
            message: 'Successfuly deleted the database.',
            duration: 3000,
          }),
        ]),
        catchError(error => of(deleteDatabase.failure(error))),
      );
    }),
  );
};
