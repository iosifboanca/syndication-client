import { get } from 'lodash';
import { produce } from 'immer';
import { combineReducers } from 'redux';
import { createReducer } from 'typesafe-actions';

import { DatabaseDTO } from 'src/domain';
import { getIndexer, getIndexers } from 'src/modules/indexers/actions';
import { operationStatusReducerCreator } from 'src/redux/utils';

import { createDatabase, deleteDatabase, getDatabases, updateDatabase } from './actions';

const databasesInitialState: DatabaseDTO[] = [];

const databases = createReducer(databasesInitialState)
  .handleAction(updateDatabase.success, (state, action) =>
    produce(state, draftState => {
      const index = draftState.findIndex(db => db.id === action.payload.id);
      draftState[index] = action.payload;
    }),
  )
  .handleAction(getIndexers.success, (_, action) => action.payload.flatMap(i => i.databases || []))
  .handleAction(getDatabases.success, (_, action) => action.payload)
  .handleAction(createDatabase.success, (state, action) => [...state, action.payload])
  .handleAction(getIndexer.success, (_, action) => get(action.payload, 'databases', []))
  .handleAction(deleteDatabase.success, (state, action) =>
    state.filter(db => db.id !== action.payload),
  );

const getDatabasesStatus = operationStatusReducerCreator(getDatabases);
const createDatabaseStatus = operationStatusReducerCreator(createDatabase);
const updateDatabaseStatus = operationStatusReducerCreator(updateDatabase);
const deleteDatabaseStatus = operationStatusReducerCreator(deleteDatabase);

export default combineReducers({
  databases,
  getDatabasesStatus,
  createDatabaseStatus,
  updateDatabaseStatus,
  deleteDatabaseStatus,
});
