import React, { Fragment } from 'react';
import { Formik } from 'formik';
import styled from 'styled-components';

import { Database } from 'src/domain';
import {
  Icon,
  Flex,
  Text,
  Title,
  Button,
  Textarea,
  FormField,
  FormFieldProps,
  th,
  isRequired,
} from 'src/ui';

interface Props {
  error?: string;
  loading: boolean;
  onCancel(): void;
  onSubmit(values: Database): void;
}

const TextareaField = (props: FormFieldProps) => <Textarea height={20} {...props} />;

export const CreateDatabaseForm: React.FunctionComponent<Props> = ({
  error,
  loading,
  onCancel,
  onSubmit,
}) => {
  return (
    <Root alignSelf='stretch' vertical>
      <Flex justifyContent='space-between' mb={2}>
        <Title variant='primary'>Create database</Title>
        <Icon color='colors.textPrimary' name='close' onClick={onCancel} />
      </Flex>
      <Formik onSubmit={onSubmit} initialValues={new Database({})}>
        {({ handleSubmit, values }) => {
          return (
            <Fragment>
              <Flex vertical flex={1}>
                <FormField required name='name' label='Database name' validate={isRequired} />
                <FormField name='observations' label='Observations' component={TextareaField} />
                <FormField
                  name='acceptanceCriteria'
                  label='Acceptance Criteria'
                  component={TextareaField}
                />
              </Flex>
              {error && (
                <Flex justifyContent='flex-end' mb={2}>
                  <Text variant='warning'>{error}</Text>
                </Flex>
              )}
              <Flex justifyContent='flex-end' mb={1}>
                <Button variant='outline' size='medium' onClick={onCancel}>
                  CANCEL
                </Button>
                <Button
                  ml={6}
                  type='submit'
                  size='medium'
                  variant='primary'
                  loading={loading}
                  onClick={handleSubmit}
                >
                  CREATE
                </Button>
              </Flex>
            </Fragment>
          );
        }}
      </Formik>
    </Root>
  );
};

// #region styles
const Root = styled(Flex)`
  background-color: ${th('colors.white')};
  box-shadow: 0px 1px 10px rgba(36, 36, 36, 0.8);
  padding: calc(${th('gridUnit')} * 6);
  width: calc(${th('gridUnit')} * 130);
`;
// #endregion
