import React, { Fragment, useCallback } from 'react';
import { RootState } from 'typesafe-actions';
import { useDispatch, useSelector } from 'react-redux';

import { Modal, useModal } from 'src/modules/modal';
import { Database as DatabaseClass } from 'src/domain';
import { Button, Icon, Flex, Text, Title, ScrollContainer, TextButton } from 'src/ui';

import { CreateDatabaseForm, DatabaseCard } from './';
import { createDatabase as createDbActions } from '../actions';
import { createDatabaseStatus, makeIndexerDatabases } from '../selectors';

const indexerDatabasesSelector = makeIndexerDatabases();

const CREATE_DB_MODAL = 'create_database';
export const Databases: React.FC<{
  indexerId: string;
}> = ({ indexerId }) => {
  const dispatch = useDispatch();
  const [showModal, closeModal] = useModal(CREATE_DB_MODAL);
  const databases = useSelector((state: RootState) => indexerDatabasesSelector(state, indexerId));
  const { loading: createLoading, error: createError } = useSelector(createDatabaseStatus);

  const createDb = useCallback(
    (db: DatabaseClass) => {
      dispatch(createDbActions.request({ ...db, indexerId }));
    },
    [indexerId, dispatch],
  );

  return (
    <Fragment>
      <Flex justifyContent='space-between' mb={1}>
        <Title variant='small'>Databases</Title>
        <TextButton onClick={showModal}>
          <Icon mr={1} size={3} name='expand' />
          Create database
        </TextButton>
      </Flex>

      {!databases.length ? (
        <Flex alignItems='center' bg='background' justifyContent='center' height={20} vertical>
          <Text mb={2}>No databases available yet.</Text>
          <Button variant='outline' size='small' onClick={showModal}>
            Create database
          </Button>
        </Flex>
      ) : (
        <ScrollContainer>
          {databases.map(db => (
            <DatabaseCard key={db.id} database={db} indexerId={indexerId} />
          ))}
        </ScrollContainer>
      )}

      <Modal justifyContent='flex-start' modalKey={CREATE_DB_MODAL}>
        <CreateDatabaseForm
          onSubmit={createDb}
          error={createError}
          onCancel={closeModal}
          loading={createLoading}
        />
      </Modal>
    </Fragment>
  );
};
