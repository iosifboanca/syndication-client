import React, { Fragment, useCallback } from 'react';
import styled from 'styled-components';
import { useDispatch, useSelector } from 'react-redux';

import { Database } from 'src/domain';
import { Modal, useModal } from 'src/modules/modal';
import { ConfirmationModal, TextButton, Icon, Flex, Title, th } from 'src/ui';

import { EditDatabaseForm } from './EditDatabaseForm';
import { deleteDatabase, updateDatabase } from '../actions';
import { deleteDatabaseStatus, updateDatabaseStatus } from '../selectors';

interface Props {
  indexerId: string;
  database: Database;
}

export const DatabaseCard: React.FunctionComponent<Props> = ({ database, indexerId }) => {
  const dispatch = useDispatch();
  const updateModalKey = `update-db-${database.id}`;
  const deleteModalKey = `delete-db-${database.id}`;
  const [showUpdateModal, hideUpdateModal] = useModal(updateModalKey);
  const [showDeleteModal, hideDeleteModal] = useModal(deleteModalKey);

  const { loading: updateLoading, error: updateError } = useSelector(updateDatabaseStatus);
  const { loading: deleteLoading, error: deleteError } = useSelector(deleteDatabaseStatus);

  const updateDb = useCallback(
    (values: Database) => {
      dispatch(updateDatabase.request(values.dto));
    },
    [dispatch],
  );

  const handleDelete = useCallback(
    (databaseId: string) => () => {
      dispatch(deleteDatabase.request(databaseId));
    },
    [dispatch],
  );

  return (
    <Fragment>
      <Root justifyContent='space-between' p={2} mb={2}>
        <Title variant='small'>{database.name}</Title>

        <Flex justifyContent='flex-end'>
          <TextButton onClick={showUpdateModal}>
            <Icon size={3} name='edit' mr={1} />
            Edit
          </TextButton>
          <TextButton onClick={showDeleteModal}>
            <Icon size={3} name='delete' mr={1} />
            Delete
          </TextButton>
        </Flex>
      </Root>

      <Modal justifyContent='flex-start' modalKey={updateModalKey}>
        <EditDatabaseForm
          error={updateError}
          database={database}
          onSubmit={updateDb}
          loading={updateLoading}
          onCancel={hideUpdateModal}
        />
      </Modal>

      <Modal modalKey={deleteModalKey}>
        <ConfirmationModal
          error={deleteError}
          loading={deleteLoading}
          onCancel={hideDeleteModal}
          onAccept={handleDelete(database.id)}
          title={`Are you sure you want to delete the database ${database.name}?`}
        />
      </Modal>
    </Fragment>
  );
};

// #region styles
const Root = styled(Flex)`
  border-radius: ${th('gridUnit')};
  background-color: ${th('colors.background')};
`;
// #endregion
