import gql from 'graphql-tag';
import { DatabaseDTO } from 'src/domain';

import { databaseFragment } from './fragments';

export const DATABASES = gql`
  query {
    databases {
      ...databaseFragment
    }
  }
  ${databaseFragment}
`;

export interface IDatabases {
  databases: DatabaseDTO[];
}
