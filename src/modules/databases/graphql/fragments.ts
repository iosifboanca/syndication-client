import gql from 'graphql-tag';

export const databaseFragment = gql`
  fragment databaseFragment on Database {
    id
    indexerId
    name
    observations
    acceptanceCriteria
  }
`;
