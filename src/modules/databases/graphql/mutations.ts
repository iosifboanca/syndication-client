import gql from 'graphql-tag';

import { DatabaseDTO } from 'src/domain';
import { databaseFragment } from './fragments';

export const CREATE_DATABASE = gql`
  mutation createDatabase($data: DatabaseInput!) {
    createDatabase(data: $data) {
      ...databaseFragment
    }
  }
  ${databaseFragment}
`;

export interface ICreateDatabase {
  createDatabase: DatabaseDTO;
}

export const UPDATE_DATABASE = gql`
  mutation updateDatabase($id: String!, $data: DatabaseInput!) {
    updateDatabase(id: $id, data: $data) {
      ...databaseFragment
    }
  }
  ${databaseFragment}
`;

export interface IUpdateDatabase {
  updateDatabase: DatabaseDTO;
}

export const deleteDatabaseMutation = gql`
  mutation deleteDatabase($databaseId: String!) {
    deleteDatabase(databaseId: $databaseId)
  }
`;

export interface IDeleteDatabase {
  deleteDatabase: string;
}
