import { createAsyncAction } from 'typesafe-actions';

import { DatabaseDTO } from 'src/domain';

export const createDatabase = createAsyncAction(
  'databases/CREATE_DATABASE_REQUEST',
  'databases/CREATE_DATABASE_SUCCESS',
  'databases/CREATE_DATABASE_FAILURE',
)<DatabaseDTO, DatabaseDTO, Error>();

export const getDatabases = createAsyncAction(
  'databases/GET_DATABASES_REQUEST',
  'databases/GET_DATABASES_SUCCESS',
  'databases/GET_DATABASES_FAILURE',
)<void, DatabaseDTO[], Error>();

export const updateDatabase = createAsyncAction(
  'databases/UPDATE_DATABASE_REQUEST',
  'databases/UPDATE_DATABASE_SUCCESS',
  'databases/UPDATE_DATABASE_FAILURE',
)<DatabaseDTO, DatabaseDTO, Error>();

export const deleteDatabase = createAsyncAction(
  'databases/DELETE_DATABASE_REQUEST',
  'databases/DELETE_DATABASE_SUCCESS',
  'databases/DELETE_DATABASE_FAILURE',
)<string, string, Error>();
