import { createSelector } from 'reselect';
import { RootState } from 'typesafe-actions';

import { Database } from 'src/domain';

const root = (state: RootState) => state.databases;

export const makeIndexerDatabases = () => {
  return createSelector(indexerDatabases, databases => databases);
};

const indexerDatabases = (state: RootState, indexerId: string) =>
  state.databases.databases.filter(db => db.indexerId === indexerId).map(db => new Database(db));

export const databases = createSelector(root, ({ databases = [] }) =>
  databases.map(db => new Database(db)),
);

export const createDatabaseStatus = createSelector(root, state => state.createDatabaseStatus);
export const updateDatabaseStatus = createSelector(root, state => state.updateDatabaseStatus);
export const deleteDatabaseStatus = createSelector(root, state => state.deleteDatabaseStatus);
