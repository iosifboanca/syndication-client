import { useCallback } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { Indexer } from 'src/domain';

import * as filterActions from '../actions';
import { journalDB } from '../selectors';

export const useJournalDB = () => {
  const dispatch = useDispatch();
  const { searchInput, selectedIndexer } = useSelector(journalDB);

  const selectIndexer = useCallback(
    (indexer: Indexer | undefined) =>
      dispatch(filterActions.selectIndexer(indexer ? indexer.dto : undefined)),
    [dispatch],
  );

  const changeInput = useCallback(
    (e: any) => {
      dispatch(filterActions.changeDBSearchInput(e.target.value));
    },
    [dispatch],
  );

  return {
    changeInput,
    selectIndexer,
    searchInput,
    selectedIndexer,
  };
};
