import { createAction } from 'typesafe-actions';

import { IndexerDTO } from 'src/domain';

// journal filter
export const selectIndexer = createAction('filters/SELECT_INDEXER')<IndexerDTO | undefined>();
export const changeDBSearchInput = createAction('filters/CHANGE_DB_SEARCH_INPUT')<string>();
