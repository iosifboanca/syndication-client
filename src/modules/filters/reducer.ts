import { combineReducers } from 'redux';
import { createReducer } from 'typesafe-actions';

import { IndexerDTO } from 'src/domain';

import { selectIndexer, changeDBSearchInput } from './actions';

interface JournalDBFilters {
  searchInput: string;
  selectedIndexer: IndexerDTO | undefined;
}

const initialJournalDBFilters: JournalDBFilters = {
  searchInput: '',
  selectedIndexer: undefined,
};

const journalDBFilters = createReducer(initialJournalDBFilters)
  .handleAction(selectIndexer, (state, action) => ({ ...state, selectedIndexer: action.payload }))
  .handleAction(changeDBSearchInput, (state, action) => ({
    ...state,
    searchInput: action.payload,
  }));

export default combineReducers({
  journalDBFilters,
});
