import { createSelector } from 'reselect';
import { RootState } from 'typesafe-actions';

import { indexersDatabases } from 'src/modules/indexers/selectors';
import { journalDatabaseRecord } from 'src/modules/journals/selectors';
import { Database, JournalDatabaseRecord, JournalDatabaseStatus } from 'src/domain';

const root = (state: RootState) => state.filters;

export const journalDB = createSelector(root, filters => filters.journalDBFilters);

export const filteredDatabases = createSelector(
  journalDB,
  indexersDatabases,
  ({ searchInput, selectedIndexer }, databases) => {
    const selectedIndexerDBIds = selectedIndexer?.databases?.map(db => db.id);

    const filteredByName = databases.filter(db =>
      db.name.toLowerCase().includes(searchInput.toLowerCase()),
    );

    if (!selectedIndexerDBIds) {
      return filteredByName;
    }

    return filteredByName.filter(db => selectedIndexerDBIds?.includes(db.id));
  },
);

interface SplitDBs {
  availableDBs: Database[];
  submittedDBs: Database[];
  coveredDBs: Database[];
  journalDBs: JournalDatabaseRecord;
}

export const splitDatabases = createSelector(
  filteredDatabases,
  journalDatabaseRecord,
  (databases, journalDatabaseRecord) => {
    const result: SplitDBs = {
      availableDBs: [],
      submittedDBs: [],
      coveredDBs: [],
      journalDBs: journalDatabaseRecord,
    };

    const journalDBIds = Object.keys(journalDatabaseRecord);

    databases.forEach(db => {
      if (!journalDBIds.includes(db.id)) {
        result.availableDBs.push(db);
      } else {
        const jDB = journalDatabaseRecord[db.id];

        if (jDB.status === JournalDatabaseStatus.covered) {
          result.coveredDBs.push(db);
        } else {
          result.submittedDBs.push(db);
        }
      }
    });

    return result;
  },
);
