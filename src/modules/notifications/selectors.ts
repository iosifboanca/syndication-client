import { createSelector } from 'reselect';
import { RootState } from 'typesafe-actions';

const root = (state: RootState) => state.notifications;

export const toasts = createSelector(root, state => state.toasts);
