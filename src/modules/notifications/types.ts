export enum ToastStatus {
  success = 'success',
  warning = 'warning',
  error = 'error',
}

export interface Toast {
  id: string;
  message: string;
  duration?: number;
  status: ToastStatus;
}
