import { createAction } from 'typesafe-actions';

import { Toast, ToastStatus } from './types';

type ToastPayload = Omit<Toast, 'status'>;

export const showToast = createAction('notifications/SHOW_TOAST')<Toast>();
export const hideToast = createAction('notifications/HIDE_TOAST')<string>();

export const showSuccessToast = (payload: ToastPayload) =>
  showToast({ ...payload, status: ToastStatus.success });

export const showFailureTost = (payload: ToastPayload) =>
  showToast({ ...payload, status: ToastStatus.error });
