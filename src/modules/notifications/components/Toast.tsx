import React from 'react';
import styled from 'styled-components';

import { Flex, Text, Icon, th } from 'src/ui';

import { Toast as ToastType, ToastStatus } from '../types';

export interface ToastProps {
  toast: ToastType;
  onClose?(e: any): void;
}

const statusHash: any = {
  [ToastStatus.success]: {
    icon: 'save',
    color: 'colors.actionPrimary',
  },
  [ToastStatus.error]: {
    icon: 'warning',
    color: 'colors.warning',
  },
};

export const Toast: React.FunctionComponent<ToastProps> = ({ toast, onClose }) => {
  return (
    <Root mb={2} height={8} p={2} status={toast.status}>
      <StatusIcon name={statusIcon(toast.status)} mr={2} size={4} status={toast.status} />
      <ToastText>{toast.message}</ToastText>
      <CloseIcon name='close' color='colors.textSecondary' size={4} onClick={onClose} />
    </Root>
  );
};

// #region styles
interface StyleStatus {
  status: ToastStatus;
}

const statusIcon = (status: ToastStatus) => statusHash[status].icon;

const statusColor = (props: StyleStatus) => {
  return th(statusHash[props.status].color);
};

const Root = styled(Flex)<StyleStatus>`
  background-color: ${th('colors.white')};
  box-shadow: 0 0 4px ${th('colors.furniture')};
  border-left: 3px solid ${th('colors.actionPrimary')};

  border-left-color: ${statusColor};

  & > span {
    flex: 1;
  }
`;

const StatusIcon = styled(Icon)<StyleStatus>`
  fill: ${statusColor};
`;

const CloseIcon = styled(Icon)`
  cursor: pointer;
`;

const ToastText = styled(Text)`
  font-size: ${th('fontSizes.titleSmall')};
`;
// #endregion
