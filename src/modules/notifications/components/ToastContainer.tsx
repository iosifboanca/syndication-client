import React, { useCallback } from 'react';
import styled from 'styled-components';
import { useDispatch, useSelector } from 'react-redux';

import { th } from 'src/ui';

import { Toast } from './Toast';
import { hideToast as hideAction } from '../actions';
import { toasts as toastsSelector } from '../selectors';

export interface ToastContainerProps {}

export const ToastContainer: React.FunctionComponent<ToastContainerProps> = () => {
  const dispatch = useDispatch();
  const toasts = useSelector(toastsSelector);
  const hideToast = useCallback(
    (toastId: string) => () => {
      dispatch(hideAction(toastId));
    },
    [dispatch],
  );

  return toasts.length > 0 ? (
    <Root>
      {toasts.map(t => (
        <Toast key={t.id} toast={t} onClose={hideToast(t.id)} />
      ))}
    </Root>
  ) : null;
};

// #region styles
const Root = styled.div`
  position: absolute;
  top: calc(${th('gridUnit')} * 18);
  right: calc(${th('gridUnit')} * 4);
  width: calc(${th('gridUnit')} * 120);
`;

// #endregion
