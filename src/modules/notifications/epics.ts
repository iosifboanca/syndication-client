import { of } from 'rxjs';
import { isActionOf, RootEpic } from 'typesafe-actions';
import { filter, mapTo, delay, mergeMap } from 'rxjs/operators';

import { showToast, hideToast } from './actions';

export const showToastEpic: RootEpic = action$ =>
  action$.pipe(
    filter(isActionOf(showToast)),
    mergeMap(({ payload: { id, duration = 3000 } }) =>
      of(id).pipe(delay(duration), mapTo(hideToast(id))),
    ),
  );
