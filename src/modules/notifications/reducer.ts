import { combineReducers } from 'redux';
import { createReducer } from 'typesafe-actions';

import { Toast } from './types';
import { showToast, hideToast } from './actions';

const initialToasts: Toast[] = [];

const toasts = createReducer(initialToasts)
  .handleAction(showToast, (toasts, action) => [...toasts, action.payload])
  .handleAction(hideToast, (toasts, action) => toasts.filter(t => t.id !== action.payload));

export default combineReducers({
  toasts,
});
