import React from 'react';
import styled from 'styled-components';

import { Label, Flex, th } from 'src/ui';
import { Database, JournalDatabaseRecord, JournalDatabase } from 'src/domain';

import { EmptyDBState } from './';

export interface DBRowProps {
  db: Database;
  journalId: string;
  journalDb: JournalDatabase;
}

interface Props {
  title?: string;
  journalId: string;
  emptyLabel: string;
  databases: Database[];
  dbRow: React.FC<DBRowProps>;
  journalDBs: JournalDatabaseRecord;
}

export const DatabaseColumn: React.FunctionComponent<Props> = ({
  title,
  journalId,
  databases,
  emptyLabel,
  journalDBs,
  dbRow: DBRow,
}) => {
  return (
    <Root vertical px={4}>
      <Label mb={2}>{title}</Label>
      {databases.length > 0 ? (
        <ScrollContainer vertical>
          {databases.map(db => (
            <DBRow key={db.id} db={db} journalId={journalId} journalDb={journalDBs[db.id]} />
          ))}
        </ScrollContainer>
      ) : (
        <EmptyDBState message={emptyLabel} />
      )}
    </Root>
  );
};

// #region styles
const Root = styled(Flex)`
  border-right: 1px solid ${th('colors.furniture')};
  height: 100%;

  &:first-child {
    padding-left: 0;
  }

  &:last-child {
    border-right: none;
    padding-right: 0;
  }
`;

const ScrollContainer = styled(Flex)`
  overflow-y: auto;
`;
// #endregion
