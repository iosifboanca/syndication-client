import React from 'react';

import { useNavigation } from 'src/hooks';
import { Journal, Publisher } from 'src/domain';
import { Flex, Text, Label, Table, THead, StatusText, BorderedRow, ScrollContainer } from 'src/ui';

interface Props {
  journals: Journal[];
  publishers: Record<string, Publisher>;
}

export const JournalsTable: React.FC<Props> = ({ journals, publishers }) => {
  const { goTo } = useNavigation();

  return journals.length > 0 ? (
    <Table vertical>
      <TableHead />
      <ScrollContainer>
        {journals.map(j => (
          <TableRow
            key={j.id}
            journal={j}
            onClick={goTo(`/journals/${j.id}`)}
            publisher={publishers[j.publisherId]}
          />
        ))}
      </ScrollContainer>
    </Table>
  ) : (
    <Flex alignItems='center' bg='background' justifyContent='center' height={20} vertical>
      <Text>No journals found.</Text>
    </Flex>
  );
};

const TableHead = () => (
  <THead height={9} pl={4}>
    <Flex flex={2} justifyContent='flex-start'>
      <Label>Name</Label>
    </Flex>
    <Flex flex={1} justifyContent='flex-start'>
      <Label>Code</Label>
    </Flex>
    <Flex flex={1} justifyContent='flex-start'>
      <Label>Publisher</Label>
    </Flex>
    <Flex flex={1} justifyContent='flex-end' mr={6}>
      <Label>Accepting Submisisons</Label>
    </Flex>
  </THead>
);

const TableRow: React.FC<{
  onClick(): any;
  journal: Journal;
  publisher?: Publisher;
}> = ({ journal, publisher, onClick }) => {
  const status = journal.active ? 'ACTIVE' : 'INACTIVE';

  return (
    <BorderedRow height={8} pl={4} onClick={onClick}>
      <Flex flex={2} justifyContent='flex-start'>
        <Text>{journal.name}</Text>
      </Flex>
      <Flex flex={1} justifyContent='flex-start'>
        <Text>{journal.code}</Text>
      </Flex>
      <Flex flex={1} justifyContent='flex-start'>
        <Text>{publisher?.name}</Text>
      </Flex>
      <Flex flex={1} justifyContent='flex-end' mr={6}>
        <StatusText status={status} infoStatuses={['INACTIVE']}>
          {status}
        </StatusText>
      </Flex>
    </BorderedRow>
  );
};
