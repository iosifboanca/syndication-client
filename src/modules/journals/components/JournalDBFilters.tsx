import React, { useMemo } from 'react';

import { Indexer } from 'src/domain';
import { Flex, Label, Input, Dropdown } from 'src/ui';
import { useJournalDB } from 'src/modules/filters/hooks';

interface Props {
  indexers: Indexer[];
}

export const JournalDBFilters: React.FunctionComponent<Props> = ({ indexers }) => {
  const { changeInput, selectIndexer, searchInput, selectedIndexer } = useJournalDB();

  const indexerFilterOptions = useMemo(() => {
    return [{ id: 'none', value: undefined, name: 'None' }, ...indexers];
  }, [indexers]);

  return (
    <Flex mt={2} justifyContent='flex-start'>
      <Flex justifyContent='flex-start' width={75} mr={4} vertical>
        <Label>Databases</Label>
        <Input value={searchInput} onChange={changeInput} placeholder='Search by Name' />
      </Flex>

      <Flex vertical width={55}>
        <Label>Filter by Indexer</Label>
        <Dropdown
          optionLabel='name'
          clearOptionId='none'
          value={selectedIndexer}
          onChange={selectIndexer}
          options={indexerFilterOptions}
        />
      </Flex>
    </Flex>
  );
};
