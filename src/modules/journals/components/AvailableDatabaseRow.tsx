import React, { Fragment, useCallback } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { ConfirmationModal } from 'src/ui';
import { Modal, useModal } from 'src/modules/modal';

import { DBRowProps } from './';
import { SubmitButton } from './SubmitButton';
import { DatabaseExpander } from './DatabaseExpander';
import { submitDatabaseToJournal } from '../actions';
import { submitDbToJournalStatus } from '../selectors';

export const AvailableDatabaseRow: React.FunctionComponent<DBRowProps> = ({
  db,
  journalId,
  journalDb,
}) => {
  const modalKey = `submit-db-${db.id}`;
  const dispatch = useDispatch();
  const [showConnectModal, hideConnectModal] = useModal(modalKey);
  const { loading, error } = useSelector(submitDbToJournalStatus);

  const submitDatabase = useCallback(() => {
    dispatch(submitDatabaseToJournal.request({ journalId, databaseId: db.id }));
  }, [dispatch, journalId, db]);

  return (
    <Fragment>
      <DatabaseExpander
        db={db}
        journalDb={journalDb}
        rightChildren={<SubmitButton onClick={showConnectModal} />}
      />

      <Modal modalKey={modalKey}>
        <ConfirmationModal
          error={error}
          loading={loading}
          acceptButtonLabel='SUBMIT'
          cancelButtonLabel='CANCEL'
          onAccept={submitDatabase}
          onCancel={hideConnectModal}
          title='Submit Journal to Database'
          subtitle={`Are you sure you want to submit to Database ${db.name}?`}
        />
      </Modal>
    </Fragment>
  );
};
