import React, { useCallback } from 'react';

import { TextButton } from 'src/ui';

interface Props {
  onClick(): void;
}

export const SubmitButton: React.FC<Props> = ({ onClick }) => {
  const showModal = useCallback(
    (e: React.MouseEvent) => {
      e.stopPropagation();
      onClick();
    },
    [onClick],
  );

  return <TextButton onClick={showModal}>Submit</TextButton>;
};
