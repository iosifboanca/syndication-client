import React, { Fragment } from 'react';
import { Formik } from 'formik';

import { Database, JournalDatabase, JournalDatabaseStatus } from 'src/domain';
import {
  Icon,
  Flex,
  Text,
  Label,
  Title,
  Button,
  Dropdown,
  Textarea,
  FormField,
  DatePicker,
  FormFieldProps,
} from 'src/ui';

interface Props {
  loading: boolean;
  database: Database;
  error?: string;
  onCancel(): void;
  journalDatabase: JournalDatabase;
  onSubmit(values: any): void;
}

const journalStatuses = [
  {
    id: JournalDatabaseStatus.ceasedCoverage,
    value: JournalDatabaseStatus.ceasedCoverage,
    label: 'Ceased Coverage',
  },
  { id: JournalDatabaseStatus.covered, value: JournalDatabaseStatus.covered, label: 'Covered' },
  { id: JournalDatabaseStatus.rejected, value: JournalDatabaseStatus.rejected, label: 'Rejected' },
  {
    id: JournalDatabaseStatus.rejectedFinal,
    value: JournalDatabaseStatus.rejectedFinal,
    label: 'Rejected Final',
  },
  {
    id: JournalDatabaseStatus.reviewHold,
    value: JournalDatabaseStatus.reviewHold,
    label: 'Review Hold',
  },
  {
    id: JournalDatabaseStatus.underReview,
    value: JournalDatabaseStatus.underReview,
    label: 'Under Review',
  },
];

const activeStatuses = [
  { id: 'active', value: true, label: 'Active' },
  { id: 'inactive', value: false, label: 'Inactive' },
];

const TextareaField = (props: FormFieldProps) => <Textarea height={20} {...props} />;

const DateField: React.FC<{
  onChange(date: any): void;
  selected: any;
}> = ({ onChange, selected }) => {
  return <DatePicker value={selected} selectDate={onChange} />;
};

export const SetupJournalDatabaseModal: React.FC<Props> = ({
  error,
  loading,
  database,
  onCancel,
  onSubmit,
  journalDatabase,
}) => {
  return (
    <Flex alignSelf='stretch' bg='white' width={600} vertical p={6}>
      <Flex justifyContent='space-between' mb={4}>
        <Title variant='primary'>{database.name}</Title>
        <Icon name='close' onClick={onCancel} size={4} color='colors.textPrimary' />
      </Flex>
      <Formik initialValues={journalDatabase} onSubmit={onSubmit}>
        {({ handleSubmit, setFieldValue, values }) => {
          return (
            <Fragment>
              <Flex vertical flex={1}>
                {database.observations && (
                  <Flex vertical>
                    <Label mb={1}>Observations</Label>
                    <Text>{database.observations}</Text>
                  </Flex>
                )}

                {database.acceptanceCriteria && (
                  <Flex vertical mt={4}>
                    <Label mb={1}>Acceptance Criteria</Label>
                    <Text>{database.acceptanceCriteria}</Text>
                  </Flex>
                )}

                <FormField
                  mt={3}
                  name='status'
                  label='Status'
                  component={() => (
                    <Dropdown
                      options={journalStatuses}
                      value={journalStatuses.find(s => s.value === values.status)}
                      onChange={option => setFieldValue('status', option.value)}
                    />
                  )}
                />

                {values.status === JournalDatabaseStatus.covered && (
                  <Fragment>
                    <Flex>
                      <FormField
                        mr={2}
                        name='coverageDate'
                        label='Coverage Date'
                        component={() => (
                          <DateField
                            selected={values.coverageDate}
                            onChange={date => setFieldValue('coverageDate', date)}
                          />
                        )}
                      />
                      <FormField
                        ml={2}
                        name='acceptanceDate'
                        label='Acceptance Date'
                        component={() => (
                          <DateField
                            selected={values.acceptanceDate}
                            onChange={date => setFieldValue('acceptanceDate', date)}
                          />
                        )}
                      />
                    </Flex>

                    <FormField
                      mt={2}
                      name='acceptanceText'
                      label='Acceptance Letter'
                      component={TextareaField}
                    />
                  </Fragment>
                )}

                <FormField
                  name='active'
                  label='Active'
                  component={() => (
                    <Dropdown
                      options={activeStatuses}
                      value={activeStatuses.find(s => s.value === values.active)}
                      onChange={option => setFieldValue('active', option.value)}
                    />
                  )}
                />
              </Flex>
              {error && (
                <Flex justifyContent='flex-end' my={3}>
                  <Text variant='warning'>{error}</Text>
                </Flex>
              )}
              <Flex justifyContent='flex-end'>
                <Button size='medium' variant='secondary' onClick={onCancel} mr={2}>
                  CANCEL
                </Button>
                <Button type='submit' size='medium' onClick={handleSubmit} ml={2} loading={loading}>
                  SAVE
                </Button>
              </Flex>
            </Fragment>
          );
        }}
      </Formik>
    </Flex>
  );
};
