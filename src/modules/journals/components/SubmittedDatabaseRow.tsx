import React, { Fragment } from 'react';
import { useSelector } from 'react-redux';

import { ConfirmationModal } from 'src/ui';
import { Modal, useModal } from 'src/modules/modal';

import { DBRowProps } from './';
import { useJournalDatabases } from '../hooks';
import { RemoveOrEdit } from './RemoveOrEdit';
import { DatabaseExpander } from './DatabaseExpander';
import { SetupJournalDatabaseModal } from './SetupJournalDatabaseModal';
import { disconnectDbFromJournalStatus, updateJournalDbStatus } from '../selectors';

export const SubmittedDatabaseRow: React.FunctionComponent<DBRowProps> = ({
  db,
  journalId,
  journalDb,
}) => {
  const setupModalKey = `setup-${journalId}-${db.id}`;
  const removeModalKey = `remove-${journalId}-${db.id}`;
  const { loading: disconnectLoading, error: disconnectError } = useSelector(
    disconnectDbFromJournalStatus,
  );
  const { loading: updateLoading, error: updateError } = useSelector(updateJournalDbStatus);
  const [showSetupModal, hideSetupModal] = useModal(setupModalKey);
  const [showRemoveModal, hideRemoveModal] = useModal(removeModalKey);

  const { disconnectDatabase, updateConnectedDatabase } = useJournalDatabases(journalDb);

  return (
    <Fragment>
      <DatabaseExpander
        db={db}
        journalDb={journalDb}
        rightChildren={<RemoveOrEdit onSetup={showSetupModal} onRemove={showRemoveModal} />}
      />

      <Modal modalKey={removeModalKey}>
        <ConfirmationModal
          onAccept={disconnectDatabase}
          onCancel={hideRemoveModal}
          acceptButtonLabel='REMOVE'
          cancelButtonLabel='CANCEL'
          error={disconnectError}
          loading={disconnectLoading}
          title='Remove database from submitted'
          subtitle={`Are you sure you want to remove Database ${db.name}?`}
        />
      </Modal>
      <Modal modalKey={setupModalKey} justifyContent='flex-end'>
        <SetupJournalDatabaseModal
          database={db}
          error={updateError}
          loading={updateLoading}
          onCancel={hideSetupModal}
          journalDatabase={journalDb}
          onSubmit={updateConnectedDatabase}
        />
      </Modal>
    </Fragment>
  );
};
