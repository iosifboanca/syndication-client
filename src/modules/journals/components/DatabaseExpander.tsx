import React, { Fragment } from 'react';

import { Database, JournalDatabase, JournalDatabaseStatus } from 'src/domain';
import { Badge, Expander, Flex, DateParser, Label, Text } from 'src/ui';

interface DatabaseExpanderProps {
  db: Database;
  journalDb?: JournalDatabase;
  rightChildren?: React.ReactNode;
}

export const DatabaseExpander: React.FunctionComponent<DatabaseExpanderProps> = ({
  db,
  journalDb,
  rightChildren,
}) => {
  const activeStatus = journalDb?.active ? 'ENABLED' : 'DISABLED';

  return (
    <Expander mb={2} title={db.name} rightChildren={rightChildren}>
      <Flex m={2} vertical alignSelf='stretch' width='unset'>
        <Label>Acceptance Criteria</Label>
        <Text>{db.acceptanceCriteria || 'Not set'}</Text>
      </Flex>
      <Flex m={2} vertical alignSelf='stretch' width='unset'>
        <Label>Observations</Label>
        <Text>{db.observations || 'Not set'}</Text>
      </Flex>

      {journalDb && (
        <Fragment>
          <Flex m={2} alignSelf='stretch' width='unset'>
            <Flex vertical flex={1}>
              <Label>Status</Label>
              <Badge inverse>{journalDb.status ? journalDb.status.toUpperCase() : 'NOT SET'}</Badge>
            </Flex>
            <Flex vertical flex={2}>
              <Label>Active</Label>
              <Badge inverse>{activeStatus}</Badge>
            </Flex>
          </Flex>

          {journalDb.status === JournalDatabaseStatus.covered && (
            <Flex m={2} alignSelf='stretch' width='unset'>
              <Flex vertical flex={1}>
                <Label>Acceptance Date</Label>
                <DateParser date={journalDb.acceptanceDate}>
                  {date => <Text>{date}</Text>}
                </DateParser>
              </Flex>
              <Flex vertical flex={2}>
                <Label>Covered Date</Label>
                <DateParser date={journalDb.coverageDate}>{date => <Text>{date}</Text>}</DateParser>
              </Flex>
            </Flex>
          )}

          {journalDb.acceptanceText && (
            <Flex m={2} vertical alignSelf='stretch' width='unset'>
              <Label>Acceptance Letter</Label>
              <Text>{journalDb.acceptanceText}</Text>
            </Flex>
          )}
        </Fragment>
      )}
    </Expander>
  );
};
