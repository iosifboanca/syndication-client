import React from 'react';
import styled from 'styled-components';

import { Flex, Text, th } from 'src/ui';

interface Props {
  message: string;
}

export const EmptyDBState: React.FunctionComponent<Props> = ({ message }) => {
  return (
    <Root>
      <Text variant='secondary'>{message}</Text>
    </Root>
  );
};

// #region styles
const Root = styled(Flex)`
  background-color: ${th('colors.background')};
  border-radius: ${th('gridUnit')};
  height: calc(${th('gridUnit')} * 20);
`;
// #endregion
