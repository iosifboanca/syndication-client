import React, { Fragment, useCallback } from 'react';
import { Formik } from 'formik';
import styled from 'styled-components';
import { useDispatch, useSelector } from 'react-redux';

import { Text, Flex, Title, Label, Button, FormField, TextButton, StatusText, th } from 'src/ui';
import { Journal } from 'src/domain';

import { setJournalEditing, updateJournal } from '../actions';
import { getJournalEditing, updateJournalStatus } from '../selectors';

interface Props {
  journal: Journal;
  syndicated: boolean;
}

const CustomFields: React.FC<{ journal: Journal }> = ({
  journal: {
    printIssn,
    customFields: { legacyJournalCode, journalCodeRepec, journalSN },
  },
}) => (
  <Flex alignItems='flex-start'>
    <Flex alignItems='flex-start' vertical>
      <Label>ISSN (print)</Label>
      <Text>{printIssn || 'Not set'}</Text>
    </Flex>
    <Flex alignItems='flex-start' vertical>
      <Label>Legacy Code</Label>
      <Text>{legacyJournalCode || 'Not set'}</Text>
    </Flex>
    <Flex alignItems='flex-start' vertical>
      <Label>RePEc Code</Label>
      <Text>{journalCodeRepec || 'Not set'}</Text>
    </Flex>
    <Flex alignItems='flex-start' vertical>
      <Label>Serial Number</Label>
      <Text>{journalSN || 'Not set'}</Text>
    </Flex>
  </Flex>
);

const CustomFieldsForm: React.FC<{
  error?: string;
  loading: boolean;
  onCancel(): void;
  initialValues: Journal;
  onSubmit(values: Journal): void;
}> = ({ initialValues, onCancel, onSubmit, error, loading }) => {
  return (
    <Formik initialValues={initialValues} onSubmit={onSubmit}>
      {({ handleSubmit }) => (
        <Fragment>
          <Flex>
            <FormField name='printIssn' label='Print ISSN' mr={2} />
            <FormField name='customFields.legacyJournalCode' label='Legacy Code' mr={2} />
            <FormField name='customFields.journalCodeRepec' label='RePEc Code' mr={2} />
            <FormField name='customFields.journalSN' label='Serial Number' />
          </Flex>

          <Flex justifyContent='flex-end'>
            {error && (
              <Text mr={4} variant='warning'>
                {error}
              </Text>
            )}
            <Button size='small' onClick={onCancel} variant='secondary' mr={4}>
              CANCEL
            </Button>
            <Button size='small' onClick={handleSubmit} type='submit' loading={loading}>
              SAVE
            </Button>
          </Flex>
        </Fragment>
      )}
    </Formik>
  );
};

const Custom: React.FC<{
  journal: Journal;
}> = ({ journal }) => {
  const dispatch = useDispatch();
  const journalEditing = useSelector(getJournalEditing);
  const { error, loading } = useSelector(updateJournalStatus);

  const setEdit = useCallback(
    (editing: boolean) => () => {
      dispatch(setJournalEditing(editing));
    },
    [dispatch],
  );

  const handleSubmit = useCallback(
    (values: Journal) => {
      dispatch(updateJournal.request(values.dto));
    },
    [dispatch],
  );

  return (
    <Flex vertical flex={1} ml={4} alignItems='flex-end'>
      {!journalEditing && <TextButton onClick={setEdit(true)}>Configure</TextButton>}
      <CustomFieldsRoot justifyContent='space-around' vertical p={2}>
        {!journalEditing && <CustomFields journal={journal} />}
        {journalEditing && (
          <CustomFieldsForm
            error={error}
            loading={loading}
            initialValues={journal}
            onSubmit={handleSubmit}
            onCancel={setEdit(false)}
          />
        )}
      </CustomFieldsRoot>
    </Flex>
  );
};

const Details: React.FC<{ journal: Journal; syndicated: boolean }> = ({
  journal: { code, issn, active },
  syndicated,
}) => {
  const status = active ? 'ACTIVE' : 'INACTIVE';
  const syndicatedStatus = syndicated ? 'ACTIVE' : 'INACTIVE';

  return (
    <Flex justifyContent='flex-start' mt={4}>
      <Flex vertical mr={4} width='fit-content'>
        <Label>Code</Label>
        <Text>{code}</Text>
      </Flex>
      <Flex vertical mr={4} width='fit-content'>
        <Label>ISSN (online)</Label>
        <Text>{issn}</Text>
      </Flex>
      <Flex vertical mr={4} width='fit-content'>
        <Label>Syndicated</Label>
        <StatusText status={syndicatedStatus} warningStatuses={['INACTIVE']}>
          {syndicatedStatus}
        </StatusText>
      </Flex>
      <Flex vertical width='fit-content'>
        <Label>Accepting submissions</Label>
        <StatusText status={status} warningStatuses={['INACTIVE']}>
          {status}
        </StatusText>
      </Flex>
    </Flex>
  );
};

export const JournalHeader: React.FunctionComponent<Props> = ({ journal, syndicated }) => {
  return (
    <Flex alignItems='flex-start'>
      <Flex justifyContent='space-between' vertical flex={2}>
        <Title variant='primary' whiteSpace='normal'>
          {journal.name}
        </Title>
        <Details journal={journal} syndicated={syndicated} />
      </Flex>

      <Custom journal={journal} />
    </Flex>
  );
};

// #region styles
const CustomFieldsRoot = styled(Flex)`
  background-color: ${th('colors.background')};
  border-radius: ${th('gridUnit')};
`;
// #endregion
