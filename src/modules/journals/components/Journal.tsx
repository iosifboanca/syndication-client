import React from 'react';
import styled from 'styled-components';

import { Flex, th } from 'src/ui';
import { JournalDatabaseRecord, Journal as JournalClass, Indexer, Database } from 'src/domain';

import {
  JournalHeader,
  DatabaseColumn,
  JournalDBFilters,
  CoveredDatabaseRow,
  AvailableDatabaseRow,
  SubmittedDatabaseRow,
} from '../components';

interface Props {
  indexers: Indexer[];
  journal: JournalClass;
  coveredDBs: Database[];
  submittedDBs: Database[];
  availableDBs: Database[];
  journalDBs: JournalDatabaseRecord;
}

export const Journal: React.FunctionComponent<Props> = ({
  journal,
  indexers,
  coveredDBs,
  journalDBs,
  submittedDBs,
  availableDBs,
}) => {
  return (
    <Root vertical p={4}>
      <JournalHeader journal={journal} syndicated={coveredDBs.length > 0} />
      <JournalDBFilters indexers={indexers} />

      <Flex flex={1} mt={6} height={0}>
        <DatabaseColumn
          journalId={journal.id}
          journalDBs={journalDBs}
          databases={availableDBs}
          title='Available databases'
          dbRow={AvailableDatabaseRow}
          emptyLabel='No available databases.'
        />

        <DatabaseColumn
          journalId={journal.id}
          journalDBs={journalDBs}
          databases={submittedDBs}
          title='Submitted databases'
          dbRow={SubmittedDatabaseRow}
          emptyLabel='No submitted databases.'
        />

        <DatabaseColumn
          journalId={journal.id}
          journalDBs={journalDBs}
          databases={coveredDBs}
          title='Covered databases'
          dbRow={CoveredDatabaseRow}
          emptyLabel='No covered databases.'
        />
      </Flex>
    </Root>
  );
};

// #region styles
const Root = styled(Flex)`
  background-color: ${th('colors.white')};
  box-shadow: 0 0 calc(${th('gridUnit')} * 2) rgba(51, 51, 51, 0.11);
  border-radius: ${th('gridUnit')};
  flex: 1;
  overflow: hidden;
`;
// endregion
