import React, { useCallback } from 'react';

import { TextButton, Icon, Flex } from 'src/ui';

interface Props {
  onRemove(): void;
  onSetup(): void;
}

export const RemoveOrEdit: React.FunctionComponent<Props> = ({ onSetup, onRemove }) => {
  const remove = useCallback(
    e => {
      e.stopPropagation();
      onRemove();
    },
    [onRemove],
  );

  const update = useCallback(
    e => {
      e.stopPropagation();
      onSetup();
    },
    [onSetup],
  );
  return (
    <Flex flex={0}>
      <TextButton mr={3} onClick={remove}>
        Remove
      </TextButton>
      <TextButton onClick={update}>
        <Icon mr={1} name='edit' size={3} />
        Update
      </TextButton>
    </Flex>
  );
};
