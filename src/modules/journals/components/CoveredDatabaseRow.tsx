import React, { Fragment } from 'react';
import { useSelector } from 'react-redux';

import { ConfirmationModal } from 'src/ui';
import { Modal, useModal } from 'src/modules/modal';

import { useJournalDatabases } from '../hooks';
import { disconnectDbFromJournalStatus, updateJournalDbStatus } from '../selectors';

import { DBRowProps } from './';
import { RemoveOrEdit } from './RemoveOrEdit';
import { DatabaseExpander } from './DatabaseExpander';
import { SetupJournalDatabaseModal } from './SetupJournalDatabaseModal';

export const CoveredDatabaseRow: React.FunctionComponent<DBRowProps> = ({
  db,
  journalId,
  journalDb,
}) => {
  const setupModalKey = `update-${journalId}-${db.id}`;
  const disconnectModalKey = `disconnect-${journalId}-${db.id}`;
  const [showSetupModal, hideSetupModal] = useModal(setupModalKey);
  const { loading: updateLoading, error: updateError } = useSelector(updateJournalDbStatus);
  const [showDisconnectModal, hideDisconnectModal] = useModal(disconnectModalKey);
  const { loading: disconnectLoading, error: disconnectError } = useSelector(
    disconnectDbFromJournalStatus,
  );

  const { disconnectDatabase, updateSyndicatingDatabase } = useJournalDatabases(journalDb);

  return (
    <Fragment>
      <DatabaseExpander
        db={db}
        journalDb={journalDb}
        rightChildren={<RemoveOrEdit onSetup={showSetupModal} onRemove={showDisconnectModal} />}
      />
      <Modal modalKey={disconnectModalKey}>
        <ConfirmationModal
          onAccept={disconnectDatabase}
          onCancel={hideDisconnectModal}
          acceptButtonLabel='REMOVE'
          cancelButtonLabel='CANCEL'
          error={disconnectError}
          loading={disconnectLoading}
          title='Remove database from covered'
          subtitle={`Are you sure you want to remove Database ${db.name}?`}
        />
      </Modal>
      <Modal modalKey={setupModalKey} justifyContent='flex-end'>
        <SetupJournalDatabaseModal
          database={db}
          error={updateError}
          loading={updateLoading}
          onCancel={hideSetupModal}
          journalDatabase={journalDb}
          onSubmit={updateSyndicatingDatabase}
        />
      </Modal>
    </Fragment>
  );
};
