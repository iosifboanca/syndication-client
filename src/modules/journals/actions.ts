import { createAsyncAction, createAction } from 'typesafe-actions';

import { JournalDTO, JournalDatabaseDTO } from 'src/domain';

export const submitDatabaseToJournal = createAsyncAction(
  'journals/SUBMIT_DB_TO_JOURNAL_REQUEST',
  'journals/SUBMIT_DB_TO_JOURNAL_SUCCESS',
  'journals/SUBMIT_DB_TO_JOURNAL_FAILURE',
)<JournalDatabaseDTO, JournalDatabaseDTO, Error>();

export const disconnectDatabaseFromJournal = createAsyncAction(
  'journals/DISCONNECT_DB_FROM_JOURNAL_REQUEST',
  'journals/DISCONNECT_DB_FROM_JOURNAL_SUCCESS',
  'journals/DISCONNECT_DB_FROM_JOURNAL_FAILURE',
)<JournalDatabaseDTO, JournalDatabaseDTO, Error>();

export const updateJournalDatabase = createAsyncAction(
  'journals/UPDATE_JOURNAL_DATABASE_REQUEST',
  'journals/UPDATE_JOURNAL_DATABASE_SUCCESS',
  'journals/UPDATE_JOURNAL_DATABASE_FAILURE',
)<JournalDatabaseDTO, JournalDatabaseDTO, Error>();

export const getJournals = createAsyncAction(
  'journals/GET_JOURNALS_REQUEST',
  'journals/GET_JOURNALS_SUCCESS',
  'journals/GET_JOURNALS_FAILURE',
)<void, JournalDTO[], Error>();

export const getJournal = createAsyncAction(
  'journals/GET_JOURNAL_REQUEST',
  'journals/GET_JOURNAL_SUCCESS',
  'journals/GET_JOURNAL_FAILURE',
)<string, JournalDTO, Error>();

export const updateJournal = createAsyncAction(
  'journals/UPDATE_JOURNAL_REQUEST',
  'journals/UPDATE_JOURNAL_SUCCESS',
  'journals/UPDATE_JOURNAL_FAILURE',
)<JournalDTO, JournalDTO, Error>();

export const fetchJournals = createAction('journals/FETCH_JOURNALS')();

export const setJournalEditing = createAction('journals/SET_JOURNAL_EDITING')<boolean>();
