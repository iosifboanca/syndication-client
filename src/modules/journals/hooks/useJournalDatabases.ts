import { useCallback } from 'react';
import { useDispatch } from 'react-redux';

import { JournalDatabase, JournalDBActive, JournalDatabaseStatus } from 'src/domain';

import { updateJournalDatabase, disconnectDatabaseFromJournal } from '../actions';

export const useJournalDatabases = (journalDb: JournalDatabase) => {
  const dispatch = useDispatch();

  const disconnectDatabase = useCallback(() => {
    dispatch(disconnectDatabaseFromJournal.request(journalDb));
  }, [dispatch, journalDb]);

  const updateConnectedDatabase = useCallback(
    (values: { active: JournalDBActive; status: JournalDatabaseStatus }) => {
      dispatch(
        updateJournalDatabase.request({
          ...journalDb,
          status: values.status,
          active: values.active === JournalDBActive.active,
        }),
      );
    },
    [dispatch, journalDb],
  );

  const updateSyndicatingDatabase = useCallback(
    (values: JournalDatabase & { active: string }) => {
      dispatch(
        updateJournalDatabase.request({
          ...journalDb,
          ...values,
          status: values.status,
          active: values.active === JournalDBActive.active,
        }),
      );
    },
    [dispatch, journalDb],
  );

  return {
    disconnectDatabase,
    updateConnectedDatabase,
    updateSyndicatingDatabase,
  };
};
