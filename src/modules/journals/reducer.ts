import { createReducer } from 'typesafe-actions';
import { combineReducers } from 'redux';

import { JournalDTO } from 'src/domain';
import { operationStatusReducerCreator } from 'src/redux/utils';

import {
  getJournal,
  getJournals,
  updateJournal,
  setJournalEditing,
  updateJournalDatabase,
  submitDatabaseToJournal,
  disconnectDatabaseFromJournal,
} from './actions';

const initialJournalsState: JournalDTO[] = [];

const journals = createReducer(initialJournalsState).handleAction(
  getJournals.success,
  (_, action) => action.payload,
);

const journal = createReducer({} as JournalDTO)
  .handleAction(getJournal.success, (_, action) => action.payload)
  .handleAction(updateJournalDatabase.success, (state, action) => ({
    ...state,
    databasePairs: state.databasePairs.map(db =>
      db.journalId === action.payload.journalId && db.databaseId === action.payload.databaseId
        ? action.payload
        : db,
    ),
  }))
  .handleAction(submitDatabaseToJournal.success, (state, action) => ({
    ...state,
    databasePairs: [...state.databasePairs, action.payload],
  }))
  .handleAction(updateJournal.success, (state, action) => ({
    ...state,
    ...action.payload,
  }))
  .handleAction(disconnectDatabaseFromJournal.success, (state, action) => ({
    ...state,
    databasePairs: state.databasePairs.filter(db => db.databaseId !== action.payload.databaseId),
  }));

const journalEditing = createReducer(false).handleAction(
  setJournalEditing,
  (_, action) => action.payload,
);

const getJournalStatus = operationStatusReducerCreator(getJournal);
const getJournalsStatus = operationStatusReducerCreator(getJournals);
const updateJournalStatus = operationStatusReducerCreator(updateJournal);
const updateDbStatus = operationStatusReducerCreator(updateJournalDatabase);
const submitDbStatus = operationStatusReducerCreator(submitDatabaseToJournal);
const disconnectDbStatus = operationStatusReducerCreator(disconnectDatabaseFromJournal);

export default combineReducers({
  journal,
  journals,
  journalEditing,
  updateDbStatus,
  submitDbStatus,
  getJournalStatus,
  getJournalsStatus,
  disconnectDbStatus,
  updateJournalStatus,
});
