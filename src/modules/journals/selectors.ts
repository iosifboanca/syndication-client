import { createSelector } from 'reselect';
import { RootState } from 'typesafe-actions';

import { Journal, JournalDatabaseRecord } from 'src/domain';

const root = (state: RootState) => state.journals;

export const journals = createSelector(root, ({ journals = [] }) =>
  journals.map(j => new Journal(j)),
);
export const getJournalsStatus = createSelector(root, state => state.getJournalsStatus);

export const journal = createSelector(root, state => new Journal(state.journal));
export const getJournalEditing = createSelector(root, state => state.journalEditing);

export const getJournalStatus = createSelector(root, state => state.getJournalStatus);
export const submitDbToJournalStatus = createSelector(root, state => state.submitDbStatus);
export const disconnectDbFromJournalStatus = createSelector(
  root,
  state => state.disconnectDbStatus,
);
export const updateJournalDbStatus = createSelector(root, state => state.updateDbStatus);
export const updateJournalStatus = createSelector(root, state => state.updateJournalStatus);

export const journalsObject = createSelector(journals, journals => {
  return journals.reduce((acc, journal) => ({ ...acc, [journal.id]: journal }), {});
});

export const journalDatabaseRecord = createSelector(journal, journal => {
  return journal.databasePairs.reduce(
    (acc, journalDatabase) => ({
      ...acc,
      [journalDatabase.databaseId]: journalDatabase,
    }),
    {},
  ) as JournalDatabaseRecord;
});
