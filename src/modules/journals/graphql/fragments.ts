import gql from 'graphql-tag';

export const journalDatabase = gql`
  fragment journalDatabase on JournalDatabase {
    databaseId
    journalId
    acceptanceText
    active
    status
    acceptanceDate
    coverageDate
  }
`;

export const journalDetails = gql`
  fragment journalDetails on Journal {
    id
    publisherId
    name
    code
    issn
    active
    printIssn
    databasePairs {
      ...journalDatabase
    }
    customFields {
      legacyJournalCode
      journalCodeRepec
      journalSN
    }
  }
  ${journalDatabase}
`;

export const dashboardJournal = gql`
  fragment dashboardJournal on Journal {
    id
    name
    code
    active
    publisherId
  }
`;
