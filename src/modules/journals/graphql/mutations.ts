import gql from 'graphql-tag';
import { JournalDatabaseDTO, JournalDTO } from 'src/domain';

import { journalDatabase } from './fragments';

export const CONNECT_DT_TO_JOURNAL = gql`
  mutation connectDatabaseToJournal($journalId: String!, $databaseId: String!) {
    connectDatabaseToJournal(journalId: $journalId, databaseId: $databaseId) {
      ...journalDatabase
    }
  }
  ${journalDatabase}
`;

export interface IConnectDbToJournal {
  connectDatabaseToJournal: JournalDatabaseDTO;
}

export const DISCONNECT_DB_FROM_JOURNAL = gql`
  mutation disconnectDatabaseFromJournal($journalId: String!, $databaseId: String!) {
    disconnectDatabaseFromJournal(journalId: $journalId, databaseId: $databaseId) {
      ...journalDatabase
    }
  }
  ${journalDatabase}
`;

export interface IDisconnectDbFromJournal {
  disconnectDatabaseFromJournal: JournalDatabaseDTO;
}

export const UPDATE_JOURNAL_DB = gql`
  mutation updateJournalDatabase($data: JournalDatabaseInput!) {
    updateJournalDatabase(data: $data) {
      ...journalDatabase
    }
  }
  ${journalDatabase}
`;

export interface IUpdateJournalDb {
  updateJournalDatabase: JournalDatabaseDTO;
}

export const updateJournalMutation = gql`
  mutation updateJournal($data: JournalInput!) {
    updateJournal(data: $data) {
      id
      printIssn
      customFields {
        legacyJournalCode
        journalCodeRepec
        journalSN
      }
    }
  }
`;

export interface IUpdateJournalMutation {
  updateJournal: JournalDTO;
}
