import gql from 'graphql-tag';
import { JournalDTO } from 'src/domain';

import { journalDetails, dashboardJournal } from './fragments';

export const GET_JOURNALS = gql`
  query {
    journals {
      ...dashboardJournal
    }
  }
  ${dashboardJournal}
`;

export interface IGetJournals {
  journals: JournalDTO[];
}

export const GET_JOURNAL = gql`
  query journal($journalId: String!) {
    journal(journalId: $journalId) {
      ...journalDetails
    }
  }
  ${journalDetails}
`;

export interface IGetJournal {
  journal: JournalDTO;
}
