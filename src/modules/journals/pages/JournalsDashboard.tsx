import React, { useEffect, useMemo } from 'react';
import styled from 'styled-components';
import { useDispatch, useSelector } from 'react-redux';

import { Journal } from 'src/domain';
import { useSearch, useNavigation } from 'src/hooks';
import { publishersRecord } from 'src/modules/publishers/selectors';
import { Breadcrumb, Input, Flex, LoadingComponent, th } from 'src/ui';

import { JournalsTable } from '../';
import { fetchJournals } from '../actions';
import { journals as journalsSelector, getJournalsStatus } from '../selectors';

interface Props {
  journals: Journal[];
}

const filterJournal = (inputValue: string) => (journal: Journal) => {
  return journal.name.toLowerCase().includes(inputValue.trim());
};

const Details: React.FC<Props> = ({ journals }) => {
  const [inputValue, handleChange] = useSearch();
  const publishers = useSelector(publishersRecord);
  const filteredJournals = useMemo(() => journals.filter(filterJournal(inputValue)), [
    inputValue,
    journals,
  ]);

  return (
    <Main bg='white' flex={1} p={4} vertical>
      <Flex mb={4}>
        <Input width={75} value={inputValue} onChange={handleChange} placeholder='Search journal' />
      </Flex>
      <JournalsTable journals={filteredJournals} publishers={publishers} />
    </Main>
  );
};

export const JournalsDashboard: React.FC = () => {
  const dispatch = useDispatch();
  const { goTo } = useNavigation();
  const journals = useSelector(journalsSelector);

  const { loading, error } = useSelector(getJournalsStatus);

  useEffect(() => {
    dispatch(fetchJournals());
  }, [dispatch]);

  return (
    <Root>
      <Breadcrumb goBack={goTo('/')} backLabel='JOURNALS' flex={0} mb={4} />
      <LoadingComponent loading={loading} error={error}>
        {journals ? <Details journals={journals} /> : null}
      </LoadingComponent>
    </Root>
  );
};

// #region styles
const Main = styled(Flex)`
  border-radius: ${th('gridUnit')};
  box-shadow: 0 0 calc(${th('gridUnit')} * 2) rgba(51, 51, 51, 0.11);
`;

const Root = styled.div`
  background-color: ${th('colors.background')};
  display: flex;
  flex-direction: column;
  flex: 1;
  padding: calc(${th('gridUnit')} * 4) calc(${th('gridUnit')} * 15) calc(${th('gridUnit')} * 8)
    calc(${th('gridUnit')} * 15);
  width: 100%;
`;
// #endregion
