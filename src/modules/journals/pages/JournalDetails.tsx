import React, { useEffect } from 'react';
import styled from 'styled-components';
import { useParams } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';

import { useNavigation } from 'src/hooks';
import { Breadcrumb, LoadingComponent, th } from 'src/ui';
import { splitDatabases } from 'src/modules/filters/selectors';
import { selectors as indexerSelectors } from 'src/modules/indexers';

import { getJournal, setJournalEditing } from '../actions';
import { Journal as JournalComponent } from '../components';
import { journal as journalSelector, getJournalStatus } from '../selectors';

export const JournalDetails: React.FC = () => {
  const dispatch = useDispatch();
  const { goTo } = useNavigation();
  const { journalId } = useParams();
  const journal = useSelector(journalSelector);
  const indexers = useSelector(indexerSelectors.indexers);
  const { error, loading } = useSelector(getJournalStatus);
  const { availableDBs, submittedDBs, coveredDBs, journalDBs } = useSelector(splitDatabases);

  useEffect(() => {
    journalId && dispatch(getJournal.request(journalId));
    dispatch(setJournalEditing(false));
  }, [dispatch, journalId]);

  return (
    <Root>
      <Breadcrumb
        mb={4}
        flex={0}
        backLabel='BACK'
        label='JOURNALS'
        entityName={journal.name}
        goBack={goTo('/journals')}
      />
      <LoadingComponent loading={loading} error={error}>
        {journal ? (
          <JournalComponent
            journal={journal}
            indexers={indexers}
            journalDBs={journalDBs}
            submittedDBs={submittedDBs}
            availableDBs={availableDBs}
            coveredDBs={coveredDBs}
          />
        ) : null}
      </LoadingComponent>
    </Root>
  );
};

//#region styles
const Root = styled.div`
  align-self: stretch;
  background-color: ${th('colors.background')};
  display: flex;
  flex-direction: column;
  height: 0;
  flex: 1;
  justify-content: flex-start;
  padding: calc(${th('gridUnit')} * 15);
  padding-top: calc(${th('gridUnit')} * 4);
`;
//#endregion
