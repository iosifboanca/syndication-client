import { from, of } from 'rxjs';
import { isActionOf, RootEpic } from 'typesafe-actions';
import { throttleTime, filter, mergeMap, catchError, map } from 'rxjs/operators';

import { actions as modalActions } from 'src/modules/modal';
import { actions as sessionActions } from 'src/modules/session';
import { actions as notificationsActions } from 'src/modules/notifications';

import {
  getJournal,
  getJournals,
  fetchJournals,
  updateJournal,
  setJournalEditing,
  updateJournalDatabase,
  submitDatabaseToJournal,
  disconnectDatabaseFromJournal,
} from './actions';
import {
  GET_JOURNAL,
  IGetJournal,
  GET_JOURNALS,
  IGetJournals,
  UPDATE_JOURNAL_DB,
  IUpdateJournalDb,
  CONNECT_DT_TO_JOURNAL,
  IConnectDbToJournal,
  DISCONNECT_DB_FROM_JOURNAL,
  IDisconnectDbFromJournal,
  updateJournalMutation,
  IUpdateJournalMutation,
} from './graphql';

export const submitDatabaseToJournalEpic: RootEpic = (action$, _, { graphql }) =>
  action$.pipe(
    filter(isActionOf(submitDatabaseToJournal.request)),
    mergeMap(action => {
      const { journalId, databaseId } = action.payload;
      return from(
        graphql.request<IConnectDbToJournal>(CONNECT_DT_TO_JOURNAL, { journalId, databaseId }),
      ).pipe(
        mergeMap(r => [
          submitDatabaseToJournal.success(r.connectDatabaseToJournal),
          modalActions.hideModal(),
        ]),
        catchError(error => of(submitDatabaseToJournal.failure(error))),
      );
    }),
  );

export const disconnectDatabaseFromJournalEpic: RootEpic = (action$, _, { graphql }) =>
  action$.pipe(
    filter(isActionOf(disconnectDatabaseFromJournal.request)),
    mergeMap(action => {
      const { journalId, databaseId } = action.payload;
      return from(
        graphql.request<IDisconnectDbFromJournal>(DISCONNECT_DB_FROM_JOURNAL, {
          journalId,
          databaseId,
        }),
      ).pipe(
        mergeMap(r => [
          disconnectDatabaseFromJournal.success(r.disconnectDatabaseFromJournal),
          modalActions.hideModal(),
        ]),
        catchError(error => of(disconnectDatabaseFromJournal.failure(error))),
      );
    }),
  );

export const updateJournalDatabaseEpic: RootEpic = (action$, _, { graphql }) =>
  action$.pipe(
    filter(isActionOf(updateJournalDatabase.request)),
    mergeMap(action =>
      from(
        graphql.request<IUpdateJournalDb>(UPDATE_JOURNAL_DB, { data: action.payload }),
      ).pipe(
        mergeMap(r => [
          updateJournalDatabase.success(r.updateJournalDatabase),
          modalActions.hideModal(),
        ]),
        catchError(error => of(updateJournalDatabase.failure(error))),
      ),
    ),
  );

export const fetchJournalsEpic: RootEpic = (action$, _, { config }) =>
  action$.pipe(
    filter(isActionOf([fetchJournals, sessionActions.fetchAppData])),
    throttleTime(config.networkThrottle),
    map(() => getJournals.request()),
  );

export const getJournalsEpic: RootEpic = (action$, _, { graphql }) =>
  action$.pipe(
    filter(isActionOf(getJournals.request)),
    mergeMap(() =>
      from(graphql.request<IGetJournals>(GET_JOURNALS)).pipe(
        map(r => getJournals.success(r.journals)),
        catchError(error => of(getJournals.failure(error))),
      ),
    ),
  );

export const getJournalEpic: RootEpic = (action$, _, { graphql }) =>
  action$.pipe(
    filter(isActionOf(getJournal.request)),
    mergeMap(action =>
      from(
        graphql.request<IGetJournal>(GET_JOURNAL, { journalId: action.payload }),
      ).pipe(
        map(r => getJournal.success(r.journal)),
        catchError(error => of(getJournal.failure(error))),
      ),
    ),
  );

export const updateJournalEpic: RootEpic = (action$, _, { graphql }) =>
  action$.pipe(
    filter(isActionOf(updateJournal.request)),
    mergeMap(action => {
      const { id, customFields, printIssn } = action.payload;
      return from(
        graphql.request<IUpdateJournalMutation>(updateJournalMutation, {
          data: { id, customFields, printIssn },
        }),
      ).pipe(
        mergeMap(r => [
          updateJournal.success(r.updateJournal),
          setJournalEditing(false),
          notificationsActions.showSuccessToast({
            id: r.updateJournal.id,
            message: 'Journal updated',
          }),
        ]),
        catchError(error => of(updateJournal.failure(error))),
      );
    }),
  );
