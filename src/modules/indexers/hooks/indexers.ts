import { useCallback } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { Indexer } from 'src/domain';

import {
  editIndexerStatus,
  getIndexerDetailsEditing,
  getTransferOptionsEditing,
} from '../selectors';
import {
  editIndexer as editIndexerAction,
  createIndexer as createIndexerAction,
  setTransferOptionsEdit as setTransferEdit,
  setIndexerDetailsEdit as setIndexerEditAction,
} from '../actions';

export const useIndexer = () => {
  const dispatch = useDispatch();
  const indexerDetailsEditing = useSelector(getIndexerDetailsEditing);
  const transferOptionsEditing = useSelector(getTransferOptionsEditing);
  const { loading: editLoading, error: editError } = useSelector(editIndexerStatus);

  const handleEdit = useCallback(
    (indexer: Indexer) => {
      dispatch(editIndexerAction.request(indexer.dto));
    },
    [dispatch],
  );

  const createIndexer = useCallback(
    (indexer: Indexer) => {
      dispatch(createIndexerAction.request(indexer.dto));
    },
    [dispatch],
  );

  const setIndexerDetailsEdit = useCallback(
    (v: boolean) => () => {
      dispatch(setIndexerEditAction(v));
    },
    [dispatch],
  );

  const setTransferOptionsEdit = useCallback(
    (v: boolean) => () => {
      dispatch(setTransferEdit(v));
    },
    [dispatch],
  );

  return {
    editError,
    handleEdit,
    editLoading,
    createIndexer,
    indexerDetailsEditing,
    setIndexerDetailsEdit,
    transferOptionsEditing,
    setTransferOptionsEdit,
  };
};
