import { combineReducers } from 'redux';
import { createReducer } from 'typesafe-actions';

import { IndexerDTO } from 'src/domain';
import { operationStatusReducerCreator } from 'src/redux/utils';
import {
  getIndexer,
  getIndexers,
  editIndexer,
  createIndexer,
  setIndexerDetailsEdit,
  setTransferOptionsEdit,
  setIndexerActiveStatus,
} from './actions';

const indexersInitialState: IndexerDTO[] = [];

const indexers = createReducer(indexersInitialState)
  .handleAction(createIndexer.success, (state, action) => [action.payload, ...state])
  .handleAction(getIndexers.success, (_, action) => action.payload);

const indexer = createReducer({} as IndexerDTO)
  .handleAction(setIndexerActiveStatus.success, state => ({ ...state, active: !state.active }))
  .handleAction(getIndexer.success, (_, action) => action.payload)
  .handleAction(editIndexer.success, (_, action) => action.payload);

const indexerDetailsEditing = createReducer(false).handleAction(
  setIndexerDetailsEdit,
  (_, action) => action.payload,
);

const transferOptionsEditing = createReducer(false).handleAction(
  setTransferOptionsEdit,
  (_, action) => action.payload,
);

const createStatus = operationStatusReducerCreator(createIndexer);
const getIndexerStatus = operationStatusReducerCreator(getIndexer);
const editIndexerStatus = operationStatusReducerCreator(editIndexer);
const getIndexersStatus = operationStatusReducerCreator(getIndexers);
const indexerActiveStatus = operationStatusReducerCreator(setIndexerActiveStatus);

export default combineReducers({
  indexer,
  indexers,
  createStatus,
  getIndexerStatus,
  getIndexersStatus,
  editIndexerStatus,
  indexerActiveStatus,
  indexerDetailsEditing,
  transferOptionsEditing,
});
