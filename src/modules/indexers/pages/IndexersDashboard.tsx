import React, { Fragment, useMemo, useEffect } from 'react';
import styled from 'styled-components';
import { useDispatch, useSelector } from 'react-redux';

import { Indexer } from 'src/domain';
import { useNavigation, useSearch } from 'src/hooks';
import { Breadcrumb, LoadingComponent, Button, Flex, Text, Input, th } from 'src/ui';

import { fetchIndexers } from '../actions';
import { Modal, useModal } from '../../modal';
import { IndexersTable, AddIndexer } from '../components';
import { indexers as indexersSelector, indexersStatus } from '../selectors';

interface Props {
  indexers: Indexer[];
  showModal(): void;
}

const filterIndexer = (inputValue: string) => (indexer: Indexer) => {
  return indexer.name.toLowerCase().includes(inputValue.trim());
};

const Details: React.FC<Props> = ({ indexers, showModal }) => {
  const [inputValue, handleChange] = useSearch();
  const filteredIndexers = useMemo(() => indexers.filter(filterIndexer(inputValue)), [
    indexers,
    inputValue,
  ]);

  return (
    <Main flex={1} p={4} vertical bg='white'>
      <Flex mb={6} justifyContent='space-between'>
        <Input width={75} value={inputValue} onChange={handleChange} placeholder='Search by Name' />
        <Button onClick={showModal} size='medium'>
          Add Indexer
        </Button>
      </Flex>
      {indexers.length ? (
        <IndexersTable indexers={filteredIndexers} />
      ) : (
        <Flex alignItems='center' justifyContent='center' height={20} vertical bg='background'>
          <Text mb={2}>No indexers available yet.</Text>
          <Button variant='outline' size='small' onClick={showModal}>
            Create indexer
          </Button>
        </Flex>
      )}
    </Main>
  );
};

export const IndexersDashboard: React.FunctionComponent<Props> = () => {
  const { goTo } = useNavigation();
  const dispatch = useDispatch();
  const indexers = useSelector(indexersSelector);
  const [showModal, hideModal] = useModal('add-indexer');
  const { error, loading } = useSelector(indexersStatus);

  useEffect(() => {
    dispatch(fetchIndexers());
  }, [dispatch]);

  return (
    <Fragment>
      <Root>
        <Breadcrumb goBack={goTo('/')} backLabel='INDEXERS' mb={4} flex={0} />
        <LoadingComponent loading={loading} error={error}>
          {indexers ? <Details indexers={indexers} showModal={showModal} /> : null}
        </LoadingComponent>
      </Root>

      <Modal modalKey='add-indexer'>
        <AddIndexer closeModal={hideModal} />
      </Modal>
    </Fragment>
  );
};

// #region styles
const Main = styled(Flex)`
  border-radius: ${th('gridUnit')};
  box-shadow: 0 0 calc(${th('gridUnit')} * 2) rgba(51, 51, 51, 0.11);
`;

const Root = styled.div`
  background-color: ${th('colors.background')};
  display: flex;
  flex-direction: column;
  flex: 1;
  padding: calc(${th('gridUnit')} * 4) calc(${th('gridUnit')} * 15) calc(${th('gridUnit')} * 8)
    calc(${th('gridUnit')} * 15);
  width: 100%;
`;
// #endregion
