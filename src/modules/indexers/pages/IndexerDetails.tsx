import React, { Fragment, useEffect } from 'react';
import styled from 'styled-components';
import { useParams } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';

import { Indexer } from 'src/domain';
import { useNavigation } from 'src/hooks';
import { Modal, useModal } from 'src/modules/modal';
import { Icon, Flex, Label, Button, TextButton, Breadcrumb, LoadingComponent, th } from 'src/ui';

import { getIndexer } from '../actions';
import { indexer as indexerSelector, indexerStatus } from '../selectors';
import { ActivateIndexer, Indexer as IndexerComponent, IndexerPublisherCard } from '../components';

interface Props {
  indexer: Indexer;
}

interface IndexerHeader extends Props {
  showModal(): void;
}

const IndexerHeader: React.FC<IndexerHeader> = ({ indexer, showModal }) => {
  const { goTo } = useNavigation();

  return (
    <Flex justifyContent='space-between' mb={2}>
      <Breadcrumb
        backLabel='BACK'
        label='INDEXERS'
        entityName={indexer.name}
        goBack={goTo('/indexers')}
      />

      {!indexer.active && (
        <Flex flex={1}>
          <Icon size={5} name='warningFilled' color='colors.warning' mr={2} />
          <Label>This indexer is inactive.</Label>
          <TextButton onClick={showModal}>Activate</TextButton>
        </Flex>
      )}

      <Flex justifyContent='flex-end' mt={0} flex={1}>
        <Button variant='secondary' size='small' onClick={showModal}>
          {`${indexer.active ? 'Deactivate' : 'Activate'} Indexer`}
        </Button>
      </Flex>
    </Flex>
  );
};

const Details: React.FunctionComponent<Props> = ({ indexer }) => {
  const modalKey = `activate-indexer-${indexer.id}`;
  const [showModal, hideModal] = useModal(modalKey);

  return (
    <Fragment>
      <Root>
        <IndexerHeader indexer={indexer} showModal={showModal} />

        <Flex height='100%'>
          <IndexerComponent indexer={indexer} />
          <IndexerPublisherCard indexer={indexer} />
        </Flex>
      </Root>

      <Modal modalKey={modalKey}>
        <ActivateIndexer indexer={indexer} onCancel={hideModal} />
      </Modal>
    </Fragment>
  );
};

export const IndexerDetails: React.FC = () => {
  const dispatch = useDispatch();
  const { indexerId } = useParams();
  const indexer = useSelector(indexerSelector);
  const { error, loading } = useSelector(indexerStatus);

  useEffect(() => {
    dispatch(getIndexer.request(indexerId as string));
  }, [dispatch, indexerId]);

  return (
    <LoadingComponent loading={loading} error={error}>
      {indexer ? <Details indexer={indexer} /> : null}
    </LoadingComponent>
  );
};

//#region styles
const Root = styled.div`
  background-color: ${th('colors.background')};
  display: flex;
  flex-direction: column;
  flex: 1;
  padding: calc(${th('gridUnit')} * 4) calc(${th('gridUnit')} * 15) calc(${th('gridUnit')} * 8)
    calc(${th('gridUnit')} * 15);
  width: 100%;
`;
//#endregion
