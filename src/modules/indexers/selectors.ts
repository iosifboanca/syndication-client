import { RootState } from 'typesafe-actions';
import { createSelector } from 'reselect';

import { Indexer } from 'src/domain';

const root = (state: RootState) => state.indexers;

// create indexer selectors
export const createStatus = createSelector(root, state => state.createStatus);

// single indexer selectors
export const indexer = createSelector(root, state => new Indexer(state.indexer));
export const indexerStatus = createSelector(root, state => state.getIndexerStatus);

// indexers list selectors
export const indexers = createSelector(root, ({ indexers = [] }) =>
  indexers.map(i => new Indexer(i)),
);
export const indexersStatus = createSelector(root, state => state.getIndexersStatus);

// indexer status selectors
export const setIndexerActiveStatus = createSelector(root, state => state.indexerActiveStatus);
export const editIndexerStatus = createSelector(root, s => s.editIndexerStatus);

export const indexersDatabases = createSelector(indexers, indexers =>
  indexers.flatMap(i => i.databases),
);

// edit flags
export const getIndexerDetailsEditing = createSelector(root, s => s.indexerDetailsEditing);
export const getTransferOptionsEditing = createSelector(root, s => s.transferOptionsEditing);
