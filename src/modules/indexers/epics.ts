import { get } from 'lodash';
import { from, of } from 'rxjs';
import { isActionOf, RootEpic } from 'typesafe-actions';
import { throttleTime, filter, mergeMap, catchError, map } from 'rxjs/operators';

import { actions } from 'src/modules/modal';
import { actions as sessionActions } from 'src/modules/session';
import { actions as toastActions } from 'src/modules/notifications';
import {
  getIndexer,
  getIndexers,
  editIndexer,
  createIndexer,
  fetchIndexers,
  setIndexerDetailsEdit,
  setTransferOptionsEdit,
  setIndexerActiveStatus,
} from './actions';
import {
  GET_INDEXER,
  IGetIndexer,
  GET_INDEXERS,
  IGetIndexers,
  CREATE_INDEXER,
  ICreateIndexer,
  EDIT_INDEXER,
  IEditIndexer,
  SET_ACTIVE_STATUS,
  ISetActiveStatus,
} from './graphql';

export const createIndexerEpic: RootEpic = (action$, _, { graphql }) =>
  action$.pipe(
    filter(isActionOf(createIndexer.request)),
    mergeMap(action => {
      const { databases, ...data } = action.payload;
      return from(
        graphql.request<ICreateIndexer>(CREATE_INDEXER, { data }),
      ).pipe(
        mergeMap(r => [
          createIndexer.success(r.createIndexer),
          actions.hideModal(),
          toastActions.showSuccessToast({
            id: r.createIndexer.id as string,
            duration: 3000,
            message: 'Indexer created.',
          }),
        ]),
        catchError(error => of(createIndexer.failure(error))),
      );
    }),
  );

export const editIndexerEpic: RootEpic = (action$, _, { graphql }) =>
  action$.pipe(
    filter(isActionOf(editIndexer.request)),
    mergeMap(action => {
      const { id, databases, ...data } = action.payload;
      return from(
        graphql.request<IEditIndexer>(EDIT_INDEXER, { indexerId: id, data }),
      ).pipe(
        mergeMap(r => [
          editIndexer.success(r.updateIndexer),
          setIndexerDetailsEdit(false),
          setTransferOptionsEdit(false),
          toastActions.showSuccessToast({
            id: r.updateIndexer.id as string,
            duration: 3000,
            message: 'Indexer updated.',
          }),
        ]),
        catchError(error => of(editIndexer.failure(error))),
      );
    }),
  );

export const getIndexerEpic: RootEpic = (action$, _, { graphql }) => {
  return action$.pipe(
    filter(isActionOf(getIndexer.request)),
    mergeMap(action =>
      from(
        graphql.request<IGetIndexer>(GET_INDEXER, { id: action.payload }),
      ).pipe(
        map(r => getIndexer.success(r.indexer)),
        catchError(error => of(getIndexer.failure(error))),
      ),
    ),
  );
};

export const fetchIndexersEpic: RootEpic = (action$, _, { config }) =>
  action$.pipe(
    filter(isActionOf([fetchIndexers, sessionActions.fetchAppData])),
    throttleTime(config.networkThrottle),
    map(() => getIndexers.request()),
  );

export const getIndexersEpic: RootEpic = (action$, _, { graphql }) =>
  action$.pipe(
    filter(isActionOf(getIndexers.request)),
    mergeMap(() =>
      from(graphql.request<IGetIndexers>(GET_INDEXERS)).pipe(
        map(r => getIndexers.success(r.indexers)),
        catchError(error => of(createIndexer.failure(error))),
      ),
    ),
  );

export const setIndexerActiveStatusEpic: RootEpic = (action$, _, { graphql }) =>
  action$.pipe(
    filter(isActionOf(setIndexerActiveStatus.request)),
    mergeMap(action =>
      from(graphql.request<ISetActiveStatus>(SET_ACTIVE_STATUS, action.payload)).pipe(
        mergeMap(r => [
          setIndexerActiveStatus.success(r.setIndexerActiveStatus),
          actions.hideModal(),
        ]),
        catchError(error => {
          if (error.response) {
            const err = get(error, 'response.errors.0.message');
            return of(setIndexerActiveStatus.failure(new Error(err)));
          }
          return of(setIndexerActiveStatus.failure(error));
        }),
      ),
    ),
  );
