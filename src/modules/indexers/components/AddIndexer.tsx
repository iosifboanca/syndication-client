import React, { Fragment } from 'react';
import { Formik } from 'formik';
import styled from 'styled-components';

import { Button, Icon, Flex, FormField, Title, th, isRequired, isEmail } from 'src/ui';

import { Indexer } from 'src/domain';
import { useIndexer } from '../hooks';

export const AddIndexer: React.FunctionComponent<{
  closeModal(): void;
}> = ({ closeModal }) => {
  const { createIndexer } = useIndexer();
  const initialValues = new Indexer({ name: '' });

  return (
    <Root vertical p={8} width={500}>
      <Icon
        top={4}
        right={4}
        name='close'
        position='absolute'
        onClick={closeModal}
        color='colors.textPrimary'
      />
      <Flex>
        <Title>Add indexer</Title>
      </Flex>
      <Formik initialValues={initialValues} onSubmit={createIndexer}>
        {({ handleSubmit }) => (
          <Fragment>
            <FormField required name='name' label='Indexer Name' validate={isRequired} />
            <Flex>
              <FormField name='contact.name' label='Contact Name' mr={2} />
              <FormField name='contact.email' label='Contact Email' ml={2} validate={isEmail} />
            </Flex>
            <Flex>
              <Button onClick={closeModal} variant='outline' size='medium' mr={2}>
                CANCEL
              </Button>
              <Button type='submit' onClick={handleSubmit} size='medium' ml={2}>
                CREATE
              </Button>
            </Flex>
          </Fragment>
        )}
      </Formik>
    </Root>
  );
};

// #region styles
const Root = styled(Flex)`
  background-color: ${th('colors.white')};
  border-radius: ${th('gridUnit')};
  position: relative;
`;
// #endregion
