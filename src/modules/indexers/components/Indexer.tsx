import React from 'react';

import styled from 'styled-components';

import { Flex, Separator, th } from 'src/ui';
import { Databases } from 'src/modules/databases';
import { Indexer as IndexerClass } from 'src/domain';

import { useIndexer } from '../hooks';
import { IndexerPublisherProperties, IndexerPublisherForm, SectionHeader } from './';

export const Indexer: React.FunctionComponent<{
  indexer: IndexerClass;
}> = ({ indexer }) => {
  const {
    editError,
    editLoading,
    handleEdit,
    indexerDetailsEditing,
    setIndexerDetailsEdit,
  } = useIndexer();

  return (
    <Root vertical alignItems='flex-start' mr={2}>
      <Flex vertical>
        <SectionHeader
          title='Indexer details'
          editing={indexerDetailsEditing}
          onEdit={setIndexerDetailsEdit(true)}
        />

        {indexerDetailsEditing ? (
          <IndexerPublisherForm
            indexer={indexer}
            error={editError}
            onEdit={handleEdit}
            loading={editLoading}
            cancelEdit={setIndexerDetailsEdit(false)}
          />
        ) : (
          <IndexerPublisherProperties indexer={indexer} />
        )}
      </Flex>

      <Separator my={4} direction='horizontal' />
      <Databases indexerId={indexer.id} />
    </Root>
  );
};

// #region styles
const Root = styled(Flex)`
  align-self: stretch;
  background-color: ${th('colors.white')};
  box-shadow: 0 0 calc(${th('gridUnit')} * 2) rgba(51, 51, 51, 0.11);
  border-radius: ${th('gridUnit')};
  padding: calc(${th('gridUnit')} * 4);
  overflow: hidden;
`;

// #endregion
