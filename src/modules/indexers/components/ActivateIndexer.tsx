import React, { useCallback } from 'react';
import { get } from 'lodash';
import styled from 'styled-components';
import { useDispatch, useSelector } from 'react-redux';

import { Indexer } from 'src/domain';
import { Title, Flex, Button, Text, th } from 'src/ui';

import { setIndexerActiveStatus } from '../actions';
import { setIndexerActiveStatus as setActiveSelector } from '../selectors';

interface Props {
  indexer: Indexer;
  onCancel(): void;
}

export const ActivateIndexer: React.FunctionComponent<Props> = ({ indexer, onCancel }) => {
  const dispatch = useDispatch();
  const { error, loading } = useSelector(setActiveSelector);
  const setStatus = useCallback(() => {
    dispatch(
      setIndexerActiveStatus.request({
        active: !indexer.active,
        indexerId: get(indexer, 'id', ''),
      }),
    );
  }, [indexer, dispatch]);

  return (
    <Root vertical width={500} p={5} alignItems='center'>
      <Title>Activate indexer</Title>
      <Text mt={5}>Are you sure you want to activate {indexer.name}?</Text>

      {error && (
        <Text variant='warning' mt={4}>
          {error}
        </Text>
      )}

      <Flex mt={6}>
        <Button size='medium' variant='secondary' onClick={onCancel} mr={2}>
          CANCEL
        </Button>
        <Button size='medium' onClick={setStatus} ml={2} loading={loading}>
          {indexer.active ? 'DEACTIVATE' : 'ACTIVATE'}
        </Button>
      </Flex>
    </Root>
  );
};

// #region styles
const Root = styled(Flex)`
  background-color: ${th('colors.white')};
  border-radius: ${th('gridUnit')};
`;
// #endregion
