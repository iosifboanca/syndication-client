import React, { Fragment } from 'react';

import { Indexer } from 'src/domain';
import { Flex, Label, Text } from 'src/ui';

interface Props {
  indexer: Indexer;
}

export const IndexerPublisherProperties: React.FunctionComponent<Props> = ({
  indexer: { name, contact, observations },
}) => {
  return (
    <Fragment>
      <Flex justifyContent='flex-start' vertical>
        <Label>Indexer Name</Label>
        <Text>{name}</Text>
      </Flex>

      {contact && (
        <Flex justifyContent='flex-start' mt={4}>
          <Flex vertical>
            <Label>Contact Name</Label>
            <Text>{contact.name || 'Not set'}</Text>
          </Flex>
          <Flex vertical>
            <Label>Contact Email</Label>
            <Text>{contact.email || 'Not set'}</Text>
          </Flex>
        </Flex>
      )}

      <Flex justifyContent='flex-start' vertical mt={4}>
        <Label>Observations</Label>
        <Text>{observations || 'Not set'}</Text>
      </Flex>
    </Fragment>
  );
};
