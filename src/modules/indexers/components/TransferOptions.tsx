import React, { Fragment } from 'react';
import { capitalize } from 'lodash';

import { Badge, Flex, Label, Text, TextHighlight } from 'src/ui';
import {
  Indexer,
  ZipLabels,
  ZipTokens,
  FileLabels,
  SyndicationType,
  TransferProtocol,
} from 'src/domain';

interface Props {
  indexer: Indexer;
}

export const TransferOptions: React.FunctionComponent<Props> = ({ indexer }) => {
  const {
    syndicationType,
    transferProtocol,
    syndicationSchedule = '',
    syndicationFileOptions: { files, zipType, zipPath },
  } = indexer;

  return (
    <Fragment>
      <Flex>
        <Flex vertical>
          <Label>Transfer Protocol</Label>
          <Text>{transferProtocol ? transferProtocol.toUpperCase() : 'Not set'}</Text>
        </Flex>

        <Flex vertical>
          <Label>Upload Schedule</Label>
          <Text>{syndicationSchedule ? capitalize(syndicationSchedule) : 'Not set'}</Text>
        </Flex>

        <Flex vertical>
          <Label>Syndication Type</Label>
          <Text>{syndicationType ? capitalize(syndicationType) : 'Not set'}</Text>
        </Flex>
      </Flex>

      {syndicationType && syndicationType === SyndicationType.files && (
        <Flex vertical mt={2}>
          <Label>Syndication Type</Label>
          <Flex justifyContent='flex-start' flexWrap='wrap'>
            <Text mr={1}>{capitalize(syndicationType)}</Text>
            {files.map((f: string) => (
              <Badge key={f} inverse mr={2}>
                {FileLabels[f]}
              </Badge>
            ))}
          </Flex>
        </Flex>
      )}

      {transferProtocol === TransferProtocol.ftp && (
        <Flex mt={2} alignItems='flex-start'>
          <Flex vertical flex={1}>
            <Label>Packaging options</Label>
            <Text>{ZipLabels[zipType]}</Text>
          </Flex>
          {zipPath && (
            <Flex vertical flex={2}>
              <Label>Zip path</Label>
              <TextHighlight keywords={Object.keys(ZipTokens)}>{zipPath}</TextHighlight>
            </Flex>
          )}
        </Flex>
      )}
    </Fragment>
  );
};
