import React, { Fragment } from 'react';
import { set } from 'lodash';
import { Formik } from 'formik';

import {
  ZipType,
  Indexer,
  ZipTokens,
  ZipTypeOptions,
  SyndicationType,
  TransferProtocol,
  TransferFileOptions,
  transferOptions,
  scheduleOptions,
  syndicationOptions,
} from 'src/domain';
import {
  Text,
  Flex,
  Button,
  Label,
  Dropdown,
  PathInput,
  FormField,
  CheckboxGroup,
  isRequired,
} from 'src/ui';

const validateFn = (values: Indexer) => {
  const errors: any = {};

  if (
    values.syndicationType === SyndicationType.files &&
    (!values.syndicationFileOptions.files || values.syndicationFileOptions.files.length === 0)
  ) {
    set(errors, 'syndicationFileOptions.files', 'At least one file type is required.');
  }

  if (
    values.syndicationFileOptions.zipType !== ZipType.noZip &&
    !values.syndicationFileOptions.zipPath
  ) {
    set(errors, 'syndicationFileOptions.zipPath', 'Path required');
  }

  return errors;
};

const PathFormInput = (props: any) => <PathInput tokens={ZipTokens} {...props} />;

interface Props {
  error?: string;
  loading: boolean;
  indexer: Indexer;
  cancelEdit(): any;
  onEdit(indexer: Indexer): void;
}

export const TransferOptionsForm: React.FunctionComponent<Props> = ({
  error,
  onEdit,
  indexer,
  loading,
  cancelEdit,
}) => {
  return (
    <Formik onSubmit={onEdit} initialValues={indexer} validate={validateFn}>
      {({ handleSubmit, errors, values, setFieldValue }) => {
        return (
          <Fragment>
            <Flex mt={1}>
              <FormField
                mr={2}
                required
                name='transferProtocol'
                label='Transfer Protocol'
                validate={isRequired}
                component={() => (
                  <Dropdown
                    options={transferOptions}
                    value={transferOptions.find(s => s.value === values.transferProtocol)}
                    onChange={option => setFieldValue('transferProtocol', option.value)}
                  />
                )}
              />

              <FormField
                mr={2}
                required
                label='Upload schedule'
                name='syndicationSchedule'
                validate={isRequired}
                component={() => (
                  <Dropdown
                    options={scheduleOptions}
                    value={scheduleOptions.find(s => s.value === values.syndicationSchedule)}
                    onChange={option => setFieldValue('syndicationSchedule', option.value)}
                  />
                )}
              />

              <FormField
                required
                name='syndicationType'
                label='Syndication Type'
                validate={isRequired}
                component={() => (
                  <Dropdown
                    options={syndicationOptions}
                    value={syndicationOptions.find(s => s.value === values.syndicationType)}
                    onChange={option => setFieldValue('syndicationType', option.value)}
                  />
                )}
              />
            </Flex>

            {values.syndicationType === SyndicationType.files && (
              <Flex justifyContent='flex-start' vertical>
                <Label required>File Types</Label>
                <CheckboxGroup
                  optionKey='value'
                  optionLabel='label'
                  optionValue='value'
                  options={TransferFileOptions}
                  name='syndicationFileOptions.files'
                />

                <Flex minHeight={6} justifyContent='flex-start'>
                  {errors.syndicationFileOptions?.files && (
                    <Text variant='warning'>{errors.syndicationFileOptions.files}</Text>
                  )}
                </Flex>
              </Flex>
            )}

            {values.transferProtocol === TransferProtocol.ftp && (
              <Flex vertical>
                <FormField
                  required
                  label='Packaging options'
                  name='syndicationFileOptions.zipType'
                  component={() => (
                    <Dropdown
                      options={ZipTypeOptions}
                      value={ZipTypeOptions.find(
                        o => o.value === values.syndicationFileOptions.zipType,
                      )}
                      onChange={option =>
                        setFieldValue('syndicationFileOptions.zipType', option.value)
                      }
                    />
                  )}
                />

                {values.syndicationFileOptions.zipType !== ZipType.noZip && (
                  <FormField
                    required
                    label='Path inside zip'
                    component={PathFormInput}
                    name='syndicationFileOptions.zipPath'
                  />
                )}
              </Flex>
            )}

            {error && (
              <Flex mb={2}>
                <Text variant='warning'>{error}</Text>
              </Flex>
            )}

            <Flex>
              <Button size='small' variant='outline' onClick={cancelEdit} mr={2}>
                Cancel
              </Button>
              <Button size='small' onClick={handleSubmit} ml={2} type='submit' loading={loading}>
                Save
              </Button>
            </Flex>
          </Fragment>
        );
      }}
    </Formik>
  );
};
