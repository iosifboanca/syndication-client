import React from 'react';

import { TextButton, Flex, Title, Icon } from 'src/ui';

interface Props {
  title: string;
  onEdit(): any;
  editing: boolean;
}

export const SectionHeader: React.FunctionComponent<Props> = ({ title, onEdit, editing }) => {
  return (
    <Flex justifyContent='space-between' mb={2}>
      <Title variant='small'>{title}</Title>
      {!editing && (
        <TextButton onClick={onEdit}>
          <Icon mx={1} name='edit' size={3} color='colors.actionSecondary' />
          Edit
        </TextButton>
      )}
    </Flex>
  );
};
