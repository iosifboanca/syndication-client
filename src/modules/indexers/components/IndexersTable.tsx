import React from 'react';

import { Indexer } from 'src/domain';
import { useNavigation } from 'src/hooks';
import { BorderedRow, Flex, Label, StatusText, Text, THead, Table, ScrollContainer } from 'src/ui';

interface Props {
  indexers: Indexer[];
}

interface TableRowProps {
  item: Indexer;
  onClick(): any;
}

const TableHead = () => (
  <THead height={9} pl={4}>
    <Flex flex={2} justifyContent='flex-start'>
      <Label>Name</Label>
    </Flex>
    <Flex flex={1} justifyContent='flex-start'>
      <Label>Publishers</Label>
    </Flex>
    <Flex flex={1} justifyContent='flex-start'>
      <Label>Status</Label>
    </Flex>
  </THead>
);

const TableRow: React.FC<TableRowProps> = ({ item, onClick }) => {
  const status = item.active ? 'ACTIVE' : 'INACTIVE';

  return (
    <BorderedRow height={8} pl={4} onClick={onClick}>
      <Flex flex={2} justifyContent='flex-start'>
        <Text>{item.name}</Text>
      </Flex>
      <Flex flex={1} justifyContent='flex-start'>
        <StatusText status={item.publisherCount.toString()} warningStatuses={['0']}>
          {item.publisherCount}
        </StatusText>
      </Flex>
      <Flex flex={1} justifyContent='flex-start'>
        <StatusText status={status} warningStatuses={['INACTIVE']}>
          {status}
        </StatusText>
      </Flex>
    </BorderedRow>
  );
};

export const IndexersTable: React.FC<Props> = ({ indexers }) => {
  const { goTo } = useNavigation();

  return (
    <Table vertical>
      <TableHead />
      <ScrollContainer>
        {indexers.map(i => (
          <TableRow key={i.id} item={i} onClick={goTo(`/indexers/${i.id}`)} />
        ))}
      </ScrollContainer>
    </Table>
  );
};
