import React, { Fragment } from 'react';
import styled from 'styled-components';

import { Indexer } from 'src/domain';
import { Publishers } from 'src/modules/publishers';
import { Flex, Title, Text, Separator, th } from 'src/ui';

import { useIndexer } from '../hooks';
import { SectionHeader, TransferOptionsForm, TransferOptions } from './';

export const IndexerPublisherCard: React.FunctionComponent<{
  indexer: Indexer;
}> = ({ indexer }) => {
  const {
    editError,
    handleEdit,
    editLoading,
    setTransferOptionsEdit,
    transferOptionsEditing,
  } = useIndexer();

  return (
    <Root vertical ml={2}>
      <SectionHeader
        title='Transfer Options'
        editing={transferOptionsEditing}
        onEdit={setTransferOptionsEdit(true)}
      />

      {transferOptionsEditing ? (
        <TransferOptionsForm
          indexer={indexer}
          error={editError}
          onEdit={handleEdit}
          loading={editLoading}
          cancelEdit={setTransferOptionsEdit(false)}
        />
      ) : (
        <TransferOptions indexer={indexer} />
      )}
      {indexer.active ? (
        <Fragment>
          <Separator direction='horizontal' my={4} />
          <Publishers indexer={indexer} />
        </Fragment>
      ) : (
        <Fragment>
          <Separator direction='horizontal' my={4} />
          <Flex justifyContent='flex-start' vertical>
            <Title variant='small'>Publishers</Title>
            <Text>Activate the indexer to see available publishers.</Text>
          </Flex>
        </Fragment>
      )}
    </Root>
  );
};

// #region styles
const Root = styled(Flex)`
  align-self: stretch;
  background-color: ${th('colors.white')};
  box-shadow: 0 0 calc(${th('gridUnit')} * 2) rgba(51, 51, 51, 0.11);
  border-radius: ${th('gridUnit')};
  padding: calc(${th('gridUnit')} * 4);
`;
// #endregion
