import React, { Fragment } from 'react';
import { Formik } from 'formik';

import { Indexer } from 'src/domain';
import {
  Text,
  Flex,
  Button,
  Textarea,
  FormField,
  FormFieldProps,
  isEmail,
  isRequired,
} from 'src/ui';

interface Props {
  error?: string;
  loading: boolean;
  indexer: Indexer;
  cancelEdit(): void;
  onEdit(values: Indexer): void;
}

const TextareaField = (props: FormFieldProps) => <Textarea height={20} {...props} />;

export const IndexerPublisherForm: React.FunctionComponent<Props> = ({
  error,
  onEdit,
  indexer,
  loading,
  cancelEdit,
}) => {
  return (
    <Formik onSubmit={onEdit} initialValues={indexer}>
      {({ handleSubmit }) => {
        return (
          <Fragment>
            <FormField required name='name' label='Indexer Name' validate={isRequired} />

            <Flex>
              <FormField name='contact.name' label='Contact Name' mr={2} />
              <FormField name='contact.email' label='Contact Email' ml={2} validate={isEmail} />
            </Flex>

            <FormField name='observations' component={TextareaField} label='Observations' />

            {error && (
              <Flex mb={2}>
                <Text variant='warning'>{error}</Text>
              </Flex>
            )}

            <Flex>
              <Button size='small' variant='outline' onClick={cancelEdit} mr={2}>
                Cancel
              </Button>
              <Button size='small' onClick={handleSubmit} ml={2} type='submit' loading={loading}>
                Save
              </Button>
            </Flex>
          </Fragment>
        );
      }}
    </Formik>
  );
};
