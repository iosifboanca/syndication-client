import { createAsyncAction, createAction } from 'typesafe-actions';
import { IndexerDTO } from 'src/domain';

interface ChangeIndexerStatus {
  indexerId: string;
  active: boolean;
}

export const setIndexerActiveStatus = createAsyncAction(
  'indexers/ACTIVATE_INDEXER_REQUEST',
  'indexers/ACTIVATE_INDEXER_SUCCESS',
  'indexers/ACTIVATE_INDEXER_FAILURE',
)<ChangeIndexerStatus, string, Error>();

export const createIndexer = createAsyncAction(
  'indexers/CREATE_REQUEST',
  'indexers/CREATE_SUCCESS',
  'indexers/CREATE_FAILURE',
)<IndexerDTO, IndexerDTO, Error>();

export const editIndexer = createAsyncAction(
  'indexers/EDIT_INDEXER_REQUEST',
  'indexers/EDIT_INDEXER_SUCCESS',
  'indexers/EDIT_INDEXER_FAILURE',
)<IndexerDTO, IndexerDTO, Error>();

export const getIndexer = createAsyncAction(
  'indexers/GET_INDEXER_REQUEST',
  'indexers/GET_INDEXER_SUCCESS',
  'indexers/GET_INDEXER_FAILURE',
)<string, IndexerDTO, Error>();

export const getIndexers = createAsyncAction(
  'indexers/GET_INDEXERS_REQUEST',
  'indexers/GET_INDEXERS_SUCCESS',
  'indexers/GET_INDEXERS_FAILURE',
)<void, IndexerDTO[], Error>();

export const fetchIndexers = createAction('indexers/FETCH_INDEXERS')();
export const resetCreateIndexer = createAction('indexers/RESET_CREATE')();
export const setIndexerDetailsEdit = createAction('indexers/SET_INDEXER_DETAILS_EDIT')<boolean>();
export const setTransferOptionsEdit = createAction('indexers/SET_TRANSFER_OPTIONS_EDIT')<boolean>();
