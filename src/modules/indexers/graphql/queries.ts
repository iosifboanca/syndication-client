import gql from 'graphql-tag';
import { IndexerDTO } from 'src/domain';
import { indexerDetails, indexerDashboard } from './fragments';

export interface IGetIndexer {
  indexer: IndexerDTO;
}
export interface IGetIndexers {
  indexers: IndexerDTO[];
}

export const GET_INDEXER = gql`
  query indexer($id: String!) {
    indexer(id: $id) {
      ...indexerDetails
    }
  }
  ${indexerDetails}
`;

export const GET_INDEXERS = gql`
  query {
    indexers {
      ...indexerDashboard
    }
  }
  ${indexerDashboard}
`;
