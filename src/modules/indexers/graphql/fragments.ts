import gql from 'graphql-tag';

import { databaseFragment } from 'src/modules/databases';
import { publisherFragment } from 'src/modules/publishers';

export const indexerDashboard = gql`
  fragment indexerDashboard on Indexer {
    id
    name
    active
    publisherCount
    databases {
      ...databaseFragment
    }
  }
  ${databaseFragment}
`;

export const indexerDetails = gql`
  fragment indexerDetails on Indexer {
    id
    name
    active
    code
    owner
    acceptanceCriteria
    observations
    syndicationType
    transferProtocol
    syndicationSchedule
    contact {
      id
      name
      email
    }
    publisherPairs {
      ...publisherFragment
    }
    syndicationManifestOptions {
      type
    }
    syndicationFileOptions {
      type
      files
      zipPath
      zipType
    }
    databases {
      ...databaseFragment
    }
  }
  ${publisherFragment}
  ${databaseFragment}
`;
