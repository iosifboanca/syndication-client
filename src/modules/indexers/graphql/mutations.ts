import gql from 'graphql-tag';
import { IndexerDTO } from 'src/domain';
import { indexerDetails } from './fragments';

export const CREATE_INDEXER = gql`
  mutation($data: IndexerInput!) {
    createIndexer(data: $data) {
      ...indexerDetails
    }
  }
  ${indexerDetails}
`;

export interface ICreateIndexer {
  createIndexer: IndexerDTO;
}

export const EDIT_INDEXER = gql`
  mutation($indexerId: String!, $data: IndexerInput!) {
    updateIndexer(indexerId: $indexerId, data: $data) {
      ...indexerDetails
    }
  }
  ${indexerDetails}
`;

export interface IEditIndexer {
  updateIndexer: IndexerDTO;
}

export const SET_ACTIVE_STATUS = gql`
  mutation($indexerId: String!, $active: Boolean!) {
    setIndexerActiveStatus(indexerId: $indexerId, active: $active)
  }
`;

export interface ISetActiveStatus {
  setIndexerActiveStatus: string;
}
