import { createSelector } from 'reselect';
import { RootState } from 'typesafe-actions';

import { Publisher, IndexerPublisher } from 'src/domain';

export type PublishersRecord = Record<string, Publisher>;

const root = (state: RootState) => state.publishers;

// publishers selectors
export const publishers = createSelector(root, ({ publishers = [] }) =>
  publishers.map(p => new Publisher(p)),
);
export const publishersStatus = createSelector(root, state => state.publishersStatus);

export const getPublishers = createSelector(publishers, publishersStatus, (publishers, status) => ({
  publishers,
  ...status,
}));

// indexerPublishers selectors
export const indexerPublishers = createSelector(root, ({ indexerPublishers = [] }) =>
  indexerPublishers.map(ip => new IndexerPublisher(ip)),
);
export const getIndexerPublishersStatus = createSelector(
  root,
  state => state.getIndexerPublishersStatus,
);

export const getIndexerPublishers = createSelector(
  indexerPublishers,
  getIndexerPublishersStatus,
  (indexerPublishers, status) => ({ indexerPublishers, ...status }),
);

export const createIndexerPublishersStatus = createSelector(
  root,
  state => state.createIndexerPublisherStatus,
);

export const disableIndexerPublisherStatus = createSelector(
  root,
  state => state.disableIndexerPublisherStatus,
);

export const updateIndexerPublisherStatus = createSelector(
  root,
  state => state.updateIndexerPublisherStatus,
);

export const publishersRecord = createSelector(publishers, publishers => {
  return publishers.reduce(
    (acc, publisher) => ({
      ...acc,
      [publisher.id]: publisher,
    }),
    {},
  ) as PublishersRecord;
});

// combined selectors
export const availablePublishers = createSelector(
  publishers,
  indexerPublishers,
  (publishers, indexerPublishers) => {
    const connectedIds = new Set(indexerPublishers.map(ip => ip.publisherId));
    return publishers.filter(p => !connectedIds.has(p.id));
  },
);
