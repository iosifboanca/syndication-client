import { from, of } from 'rxjs';
import { isActionOf, RootEpic } from 'typesafe-actions';
import { filter, mergeMap, catchError, map, throttleTime } from 'rxjs/operators';

import { actions as modalActions } from 'src/modules/modal';
import { actions as toastActions } from 'src/modules/notifications';
import { actions as sessionActions } from 'src/modules/session';

import {
  getPublishers,
  fetchPublishers,
  createIndexerPublisher,
  updateIndexerPublisher,
  disableIndexerPublisher,
} from './actions';
import {
  GET_PUBLISHERS,
  IGetPublishers,
  CREATE_INDEXER_PUBLISHER,
  ICreateIndexerPublisher,
  DISABLE_INDEXER_PUBLISHER,
  IDisableIndexerPublisher,
  UPDATE_INDEXER_PUBLISHER,
  IUpdateIndexerPublisher,
} from './graphql';

export const fetchPublishersEpic: RootEpic = (action$, _, { config }) =>
  action$.pipe(
    filter(isActionOf([fetchPublishers, sessionActions.fetchAppData])),
    throttleTime(config.networkThrottle),
    map(() => getPublishers.request()),
  );

export const getPublishersEpic: RootEpic = (action$, _, { graphql }) =>
  action$.pipe(
    filter(isActionOf(getPublishers.request)),
    mergeMap(() =>
      from(graphql.request<IGetPublishers>(GET_PUBLISHERS)).pipe(
        map(r => getPublishers.success(r.publishers)),
        catchError(error => of(getPublishers.failure(error))),
      ),
    ),
  );

export const createIndexerPublisherEpic: RootEpic = (action$, _, { graphql }) =>
  action$.pipe(
    filter(isActionOf(createIndexerPublisher.request)),
    mergeMap(action => {
      return from(
        graphql.request<ICreateIndexerPublisher>(CREATE_INDEXER_PUBLISHER, {
          data: action.payload,
        }),
      ).pipe(
        mergeMap(r => [
          createIndexerPublisher.success(r.createIndexerPublisher),
          toastActions.showSuccessToast({
            id: r.createIndexerPublisher.indexerId,
            duration: 3000,
            message: 'Successfully created indexer publisher pair.',
          }),
        ]),
        catchError(error => of(createIndexerPublisher.failure(error))),
      );
    }),
  );

export const disableIndexerPublisherEpic: RootEpic = (action$, _, { graphql }) =>
  action$.pipe(
    filter(isActionOf(disableIndexerPublisher.request)),
    mergeMap(action => {
      const { indexerId, publisherId } = action.payload;
      return from(
        graphql.request<IDisableIndexerPublisher>(DISABLE_INDEXER_PUBLISHER, {
          indexerId,
          publisherId,
        }),
      ).pipe(
        mergeMap(r => [
          disableIndexerPublisher.success(r.disableIndexerPublisher),
          modalActions.hideModal(),
          toastActions.showSuccessToast({
            id: r.disableIndexerPublisher.indexerId,
            duration: 3000,
            message: 'Disabled publisher.',
          }),
        ]),
        catchError(error => of(disableIndexerPublisher.failure(error))),
      );
    }),
  );

export const updateIndexerPublisherEpic: RootEpic = (action$, _, { graphql }) =>
  action$.pipe(
    filter(isActionOf(updateIndexerPublisher.request)),
    mergeMap(action =>
      from(
        graphql.request<IUpdateIndexerPublisher>(UPDATE_INDEXER_PUBLISHER, {
          data: action.payload,
        }),
      ).pipe(
        mergeMap(r => [
          updateIndexerPublisher.success(r.updateIndexerPublisher),
          modalActions.hideModal(),
          toastActions.showSuccessToast({
            id: r.updateIndexerPublisher.indexerId,
            duration: 3000,
            message: 'Successfully updated indexer publisher pair.',
          }),
        ]),
        catchError(error => of(updateIndexerPublisher.failure(error))),
      ),
    ),
  );
