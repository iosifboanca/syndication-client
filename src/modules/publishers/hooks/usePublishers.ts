import { useDispatch } from 'react-redux';
import { useCallback } from 'react';

import { IndexerPublisher, TransferProtocol, Indexer, IndexerPublisherDTO } from 'src/domain';

import { createIndexerPublisher, updateIndexerPublisher } from '../actions';

function cleanupIndexerPublisherData(
  indexer: Indexer,
  values: IndexerPublisher,
): IndexerPublisherDTO {  
  const data = values.dto;

  if (indexer.transferProtocol === TransferProtocol.ftp) {
    delete data.httpTransferOptions;
  } else if (indexer.transferProtocol === TransferProtocol.http) {
    delete data.ftpTransferOptions;
  }

  return data;
}

export const usePublishers = (indexer: Indexer) => {
  const dispatch = useDispatch();

  const createPublisher = useCallback(
    (values: IndexerPublisher) => {
      const data = cleanupIndexerPublisherData(indexer, values);
      dispatch(createIndexerPublisher.request(data));
    },
    [dispatch, indexer],
  );

  const editPublisher = useCallback(
    (values: IndexerPublisher) => {
      const data = cleanupIndexerPublisherData(indexer, values);
      dispatch(updateIndexerPublisher.request(data));
    },
    [dispatch, indexer],
  );

  return {
    createPublisher,
    editPublisher,
  };
};
