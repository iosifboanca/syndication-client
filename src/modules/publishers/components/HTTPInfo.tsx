import React from 'react';
import { get } from 'lodash';

import { Flex, Label, Text } from 'src/ui';
import { IndexerPublisherDTO } from 'src/domain';

export interface HTTPInfoProps {
  indexerPublisher: IndexerPublisherDTO;
}

export const HTTPInfo: React.FunctionComponent<HTTPInfoProps> = ({ indexerPublisher }) => {
  return (
    <Flex vertical my={2} pl={4}>
      <Flex>
        <Flex flex={1} justifyContent='flex-start'>
          <Label>HTTP Endpoint</Label>
        </Flex>
        <Flex flex={2} justifyContent='flex-start'>
          <Text>{get(indexerPublisher, 'httpTransferOptions.endpoint', '')}</Text>
        </Flex>
      </Flex>
      <Flex>
        <Flex flex={1} justifyContent='flex-start'>
          <Label>Access Token</Label>
        </Flex>
        <Flex flex={2} justifyContent='flex-start'>
          <Text>{get(indexerPublisher, 'httpTransferOptions.accessToken', '')}</Text>
        </Flex>
      </Flex>
    </Flex>
  );
};
