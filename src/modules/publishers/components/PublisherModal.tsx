import React from 'react';
import styled from 'styled-components';

import { Icon, Flex, Title, th } from 'src/ui';
import { Publisher, TransferProtocol, IndexerPublisher, Indexer } from 'src/domain';

import { FTPForm } from './FTPForm';
import { HTTPForm } from './HTTPForm';

interface Props {
  onCancel: any;
  loading: boolean;
  buttonLabel?: string;
  error?: string;
  onSubmit(values: any): void;
  indexer: Indexer;
  publisher: Publisher;
  initialValues?: IndexerPublisher;
}

export const PublisherModal: React.FunctionComponent<Props> = ({
  indexer,
  publisher,
  initialValues,
  ...props
}) => {
  const initial = new IndexerPublisher({
    indexerId: indexer.id,
    publisherId: publisher.id,
    ...initialValues,
  });

  return (
    <Root alignSelf='stretch' vertical>
      <Flex justifyContent='space-between' mb={2}>
        <Title variant='primary'>{`${indexer.name} - ${publisher.name}`}</Title>
        <Icon color='colors.textPrimary' name='close' onClick={props.onCancel} />
      </Flex>

      {indexer.transferProtocol === TransferProtocol.ftp && (
        <FTPForm initialValues={initial} {...props} />
      )}

      {indexer.transferProtocol === TransferProtocol.http && (
        <HTTPForm initialValues={initial} {...props} />
      )}
    </Root>
  );
};

PublisherModal.defaultProps = {
  buttonLabel: 'ADD',
};

// #region styles
const Root = styled(Flex)`
  background-color: ${th('colors.white')};
  box-shadow: 0px 1px 10px rgba(36, 36, 36, 0.8);
  padding: calc(${th('gridUnit')} * 6);
  width: calc(${th('gridUnit')} * 180);
`;
// #endregion
