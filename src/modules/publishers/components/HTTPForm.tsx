import React, { Fragment } from 'react';
import { Formik, FieldArray } from 'formik';

import { ContactListForm } from './ContactListForm';
import { IndexerPublisher } from 'src/domain';
import { Flex, FormField, Text, Button, isRequired } from 'src/ui';

interface Props {
  error?: string;
  onCancel(): void;
  loading: boolean;
  buttonLabel?: string;
  onSubmit(values: any): void;
  initialValues: IndexerPublisher;
}

export const HTTPForm: React.FunctionComponent<Props> = ({
  error,
  loading,
  onCancel,
  onSubmit,
  buttonLabel,
  initialValues,
}) => {
  return (
    <Formik initialValues={initialValues} onSubmit={onSubmit}>
      {({ handleSubmit, values }) => {
        return (
          <Fragment>
            <Flex vertical flex={1}>
              <FormField
                required
                label='HTTP Endpoint'
                validate={isRequired}
                name='httpTransferOptions.endpoint'
              />
              <FormField
                required
                validate={isRequired}
                label='HTTP Access Token'
                name='httpTransferOptions.accessToken'
              />
              <FieldArray
                name='contacts'
                render={helpers => <ContactListForm values={values.contacts} helpers={helpers} />}
              />
            </Flex>

            {error && (
              <Flex justifyContent='flex-end' mb={2}>
                <Text variant='warning'>{error}</Text>
              </Flex>
            )}

            <Flex justifyContent='flex-end' mb={1}>
              <Button variant='outline' size='medium' onClick={onCancel}>
                CANCEL
              </Button>
              <Button
                ml={6}
                type='submit'
                size='medium'
                variant='primary'
                loading={loading}
                onClick={handleSubmit}
              >
                {buttonLabel}
              </Button>
            </Flex>
          </Fragment>
        );
      }}
    </Formik>
  );
};
