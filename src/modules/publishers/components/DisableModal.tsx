import React, { useCallback } from 'react';
import styled from 'styled-components';
import { useDispatch, useSelector } from 'react-redux';

import { IndexerPublisherDTO } from 'src/domain';
import { Button, Flex, Text, Title, th } from 'src/ui';

import { disableIndexerPublisher } from '../actions';
import { disableIndexerPublisherStatus } from '../selectors';

export interface DisableModalProps {
  onCancel(): any;
  publisherName: string;
  indexerPublisher: IndexerPublisherDTO;
}

export const DisableModal: React.FunctionComponent<DisableModalProps> = ({
  onCancel,
  publisherName,
  indexerPublisher,
}) => {
  const dispatch = useDispatch();
  const { error, loading } = useSelector(disableIndexerPublisherStatus);

  const disable = useCallback(() => {
    dispatch(disableIndexerPublisher.request(indexerPublisher));
  }, [indexerPublisher, dispatch]);

  return (
    <Root vertical p={5} alignItems='center' width={500}>
      <Title>Disable connection</Title>
      <Text mt={5}>Are you sure you want to disable this connection to {publisherName}?</Text>
      {error && <Text variant='warning'>{error}</Text>}

      <Flex mt={6}>
        <Button size='medium' variant='secondary' onClick={onCancel} mr={2}>
          CANCEL
        </Button>
        <Button ml={2} size='medium' variant='primary' loading={loading} onClick={disable}>
          DISABLE
        </Button>
      </Flex>
    </Root>
  );
};

// #region styles
const Root = styled(Flex)`
  background-color: ${th('colors.white')};
  border-radius: ${th('gridUnit')};
`;
// #endregion
