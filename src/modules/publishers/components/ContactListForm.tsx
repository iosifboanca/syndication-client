import React, { useCallback } from 'react';
import { FieldArrayRenderProps } from 'formik';

import { Contact } from 'src/domain';
import {
  Icon,
  Text,
  Flex,
  Title,
  FormField,
  TextButton,
  ScrollContainer,
  isEmail,
  isRequired,
} from 'src/ui';
interface Props {
  values: Contact[];
  helpers: FieldArrayRenderProps;
}

const emailValidators = [isEmail, isRequired];
export const ContactListForm = ({ values, helpers }: Props) => {
  const handleAdd = useCallback(() => helpers.push(new Contact({ name: '', email: '' })), [
    helpers,
  ]);
  const handleRemove = useCallback(i => () => helpers.remove(i), [helpers]);

  return (
    <Flex vertical height='100%' pb={4}>
      <Flex justifyContent='space-between' mb={2}>
        <Title>Contacts</Title>
        <TextButton onClick={handleAdd}>
          <Icon mx={1} name='expand' size={3} />
          Add another contact
        </TextButton>
      </Flex>

      <ScrollContainer>
        {values ? (
          values.map((_, index) => (
            <Flex key={index} alignItems='center'>
              <FormField name={`contacts.${index}.name`} label='Name' mr={2} />
              <FormField
                ml={2}
                label='Email'
                validate={emailValidators}
                name={`contacts.${index}.email`}
              />
              <TextButton onClick={handleRemove(index)} mb={3}>
                <Icon mr={1} name='delete' size={4} />
                Remove
              </TextButton>
            </Flex>
          ))
        ) : (
          <Flex justifyContent='flex-start'>
            <Text>No contacts.</Text>
          </Flex>
        )}
      </ScrollContainer>
    </Flex>
  );
};
