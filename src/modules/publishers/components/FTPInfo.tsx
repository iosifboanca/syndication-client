import React from 'react';

import { FtpOptions, FTPTokens } from 'src/domain';
import { Flex, Label, Text, TextHighlight } from 'src/ui';

const ParameterGuard: React.FC<{ parameter: string }> = ({ parameter }) =>
  parameter ? <Text>{parameter}</Text> : <Text variant='warning'>Missing parameter</Text>;

export const FTPInfo: React.FunctionComponent<{
  ftpOptions: FtpOptions;
}> = ({ ftpOptions }) => {
  return (
    <Flex vertical my={2} pl={4}>
      <Flex>
        <Flex flex={1} justifyContent='flex-start'>
          <Label>User Name</Label>
        </Flex>
        <Flex flex={2} justifyContent='flex-start'>
          <ParameterGuard parameter={ftpOptions.username} />
        </Flex>
      </Flex>
      <Flex>
        <Flex flex={1} justifyContent='flex-start'>
          <Label>User Password</Label>
        </Flex>
        <Flex flex={2} justifyContent='flex-start'>
          <ParameterGuard parameter={ftpOptions.password} />
        </Flex>
      </Flex>
      <Flex>
        <Flex flex={1} justifyContent='flex-start'>
          <Label>FTP Server</Label>
        </Flex>
        <Flex flex={2} justifyContent='flex-start'>
          <ParameterGuard parameter={ftpOptions.hostname} />
        </Flex>
      </Flex>
      <Flex mb={2}>
        <Flex flex={1} justifyContent='flex-start'>
          <Label>FTP Server Local Path</Label>
        </Flex>
        <Flex flex={2} justifyContent='flex-start'>
          {ftpOptions.pathPattern ? (
            <TextHighlight keywords={Object.keys(FTPTokens)}>
              {ftpOptions.pathPattern}
            </TextHighlight>
          ) : (
            <Text variant='warning'>Missing parameter</Text>
          )}
        </Flex>
      </Flex>
    </Flex>
  );
};
