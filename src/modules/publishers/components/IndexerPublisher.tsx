import React, { Fragment, useMemo, useCallback } from 'react';
import { useSelector } from 'react-redux';

import { Modal, useModal } from 'src/modules/modal';
import { TextButton, Expander, Icon, Flex, Text } from 'src/ui';
import {
  Indexer,
  Publisher,
  TransferProtocol,
  IndexerPublisher as IndexerPublisherClass,
} from 'src/domain';

import { usePublishers } from '../hooks';
import { updateIndexerPublisherStatus } from '../selectors';
import { DisableModal, FTPInfo, HTTPInfo, PublisherModal } from './';

const RightChildren: React.FC<{ showEditModal(): void; showDisableModal(): void }> = ({
  showEditModal,
  showDisableModal,
}) => {
  const handleModal = useCallback(
    (fn: Function) => (e: React.MouseEvent) => {
      e.stopPropagation();
      fn();
    },
    [],
  );

  return (
    <Flex justifyContent='flex-end' flex={0} height={9}>
      <TextButton onClick={handleModal(showEditModal)}>
        <Icon mr={1} name='edit' size={3} />
        Edit
      </TextButton>
      <TextButton onClick={handleModal(showDisableModal)} mr={3}>
        Disable
      </TextButton>
    </Flex>
  );
};

interface Props {
  indexer: Indexer;
  publisher: Publisher;
  indexerPublisher: IndexerPublisherClass;
}

export const IndexerPublisher: React.FC<Props> = ({ indexer, publisher, indexerPublisher }) => {
  const modalKey = `edit-${indexerPublisher.publisherId}`;
  const disableModalKey = `disable-${indexerPublisher.publisherId}`;

  const [showEditModal, hideEditModal] = useModal(modalKey);
  const [showDisableModal, hideDisableModal] = useModal(disableModalKey);
  const { loading, error } = useSelector(updateIndexerPublisherStatus);
  const { editPublisher } = usePublishers(indexer);

  const hasMissingParameters = useMemo(() => {
    const { type, ...params } = indexerPublisher[
      indexer.transferProtocol === TransferProtocol.ftp
        ? 'ftpTransferOptions'
        : 'httpTransferOptions'
    ];

    return Object.values(params).some(v => !v);
  }, [indexer.transferProtocol, indexerPublisher]);

  return (
    <Fragment>
      {hasMissingParameters && (
        <Flex justifyContent='flex-start' mb={1}>
          <Icon name='warningFilled' color='colors.warning' mr={1} />
          <Text variant='warning'>Missing configuration for the current transfer protocol</Text>
        </Flex>
      )}
      <Expander
        mb={3}
        title={publisher?.name}
        rightChildren={
          <RightChildren showDisableModal={showDisableModal} showEditModal={showEditModal} />
        }
      >
        {indexer.transferProtocol === TransferProtocol.ftp && (
          <FTPInfo ftpOptions={indexerPublisher.ftpTransferOptions} />
        )}
        {indexer.transferProtocol === TransferProtocol.http && (
          <HTTPInfo indexerPublisher={indexerPublisher} />
        )}
      </Expander>

      <Modal modalKey={modalKey} justifyContent='flex-end'>
        <PublisherModal
          error={error}
          loading={loading}
          indexer={indexer}
          buttonLabel='EDIT'
          publisher={publisher}
          onSubmit={editPublisher}
          onCancel={hideEditModal}
          initialValues={indexerPublisher}
        />
      </Modal>
      <Modal modalKey={disableModalKey}>
        <DisableModal
          onCancel={hideDisableModal}
          publisherName={publisher?.name}
          indexerPublisher={indexerPublisher}
        />
      </Modal>
    </Fragment>
  );
};
