import React, { Fragment } from 'react';
import { Formik, FieldArray } from 'formik';

import { ContactListForm } from './ContactListForm';
import { IndexerPublisher, FTPTokens } from 'src/domain';
import { FormField, Flex, Text, Button, PathInput, isRequired } from 'src/ui';

interface Props {
  error?: string;
  onCancel(): void;
  loading: boolean;
  buttonLabel?: string;
  initialValues: IndexerPublisher;
  onSubmit(values: IndexerPublisher): void;
}

const PathFormInput = (props: any) => <PathInput tokens={FTPTokens} label='FTP Path:' {...props} />;

export const FTPForm: React.FunctionComponent<Props> = ({
  error,
  loading,
  onCancel,
  onSubmit,
  buttonLabel,
  initialValues,
}) => {
  return (
    <Formik initialValues={initialValues} onSubmit={onSubmit}>
      {({ handleSubmit, values }) => {
        return (
          <Fragment>
            <Flex vertical flex={1}>
              <FormField
                required
                label='User Name'
                validate={isRequired}
                name='ftpTransferOptions.username'
              />
              <FormField
                required
                label='User Password'
                validate={isRequired}
                name='ftpTransferOptions.password'
              />
              <FormField
                required
                label='FTP Server'
                validate={isRequired}
                name='ftpTransferOptions.hostname'
              />
              <FormField
                required
                validate={isRequired}
                component={PathFormInput}
                label='FTP Server Local Path'
                name='ftpTransferOptions.pathPattern'
              />
              <FieldArray
                name='contacts'
                render={helpers => <ContactListForm values={values.contacts} helpers={helpers} />}
              />
            </Flex>

            {error && (
              <Flex justifyContent='flex-end' mb={2}>
                <Text variant='warning'>{error}</Text>
              </Flex>
            )}

            <Flex justifyContent='flex-end' mb={1}>
              <Button variant='outline' size='medium' onClick={onCancel}>
                CANCEL
              </Button>
              <Button
                ml={6}
                type='submit'
                size='medium'
                variant='primary'
                loading={loading}
                onClick={handleSubmit}
              >
                {buttonLabel}
              </Button>
            </Flex>
          </Fragment>
        );
      }}
    </Formik>
  );
};
