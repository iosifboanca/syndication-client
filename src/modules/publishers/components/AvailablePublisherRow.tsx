import React, { Fragment } from 'react';
import styled from 'styled-components';

import { Indexer, Publisher } from 'src/domain';
import { Modal, useModal } from 'src/modules/modal';
import { TextButton, Flex, Label, th } from 'src/ui';

import { usePublishers } from '../hooks';
import { PublisherModal } from './PublisherModal';

interface Props {
  error?: string;
  loading: boolean;
  indexer: Indexer;
  publisher: Publisher;
}

export const AvailablePublisherRow: React.FunctionComponent<Props> = ({
  error,
  indexer,
  loading,
  publisher,
}) => {
  const modalKey = `add-publisher-${publisher.id}`;
  const [showAddPublisher, closeAddPublishers] = useModal(modalKey);
  const { createPublisher } = usePublishers(indexer);

  return (
    <Fragment>
      <PublisherRow justifyContent='space-between' mb={3} px={3}>
        <Label>{publisher.name}</Label>
        <TextButton onClick={showAddPublisher}>Enable</TextButton>
      </PublisherRow>

      <Modal modalKey={modalKey} justifyContent='flex-end'>
        <PublisherModal
          error={error}
          indexer={indexer}
          loading={loading}
          publisher={publisher}
          onSubmit={createPublisher}
          onCancel={closeAddPublishers}
        />
      </Modal>
    </Fragment>
  );
};

// #region styles
const PublisherRow = styled(Flex)`
  background-color: ${th('colors.background')};
  border-radius: ${th('gridUnit')};
  height: calc(${th('gridUnit')} * 10);
`;
// #endregion
