import React, { Fragment } from 'react';
import { useSelector } from 'react-redux';

import { Indexer } from 'src/domain';
import { Flex, ScrollContainer, Title } from 'src/ui';

import { AvailablePublisherRow } from './';
import { IndexerPublisher } from './IndexerPublisher';
import {
  publishersRecord,
  createIndexerPublishersStatus,
  indexerPublishers as indexerPublishersSelector,
  availablePublishers as availablePublishersSelector,
} from '../selectors';

export const Publishers: React.FC<{
  indexer: Indexer;
}> = ({ indexer }) => {
  const createStatus = useSelector(createIndexerPublishersStatus);
  const availablePublishers = useSelector(availablePublishersSelector);

  const indexerPublishers = useSelector(indexerPublishersSelector);
  const publishers = useSelector(publishersRecord);

  return (
    <Fragment>
      <Flex justifyContent='flex-start' mb={2}>
        <Title variant='small'>Enabled Publishers</Title>
      </Flex>

      <Flex vertical height={indexerPublishers.length === 0 ? 'fit-content' : '60%'}>
        {!indexerPublishers.length ? (
          <Flex bg='background' height={10}>
            <Title variant='small' color='light'>
              No enabled publishers yet.
            </Title>
          </Flex>
        ) : (
          <ScrollContainer>
            {indexerPublishers.map((indexerPublisher, index) => (
              <IndexerPublisher
                indexer={indexer}
                key={indexerPublisher.publisherId}
                indexerPublisher={indexerPublisher}
                publisher={publishers[indexerPublisher.publisherId]}
              />
            ))}
          </ScrollContainer>
        )}
      </Flex>

      <Flex vertical mt={2} height='100%'>
        <Title variant='small' mb={2}>
          Available publishers
        </Title>
        <ScrollContainer>
          {availablePublishers.map(publisher => (
            <AvailablePublisherRow
              indexer={indexer}
              key={publisher.id}
              publisher={publisher}
              {...createStatus}
            />
          ))}
        </ScrollContainer>
      </Flex>
    </Fragment>
  );
};
