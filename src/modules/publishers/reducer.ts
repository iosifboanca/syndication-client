import { get } from 'lodash';
import { produce } from 'immer';
import { combineReducers } from 'redux';
import { createReducer } from 'typesafe-actions';

import { getIndexer } from 'src/modules/indexers/actions';
import { PublisherDTO, IndexerPublisherDTO } from 'src/domain';
import { operationStatusReducerCreator } from 'src/redux/utils';

import {
  getPublishers,
  createIndexerPublisher,
  updateIndexerPublisher,
  disableIndexerPublisher,
} from './actions';

const initialPublishersState: PublisherDTO[] = [];

const publishers = createReducer(initialPublishersState).handleAction(
  getPublishers.success,
  (_, action) => action.payload,
);

const initialIndexerPublishers: IndexerPublisherDTO[] = [];

const filterIndexerPublisher = (indexerPublisher: IndexerPublisherDTO) => (
  ip: IndexerPublisherDTO,
) => ip.indexerId !== indexerPublisher.indexerId || ip.publisherId !== indexerPublisher.publisherId;

const indexerPublishers = createReducer(initialIndexerPublishers)
  .handleAction(disableIndexerPublisher.success, (state, action) =>
    state.filter(filterIndexerPublisher(action.payload)),
  )
  .handleAction(updateIndexerPublisher.success, (state, action) =>
    produce(state, draft => {
      const index = draft.findIndex(
        ip =>
          ip.publisherId === action.payload.publisherId &&
          ip.indexerId === action.payload.indexerId,
      );
      draft[index] = action.payload;
    }),
  )
  .handleAction(getIndexer.success, (_, action) => get(action.payload, 'publisherPairs', []))
  .handleAction(createIndexerPublisher.success, (state, action) => [action.payload, ...state]);

const publishersStatus = operationStatusReducerCreator(getPublishers);
const createIndexerPublisherStatus = operationStatusReducerCreator(createIndexerPublisher);

const getIndexerPublishersStatus = operationStatusReducerCreator(getIndexer);
const updateIndexerPublisherStatus = operationStatusReducerCreator(updateIndexerPublisher);
const disableIndexerPublisherStatus = operationStatusReducerCreator(disableIndexerPublisher);

export default combineReducers({
  publishers,
  publishersStatus,
  indexerPublishers,
  getIndexerPublishersStatus,
  createIndexerPublisherStatus,
  updateIndexerPublisherStatus,
  disableIndexerPublisherStatus,
});
