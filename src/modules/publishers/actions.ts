import { createAction, createAsyncAction } from 'typesafe-actions';

import { PublisherDTO, IndexerPublisherDTO } from 'src/domain';

export const createIndexerPublisher = createAsyncAction(
  'publishers/CREATE_INDEXER_PUBLISER_REQUEST',
  'publishers/CREATE_INDEXER_PUBLISER_SUCCESS',
  'publishers/CREATE_INDEXER_PUBLISER_FAILURE',
)<IndexerPublisherDTO, IndexerPublisherDTO, Error>();

export const disableIndexerPublisher = createAsyncAction(
  'publishers/DISABLE_INDEXER_PUBLISHER_REQUEST',
  'publishers/DISABLE_INDEXER_PUBLISHER_SUCCESS',
  'publishers/DISABLE_INDEXER_PUBLISHER_FAILURE',
)<IndexerPublisherDTO, IndexerPublisherDTO, Error>();

export const getPublishers = createAsyncAction(
  'publishers/GET_PUBLISHERS_REQUEST',
  'publishers/GET_PUBLISHERS_SUCCESS',
  'publishers/GET_PUBLISHERS_FAILURE',
)<void, PublisherDTO[], Error>();

export const updateIndexerPublisher = createAsyncAction(
  'publishers/UPDATE_INDEXER_PUBLISHER_REQUEST',
  'publishers/UPDATE_INDEXER_PUBLISHER_SUCCESS',
  'publishers/UPDATE_INDEXER_PUBLISHER_FAILURE',
)<IndexerPublisherDTO, IndexerPublisherDTO, Error>();

export const fetchPublishers = createAction('publishers/FETCH_PUBLISHERS')();
