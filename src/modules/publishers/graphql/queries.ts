import gql from 'graphql-tag';

import { PublisherDTO } from 'src/domain';

export const GET_PUBLISHERS = gql`
  query {
    publishers {
      id
      name
    }
  }
`;

export interface IGetPublishers {
  publishers: PublisherDTO[];
}
