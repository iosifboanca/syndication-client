import gql from 'graphql-tag';

export const publisherFragment = gql`
  fragment publisherFragment on IndexerPublisher {
    indexerId
    publisherId
    contacts {
      id
      name
      email
    }
    ftpTransferOptions {
      type
      hostname
      username
      password
      pathPattern
    }
    httpTransferOptions {
      type
      endpoint
      accessToken
    }
  }
`;
