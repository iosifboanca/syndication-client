import gql from 'graphql-tag';
import { IndexerPublisherDTO } from 'src/domain';

import { publisherFragment } from './fragments';

export const CREATE_INDEXER_PUBLISHER = gql`
  mutation createIndexerPublisher($data: IndexerPublisherInput!) {
    createIndexerPublisher(data: $data) {
      ...publisherFragment
    }
  }
  ${publisherFragment}
`;

export interface ICreateIndexerPublisher {
  createIndexerPublisher: IndexerPublisherDTO;
}

export const DISABLE_INDEXER_PUBLISHER = gql`
  mutation disableIndexerPublisher($indexerId: String!, $publisherId: String!) {
    disableIndexerPublisher(indexerId: $indexerId, publisherId: $publisherId) {
      indexerId
      publisherId
    }
  }
`;

export interface IDisableIndexerPublisher {
  disableIndexerPublisher: IndexerPublisherDTO;
}

export const UPDATE_INDEXER_PUBLISHER = gql`
  mutation updateIndexerPublisher($data: IndexerPublisherInput!) {
    updateIndexerPublisher(data: $data) {
      ...publisherFragment
    }
  }
  ${publisherFragment}
`;

export interface IUpdateIndexerPublisher {
  updateIndexerPublisher: IndexerPublisherDTO;
}
