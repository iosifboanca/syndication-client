import { createSelector } from 'reselect';
import { RootState } from 'typesafe-actions';

const root = (state: RootState) => state.modal;

const modalKey = (_: any, props: any) => props.modalKey;

export const isVisible = createSelector(root, modalKey, (state, key) => state.modalKey === key);
