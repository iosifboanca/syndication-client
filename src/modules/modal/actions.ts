import { createAction } from 'typesafe-actions';

export const showModal = createAction('modal/SHOW_MODAL', (modalKey: string) => ({ modalKey }))();

export const hideModal = createAction('modal/HIDE_MODAL')();
