import { createReducer } from 'typesafe-actions';

import { hideModal, showModal } from './actions';

interface ModalState {
  modalKey: string | null;
}

const root: ModalState = {
  modalKey: null,
};

const state = createReducer(root)
  .handleAction(showModal, (_, action) => ({
    modalKey: action.payload.modalKey,
  }))
  .handleAction(hideModal, () => ({
    modalKey: null,
  }));

export default state;
