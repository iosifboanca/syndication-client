import React, { useCallback } from 'react';
import styled from 'styled-components';
import { createPortal } from 'react-dom';
import { RootState } from 'typesafe-actions';
import { flexbox, FlexboxProps } from 'styled-system';
import { useDispatch, useSelector } from 'react-redux';

import { Flex } from 'src/ui';

import * as selectors from './selectors';
import * as modalActions from './actions';

interface ModalProps extends FlexboxProps {
  modalKey: string;
}

type UseModal = [() => void, () => void, boolean];

export const useModal = (modalKey: string): UseModal => {
  const dispatch = useDispatch();
  const isVisible = useSelector((state: RootState) => selectors.isVisible(state, { modalKey }));

  const showModal = useCallback(() => dispatch(modalActions.showModal(modalKey)), [
    dispatch,
    modalKey,
  ]);
  const hideModal = useCallback(() => dispatch(modalActions.hideModal()), [dispatch]);

  return [showModal, hideModal, isVisible];
};

export const Modal: React.FC<ModalProps> = ({ children, modalKey, ...rest }) => {
  const [, , isVisible] = useModal(modalKey);
  return isVisible ? createPortal(<Overlay {...rest}>{children}</Overlay>, document.body) : null;
};

// #region styles
const Overlay = styled(Flex)`
  top: 0;
  left: 0;
  bottom: 0;
  right: 0;
  position: fixed;

  align-items: center;
  background-color: rgba(0, 0, 0, 0.8);
  display: flex;
  justify-content: center;
  z-index: 100;

  ${flexbox};

  * {
    box-sizing: border-box;
  }
`;
// #endregion
