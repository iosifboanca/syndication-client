import { createAction } from "typesafe-actions";
import { NavigationState } from "src/services";

export const navigate = createAction(
  'navigation/NAVIGATE',
  (path: string, params?: object) => ({ path, params })
)();

export const setState = createAction(
  'navigation/SET_STATE',
  (state: NavigationState) => state
)();
