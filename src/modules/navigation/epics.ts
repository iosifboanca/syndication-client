import { filter, tap, ignoreElements, map, mergeMapTo } from 'rxjs/operators';
import { RootEpic, isActionOf } from 'typesafe-actions';
import { setState, navigate } from './actions';
import { actions as systemActions } from 'src/modules/system';

export const initEpic: RootEpic = (action$, _, { navigation }) =>
  action$.pipe(
    filter(isActionOf(systemActions.init)),
    mergeMapTo(navigation.state$),
    map(setState),
  );

export const navigateEpic: RootEpic = (action$, _, { navigation }) =>
  action$.pipe(
    filter(isActionOf(navigate)),
    tap(({ payload }) => navigation.goTo(payload.path, payload.params)),
    ignoreElements(),
  );
