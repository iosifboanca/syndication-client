import React from 'react';
import { currentPath } from '../selectors';
import { useSelector } from 'react-redux';

interface Props {
  path: string;
  children: React.ReactElement;
}

export const Route: React.FC<Props> = (props) => {
  const path = useSelector(currentPath);

  if (path === props.path) {
    return props.children;
  }

  return null;
};
