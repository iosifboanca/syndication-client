import { createReducer } from "typesafe-actions";
import { setState } from './actions';

interface NavigationState {
  path: string;
  params?: object;
}

const root: NavigationState = {
  path: '/',
  params: {}
};

const state = createReducer(root)
  .handleAction(setState, (state, action) =>
    action.payload
  );

export default state;
