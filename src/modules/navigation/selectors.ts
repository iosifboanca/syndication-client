import { RootState } from 'typesafe-actions';
import { createSelector } from 'reselect';

const root = (state: RootState) => state.navigation;

export const currentPath = createSelector(
  root,
  state => state.path
);
