import { createReducer } from 'typesafe-actions';
import { combineReducers } from 'redux';

import { init } from './actions';

const inited = createReducer(false as boolean)
  .handleAction(init, (state) => true);

const root = combineReducers({
  inited,
});

export default root;
