import { ignoreElements, filter, tap } from 'rxjs/operators';
import { isActionOf, RootEpic } from 'typesafe-actions';
import { init } from './actions';

export const initEpic: RootEpic = (action$, state$, { auth }) =>
  action$.pipe(
    filter(isActionOf(init)),
    tap(() => auth.init()),
    ignoreElements(),
  );
