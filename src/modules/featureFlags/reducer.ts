import { createReducer } from 'typesafe-actions';

import { toggleFeature } from './actions';

type FlagsState = Record<string, boolean>;

const flags = createReducer({} as FlagsState).handleAction(toggleFeature, (state, action) => ({
  ...state,
  [action.payload]: !state[action.payload],
}));

export default flags;
