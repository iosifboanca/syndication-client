import { createAction } from 'typesafe-actions';

export const toggleFeature = createAction('system/SET_FEATURE_FLAG')<string>();
