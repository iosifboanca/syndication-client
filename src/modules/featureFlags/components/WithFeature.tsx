import React, { Fragment } from 'react';
import { useSelector } from 'react-redux';

import { features } from '../selectors';

interface Props {
  feature: string;
}

export const WithFeature: React.FunctionComponent<Props> = ({ children, feature }) => {
  const activeFeatures = useSelector(features);

  return activeFeatures.includes(feature) ? <Fragment>{children} </Fragment> : null;
};
