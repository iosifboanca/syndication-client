import { createSelector } from 'reselect';
import { RootState } from 'typesafe-actions';

const root = (state: RootState) => state.flags;

export const features = createSelector(root, flags =>
  Object.entries(flags)
    .filter(([key, value]) => value)
    .map(([key]) => key),
);
