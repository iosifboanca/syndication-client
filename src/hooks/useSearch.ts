import { useState, useCallback } from 'react';

type UseSearch = [string, (e: any) => void];

export const useSearch = (initialValue: string = ''): UseSearch => {
  const [searchValue, changeSearchValue] = useState(initialValue);

  const handleSearchChange = useCallback(e => {
    changeSearchValue(e.target.value.toLowerCase());
  }, []);

  return [searchValue, handleSearchChange];
};
