import { Config } from './Config';
import { AuthService, NavigationService, GraphqlService } from './services';

export class Context {
  public auth: AuthService;
  public graphql: GraphqlService;
  public navigation: NavigationService;

  constructor(public readonly config: Config) {
    this.graphql = new GraphqlService(config);
    this.auth = new AuthService(config, this.graphql);
    this.navigation = new NavigationService(config);
  }
}
