import React, { Fragment } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Route } from 'react-router-dom';

import { SessionRole } from 'src/domain';
import { useNavigation } from 'src/hooks';
import { Header, GlobalStyle, Button } from 'src/ui';
import { ToastContainer } from 'src/modules/notifications';
import { actions, WithFeature } from 'src/modules/featureFlags';
import { BatchesDashboard, BatchDetails } from 'src/modules/batches';
import { IndexersDashboard, IndexerDetails } from 'src/modules/indexers';
import { JournalsDashboard, JournalDetails } from 'src/modules/journals';
import { LoginWidget, WithRoles, Home, NoRoles } from 'src/modules/session';
import { actions as batchActions, selectors as batchSelectors } from 'src/modules/batches';

const App: React.FC = () => {
  const dispatch = useDispatch();
  const { goTo } = useNavigation();

  window.toggleFeature = function(feature: string) {
    dispatch(actions.toggleFeature(feature));
  };

  const batch = useSelector(batchSelectors.singleBatch);

  return (
    <Fragment>
      <GlobalStyle />
      <ToastContainer />

      <Header title='Syndication' onClick={goTo('/')} />

      <WithFeature feature={'syndication-on'}>
        {batch ? (
          <Button onClick={() => dispatch(batchActions.commitBatch.request(batch.id))}>
            SYNDICATE!
          </Button>
        ) : null}
      </WithFeature>

      <WithRoles roles={[SessionRole.ANONYMOUS]}>
        <LoginWidget />
      </WithRoles>

      <WithRoles roles={[SessionRole.NONE]}>
        <NoRoles />
      </WithRoles>

      <WithRoles roles={[SessionRole.ADMIN]}>
        <Route path='/' exact component={Home} />

        <Route path='/indexers' exact component={IndexersDashboard} />
        <Route path='/indexers/:indexerId' exact component={IndexerDetails} />

        <Route path='/journals' exact component={JournalsDashboard} />
        <Route exact path='/journals/:journalId' component={JournalDetails} />

        <Route path='/batches' exact component={BatchesDashboard} />
        <Route path='/batches/:batchId' exact component={BatchDetails} />
      </WithRoles>
    </Fragment>
  );
};

export default App;
