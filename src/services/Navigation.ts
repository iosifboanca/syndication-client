import { Config } from 'src/Config';
import base64 from 'base-64';
import { createBrowserHistory, History, Location } from 'history';
import { fromEventPattern, BehaviorSubject } from 'rxjs';
import { map } from 'rxjs/operators';

export interface NavigationState {
  path: string;
  params?: object;
}

export class NavigationService {
  private history: History;
  public state$: BehaviorSubject<NavigationState>;

  constructor(config: Config) {
    this.history = createBrowserHistory();
    const location$ = fromEventPattern(
      handler => this.history.listen(handler),
      (handler, unlisten) => unlisten(),
    ).pipe(
      map(loc => (loc as any)[0] as Location),
      map(location => this.parseLocation(location)),
    );

    this.state$ = new BehaviorSubject(this.parseLocation(this.history.location));
    location$.subscribe(this.state$);
  }

  goTo(path: string, params?: object) {
    const search = params ? this.encodeSearch(params) : '';
    const url = `${path}/${search}`;
    this.history.push(url);
  }

  private parseLocation(location: Location): NavigationState {
    const parts = location.pathname.split('/');
    const params = this.decodeSearch(parts.pop());
    const path = parts.join('/');

    return {
      path,
      params,
    };
  }

  private encodeSearch(params: object): string {
    if (!Object.keys(params).length) {
      return '';
    }

    return base64.encode(JSON.stringify(params));
  }

  private decodeSearch(str: string | undefined): object {
    let result;

    if (!str) {
      return {};
    }

    // Make it work with react-router non-base64 params
    // TODO: should change in the future
    try {
      result = JSON.parse(base64.decode(str));
    } catch (err) {}

    return result;
  }
}
