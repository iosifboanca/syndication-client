import { BehaviorSubject } from 'rxjs';
import Keycloak from 'keycloak-js';
import { Config } from 'src/Config';
import { GraphqlService } from './graphql';
import { Session, emptySession } from 'src/domain';

export enum AuthStatus {
  LOGGED_OUT = 'LOGGED_OUT',
  IN_PROGRESS = 'IN_PROGRESS',
  LOGGED_IN = 'LOGGED_IN',
}

export interface AuthState {
  status: AuthStatus;
  session: Session;
  token?: string;
}

export class AuthService {
  public state$: BehaviorSubject<AuthState>;
  private keycloak: Keycloak.KeycloakInstance<'native'>;

  constructor(config: Config, private readonly graphqlService: GraphqlService) {
    this.state$ = new BehaviorSubject<AuthState>({
      status: AuthStatus.LOGGED_OUT,
      session: emptySession,
    });

    this.keycloak = Keycloak<'native'>(config.keycloak);

    this.keycloak.onAuthSuccess = () => this.handleAuthSuccess();
    this.keycloak.onAuthError = err => this.handleAuthError(err);
  }

  init() {
    return this.keycloak.init({
      onLoad: 'check-sso',
      silentCheckSsoRedirectUri: window.location.origin + '/check-sso.html',
      promiseType: 'native',
    });
  }

  login() {
    this.keycloak.init({
      onLoad: 'login-required',
      checkLoginIframe: true,
      promiseType: 'native',
    });
  }

  private async handleAuthSuccess() {
    const { keycloak } = this;
    const token = keycloak.token;
    if (token) {
      try {
        const res = await this.graphqlService.session(token);

        this.state$.next({
          status: AuthStatus.LOGGED_IN,
          token: res.token,
          session: new Session(res),
        });
      } catch (err) {
        console.log('handle this later');
      }
    }
  }

  private handleAuthError(err: any) {
    this.state$.next({
      status: AuthStatus.LOGGED_OUT,
      session: emptySession,
    });
  }
}
