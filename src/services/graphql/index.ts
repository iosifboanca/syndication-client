import { print, ASTNode } from 'graphql';
import { GraphQLClient } from 'graphql-request';

import { Config } from 'src/Config';
import { SESSION, SessionResponse } from './mutations';

interface QueryResponse {
  session: SessionResponse;
}

const identity = (x: any) => x;

export class GraphqlService {
  private client: GraphQLClient;

  constructor(private readonly config: Config) {
    this.client = new GraphQLClient(config.gqlUrl, { headers: {} });
  }

  setAuthorization(token: string): void {
    this.client.setHeader('Authorization', `Bearer ${token}`);
  }

  private parseGQLErrorResponse(error: any) {
    throw new Error(error.response.errors[0].message);
  }

  async session(token: string): Promise<SessionResponse> {
    const out = await this.client.request<QueryResponse>(SESSION, { token });
    return out.session;
  }

  async request<O>(gql: ASTNode, variables?: any): Promise<O> {
    return this.client.request(print(gql), variables).then(identity, this.parseGQLErrorResponse);
  }
}
