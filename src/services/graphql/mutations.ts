import { SessionRole } from 'src/domain';

export const SESSION = `
  query session($token: String!) {
    session(token: $token) {
      name
      email
      roles
      token
    }
  }
`;

export interface SessionResponse {
  name: string;
  email: string;
  roles: SessionRole[];
  token: string;
}
