import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { ThemeProvider } from 'styled-components';
import { BrowserRouter as Router } from 'react-router-dom';
import 'react-calendar/dist/Calendar.css';

import './index.css';
import App from './App';
import { theme } from 'src/ui';
import * as serviceWorker from './serviceWorker';
import { Config } from './Config';
import { Context } from './Context';
import makeStore from './redux';
import { initAction } from './redux';

const config = new Config();
const context = new Context(config);

const store = makeStore(context);

ReactDOM.render(
  <Provider store={store}>
    <Router>
      <ThemeProvider theme={theme}>
        <App />
      </ThemeProvider>
    </Router>
  </Provider>,
  document.getElementById('root'),
  () => {
    store.dispatch(initAction());
  },
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
