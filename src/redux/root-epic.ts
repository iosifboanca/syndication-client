import { combineEpics } from 'redux-observable';

import { epics as system } from 'src/modules/system';
import { epics as session } from 'src/modules/session';
import { epics as batches } from 'src/modules/batches';
import { epics as indexers } from 'src/modules/indexers';
import { epics as journals } from 'src/modules/journals';
import { epics as databases } from 'src/modules/databases';
import { epics as navigation } from 'src/modules/navigation';
import { epics as publishers } from 'src/modules/publishers';
import { epics as notifications } from 'src/modules/notifications';

export default combineEpics(
  ...Object.values(system),
  ...Object.values(session),
  ...Object.values(batches),
  ...Object.values(journals),
  ...Object.values(indexers),
  ...Object.values(databases),
  ...Object.values(navigation),
  ...Object.values(publishers),
  ...Object.values(notifications),
);
