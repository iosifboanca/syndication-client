import { compose } from 'redux';
import { createReducer, createAsyncAction } from 'typesafe-actions';

import { actions as modalActions } from 'src/modules/modal';

type AsyncAction = ReturnType<ReturnType<typeof createAsyncAction>>;

export const composeEnhancers =
  (process.env.NODE_ENV === 'development' &&
    window &&
    window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__) ||
  compose;

export interface OperationStatus {
  success?: Date | undefined;
  loading: boolean;
  error?: string | undefined;
}

export const operationStatusInitialState: OperationStatus = {
  loading: false,
};

export const operationStatusReducerCreator = (actions: AsyncAction) =>
  createReducer(operationStatusInitialState)
    .handleAction(modalActions.hideModal, () => operationStatusInitialState)
    .handleAction(actions.request, () => ({
      loading: true,
      error: undefined,
      success: undefined,
    }))
    .handleAction(actions.failure, (_, action) => ({
      error: action.payload.message,
      loading: false,
      success: undefined,
    }))
    .handleAction(actions.success, () => ({
      loading: false,
      error: undefined,
      success: new Date(),
    }));
