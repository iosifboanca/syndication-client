// import { routerActions } from 'react-router-redux';
import { actions as flags } from 'src/modules/featureFlags';

import { actions as modal } from 'src/modules/modal';
import { actions as system } from 'src/modules/system';
import { actions as batches } from 'src/modules/batches';
import { actions as filters } from 'src/modules/filters';
import { actions as session } from 'src/modules/session';
import { actions as journals } from 'src/modules/journals';
import { actions as indexers } from 'src/modules/indexers';
import { actions as databases } from 'src/modules/databases';
import { actions as navigation } from 'src/modules/navigation';
import { actions as publishers } from 'src/modules/publishers';
import { actions as notifications } from 'src/modules/notifications';

export default {
  // router: routerActions,
  flags,
  modal,
  system,
  batches,
  filters,
  session,
  journals,
  indexers,
  databases,
  navigation,
  publishers,
  notifications,
};
