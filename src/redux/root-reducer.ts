import { combineReducers } from 'redux';
// import { routerReducer } from 'react-router-redux';
import { reducer as modal } from 'src/modules/modal';
import { reducer as system } from 'src/modules/system';
import { reducer as batches } from 'src/modules/batches';
import { reducer as filters } from 'src/modules/filters';
import { reducer as session } from 'src/modules/session';
import { reducer as journals } from 'src/modules/journals';
import { reducer as indexers } from 'src/modules/indexers';
import { reducer as flags } from 'src/modules/featureFlags';
import { reducer as databases } from 'src/modules/databases';
import { reducer as navigation } from 'src/modules/navigation';
import { reducer as publishers } from 'src/modules/publishers';
import { reducer as notifications } from 'src/modules/notifications';

const rootReducer = combineReducers({
  // router: routerReducer,
  flags,
  modal,
  system,
  batches,
  filters,
  session,
  journals,
  indexers,
  databases,
  navigation,
  publishers,
  notifications,
});

export default rootReducer;
