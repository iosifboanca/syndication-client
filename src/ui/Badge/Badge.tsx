import React from "react";
import { SpaceProps } from "styled-system";

import { Badge as Root } from "./Badge.styles";

export interface BadgeProps extends SpaceProps {
  inverse?: boolean;
  children?: React.ReactNode;
}

export const Badge: React.FunctionComponent<BadgeProps> = ({
  children,
  inverse,
  ...rest
}) => {
  return (
    <Root inverse={inverse} {...rest}>
      {children}
    </Root>
  );
};

Badge.defaultProps = {
  inverse: false
};
