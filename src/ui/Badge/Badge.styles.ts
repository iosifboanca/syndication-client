import styled, { css } from 'styled-components';
import { space } from 'styled-system';

import { th } from '../Theme';
import { bold } from '../Typography';
import { BadgeProps } from './Badge';

const badgeStyles = ({ inverse }: BadgeProps) => {
  if (inverse) {
    return css`
      border: 1px solid ${th('colors.disabled')};
      background-color: ${th('colors.transparent')};
      color: ${th('colors.textPrimary')};
    `;
  }
  return css`
    background-color: ${th('colors.textPrimary')};
    color: ${th('colors.white')};
  `;
};

export const Badge = styled.span<BadgeProps>`
  align-items: center;
  border-radius: ${th('gridUnit')};
  display: inline-flex;
  justify-content: center;
  font-size: ${th('fontSizes.labelSmall')};
  line-height: ${th('fontSizes.lineHeight')};
  padding: ${th('gridUnit')};

  ${bold};
  ${badgeStyles};
  ${space};
`;
