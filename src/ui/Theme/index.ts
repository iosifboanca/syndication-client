import { createGlobalStyle } from 'styled-components';

import './fonts/index.css';
import colors from './colors';
import buttons from './buttons';
import { generateSpaces } from './helpers';
import { fontSizes, defaultFont } from './typography';

const GRID_UNIT = 4;

export const theme = {
  colors,
  buttons,
  fontSizes,
  fontFamily: defaultFont,
  gridUnit: `${GRID_UNIT}px`,
  space: generateSpaces(GRID_UNIT),
  sizes: generateSpaces(GRID_UNIT),
};

export const GlobalStyle = createGlobalStyle`
  * {
    box-sizing: border-box;
  }
`;

export * from './helpers';
