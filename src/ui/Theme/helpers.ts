import Color from 'color';
import { get } from 'lodash';
import { css } from 'styled-components';

export interface Theme {
  colors: {
    actionPrimary: string;
    actionSecondary: string;
    background: string;
    furniture: string;
    info: string;
    border: string;
    textPrimary: string;
    textSecondary: string;
    transparent: string;
    warning: string;
    white: string;
    statusPending: string;
    statusInvite: string;
    statusApproved: string;
    disabled: string;
  };
  gridUnit: string;
  sizes: number[];
}

const generateSpaces = (gridUnit: number = 4): number[] => {
  return Array.from({ length: 100 }, (_, index) => index * gridUnit);
};

const th = (themePath: string = '') => ({ theme }: { theme: Theme }): string | number => {
  return get(theme, themePath, themePath);
};

const normalizePercent = (num: number) => (num >= 1 && num <= 100 ? num / 100 : num);

const darkenLighten = (original: string, percent: number, theme: Theme, dark?: boolean) => {
  const color = get(theme, original) || original;
  const thisMuch = normalizePercent(percent);

  let converted: Color;
  try {
    converted = Color(color);
  } catch (_) {
    converted = Color('black');
  }

  if (dark) return converted.darken(thisMuch).string();
  return converted.lighten(thisMuch).string();
};

const darken = (original: string, percent: number) => ({ theme }: { theme: Theme }) =>
  darkenLighten(original, percent, theme, true);

const lighten = (original: string, percent: number) => ({ theme }: { theme: Theme }) =>
  darkenLighten(original, percent, theme);

const ellipsis = () => {
  return css`
    overflow: hidden;
    text-overflow: ellipsis;
    white-space: nowrap;
  `;
};

export { darken, lighten, th, generateSpaces, ellipsis };
