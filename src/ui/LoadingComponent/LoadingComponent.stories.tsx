import React from 'react';

import { LoadingComponent } from './LoadingComponent';

export const Default = () => <LoadingComponent loading={true} />;

export const WithError = () => (
  <LoadingComponent loading={false} error='Oops! Something went wrong...' />
);

export const NoError = () => (
  <LoadingComponent loading={false}>
    <div>I finished loading.</div>
  </LoadingComponent>
);

export default {
  title: 'Components|LoadingComponent',
  component: LoadingComponent,
};
