import React, { Fragment } from 'react';

import { Flex } from '../Flex';
import { Loader } from '../Loader';
import { Text } from '../Typography';

export interface LoadingComponentProps {
  loading: boolean;
  error?: string;
}

export const LoadingComponent: React.FunctionComponent<LoadingComponentProps> = ({
  error,
  loading,
  children,
}) => {
  if (loading) {
    return (
      <Flex>
        <Loader mt={4} size={8} />
      </Flex>
    );
  }

  if (error) {
    return (
      <Flex>
        <Text mt={4} variant='warning'>
          {error}
        </Text>
      </Flex>
    );
  }

  return <Fragment>{children}</Fragment>;
};
