import React, { Fragment } from 'react';
import styled from 'styled-components';
import { SpaceProps, FlexboxProps } from 'styled-system';

import { Flex } from '../Flex';
import { Icon } from '../Icon';
import { Text } from '../Typography';

export interface BreadcrumbProps extends SpaceProps, FlexboxProps {
  goBack(): any;
  label?: string;
  backLabel: string;
  entityName?: string;
  separator?: string;
}

export const Breadcrumb: React.FunctionComponent<BreadcrumbProps> = ({
  label,
  goBack,
  backLabel,
  entityName,
  separator = '/',
  ...rest
}) => {
  return (
    <Root justifyContent='flex-start' onClick={goBack} flex={1} {...rest}>
      <BackIcon name='breadcrumbs' color='colors.textPrimary' />
      <Text ml={1}>{backLabel}</Text>

      {label && (
        <Fragment>
          <Text ml={4}>{`${label} ${separator}`}</Text>
          {entityName && (
            <Text ml={1} fontWeight='bold'>
              {entityName}
            </Text>
          )}
        </Fragment>
      )}
    </Root>
  );
};

// #region styles
const BackIcon = styled(Icon)`
  margin-bottom: 2px;
`;

const Root = styled(Flex)`
  cursor: pointer;
  width: fit-content;
`;
// #endregion
