import React from 'react';
import { action } from '@storybook/addon-actions';
import { text } from '@storybook/addon-knobs';

import { Breadcrumb } from './Breadcrumb';

export const Default = () => (
  <Breadcrumb backLabel='BACK' label='Publishers' entityName='Hindawi' goBack={action('Go back')} />
);

export const NoLabel = () => <Breadcrumb backLabel='BACK' goBack={action('Go back')} />;

export const DifferentBacklabel = () => (
  <Breadcrumb goBack={action('Go back')} backLabel={text('Back label', 'BACK')} />
);

export default {
  title: 'Components|Breadcrumb',
  component: Breadcrumb,
};
