import React, { useCallback, useState } from 'react';
import styled from 'styled-components';

import { th } from '../Theme';
import { Flex } from '../Flex';
import { Input } from '../FormElements';
import { TextHighlight } from '../TextHighlight';
import { Label, Title, Text } from '../Typography';
import {FormFieldStatus} from '../FormElements/CommonTypes'

export interface PathInputProps {
  label?: string;
  name?: string;
  value?: string;
  onChange?: any;
  onBlur?: any;
  status?: FormFieldStatus;
  tokens: Record<string, string>;
}

export const PathInput: React.FunctionComponent<PathInputProps> = ({
  name,
  label,
  tokens,
  onBlur,
  status,
  onChange,
  value = '',
}) => {
  const [focused, setFocus] = useState(false);
  const [inputValue, setValue] = useState(value);

  const handleFocus = () => setFocus(true);

  const handleBlur = useCallback(
    e => {
      setFocus(false);
      if (typeof onBlur === 'function') {
        onBlur(e);
      }
    },
    [onBlur],
  );

  const handleChange = useCallback(
    e => {
      const value = e.target.value;
      setValue(value);

      if (typeof onChange === 'function') {
        onChange(e);
      }
    },
    [onChange],
  );

  return (
    <Root vertical>
      <Input
        value={inputValue}
        onBlur={handleBlur}
        name={name as string}
        onFocus={handleFocus}
        onChange={handleChange}
        status={status}
      />
      {inputValue && (
        <Flex justifyContent='flex-start' height={8}>
          {label && <Label mr={2}>{label}</Label>}
          <TextHighlight keywords={Object.keys(tokens)}>{inputValue}</TextHighlight>
        </Flex>
      )}
      {focused && (
        <InfoContainer vertical px={2} py={1} mt={inputValue ? 5 : 0}>
          <Flex vertical mb={1}>
            <Title variant='small'>Available tokens</Title>
            <Text>
              These tokens will be replaced with real values from the corresponding entities.
            </Text>
          </Flex>
          {Object.entries(tokens).map(([token, description]) => (
            <Flex key={token}>
              <Flex flex={1} justifyContent='flex-start'>
                <Label>{token}</Label>
              </Flex>
              <Flex flex={2} justifyContent='flex-start'>
                <Text>{description}</Text>
              </Flex>
            </Flex>
          ))}
        </InfoContainer>
      )}
    </Root>
  );
};

// #region styles
const Root = styled(Flex)`
  position: relative;
`;

const InfoContainer = styled(Flex)`
  background-color: ${th('colors.white')};
  border-radius: ${th('gridUnit')};
  box-shadow: 0 0 calc(${th('gridUnit')} * 2) rgba(51, 51, 51, 0.11);
  width: 100%;
  z-index: 2;

  position: absolute;
  top: calc(${th('gridUnit')} * 10);
`;
// #endregion
