import * as React from 'react';

import { PathInput } from './PathInput';

const tokens = {
  '{journalCode}': 'Journal code.',
  '{publicationYear}': 'Article publication year.',
  '{fileName}': 'Article materials name.',
  '{articleId}': 'Article custom ID.',
};

const pathOnFTPTokens = {
  '{journalCode}': 'Journal code.',
  '{articleId}': 'Article custom ID.',
  '{publisher}': 'The publisher name.',
  '{syndicationYear}': 'Article syndication year.',
  '{syndicationMonth}': 'Article syndication month.',
  '{syndicationDay}': 'Article syndication day.',
  '{publicationYear}': 'Article publication year.',
  '{publicationMonth}': 'Article publication month.',
  '{publicationDay}': 'Article publication day.',
};

export const PathInsideZip = () => <PathInput tokens={tokens} />;

export const PathOnFTP = () => <PathInput tokens={pathOnFTPTokens} />;

export default {
  title: 'Components|PathInput',
  component: PathInput,
};
