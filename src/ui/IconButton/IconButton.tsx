import React from "react";
import { SpaceProps } from "styled-system";

import { Icon } from "../Icon";
import { IconNames } from "../Icon/Icon.types";
import { IconButton as Root } from "./IconButton.styles";

export interface StyledIconButtonProps {
  highlight?: boolean;
}

export interface IconButtonProps extends StyledIconButtonProps, SpaceProps {
  name: IconNames;
  label?: string;
  disabled?: boolean;
  onClick?(e: React.MouseEvent<HTMLElement>): void;
}

export const IconButton: React.FunctionComponent<IconButtonProps> = ({
  name,
  label,
  onClick,
  ...rest
}) => {
  return (
    <Root aria-label={label} role="button" onClick={onClick} {...rest}>
      <Icon name={name} />
    </Root>
  );
};

IconButton.defaultProps = {
  label: "Icon button"
};
