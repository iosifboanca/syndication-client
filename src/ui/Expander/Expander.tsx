import React, { useCallback, useState } from 'react';
import styled from 'styled-components';
import { FlexboxProps, SpaceProps } from 'styled-system';

import { Flex } from '../Flex';
import { Title } from '../Typography';
import { th } from '../Theme/helpers';
import { Icon, IconNames } from '../Icon';

export interface ExpanderProps extends FlexboxProps, SpaceProps {
  title: string;
  startExpanded?: boolean;
  icons?: [IconNames, IconNames];
  rightChildren?: React.ReactNode;
}

export const Expander: React.FunctionComponent<ExpanderProps> = ({
  title,
  children,
  rightChildren,
  startExpanded,
  icons = ['expand', 'collapse'],
  ...rest
}) => {
  const [expanded, setExpanded] = useState(startExpanded);
  const toggle = useCallback(() => setExpanded(expanded => !expanded), []);

  return (
    <Root vertical expanded={expanded} {...rest}>
      <Header
        px={3}
        onClick={toggle}
        expanded={expanded}
        justifyContent={rightChildren ? 'space-between' : 'flex-start'}
      >
        <Flex justifyContent='flex-start' py={2}>
          <Icon name={icons[expanded ? 1 : 0]} color='colors.actionSecondary' mr={3} />
          <Title variant='small'>{title}</Title>
        </Flex>
        {typeof rightChildren === 'function' ? rightChildren() : rightChildren}
      </Header>
      {expanded && children}
    </Root>
  );
};

Expander.defaultProps = {
  icons: ['expand', 'collapse'],
};

// #region styles
interface ExpanderState {
  expanded?: boolean;
}

const Header = styled(Flex)<ExpanderState>`
  background-color: ${th('colors.background')};
  border-top-left-radius: ${th('gridUnit')};
  border-top-right-radius: ${th('gridUnit')};
  border-bottom-left-radius: ${props => (props.expanded ? 0 : th('gridUnit'))};
  border-bottom-right-radius: ${props => (props.expanded ? 0 : th('gridUnit'))};
  cursor: pointer;
`;

const Root = styled(Flex)<ExpanderState>`
  border-radius: ${th('gridUnit')};
  border: 1px solid ${th('colors.furniture')};

  flex: ${props => (props.expanded ? props.flex : 0)};
`;
// #endregion
