import React from 'react';

import { Flex } from '../';
import { Expander } from './Expander';

export const Default = () => (
  <Expander title='Default Expander'>
    <Flex m={3}>Here are the children!</Flex>
  </Expander>
);

export const DifferentIcons = () => (
  <Expander icons={['impersonate', 'info']} title='With Different Icons'>
    <Flex m={3}>Here are the children!</Flex>
  </Expander>
);

const Details = () => <Flex justifyContent='flex-end'>i am on the right</Flex>;

export const WithRightChildren = () => (
  <Expander rightChildren={<Details />} title='Right Children'>
    <Flex m={3}>Here are the children!</Flex>
  </Expander>
);

export default {
  title: 'Components|Expander',
  component: Expander,
};
