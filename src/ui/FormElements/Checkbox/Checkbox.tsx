import React from 'react';

import { Label } from '../../Typography';
import { Flex, FlexProps } from '../../Flex';

interface Props extends FlexProps {
  id?: string;
  label?: string;
  required?: boolean;
  onChange(e: any): void;
  checked: boolean;
}

export const Checkbox: React.FunctionComponent<Props> = ({
  id,
  label,
  required,
  onChange,
  checked = false,
}) => {
  return (
    <Flex>
      <input id={id} type='checkbox' required={required} onChange={onChange} checked={checked} />
      <Label ml={1} htmlFor={id} required={required}>
        {label}
      </Label>
    </Flex>
  );
};
