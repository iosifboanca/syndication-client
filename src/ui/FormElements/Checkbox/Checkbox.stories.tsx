import React from 'react';

import { Checkbox } from './Checkbox';

export const Default = () => (
  <div
    style={{
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'flex-start',
    }}
  >
    <Checkbox checked onChange={() => {}} id='ketchup' label='Ketchup' mb={1} />
    <Checkbox checked onChange={() => {}} id='mayo' label='Mayo' mb={1} />
    <Checkbox checked onChange={() => {}} id='ranch' label='Ranch' mb={1} />
    <Checkbox checked onChange={() => {}} id='bbq' label='Barbeque' mb={1} />
  </div>
);

export default {
  title: 'Form Elements|Checkbox',
  component: Checkbox,
};
