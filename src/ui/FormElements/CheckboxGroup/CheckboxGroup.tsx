import React from 'react';
import { Field } from 'formik';
import { FlexboxProps, SpaceProps } from 'styled-system';

import { Flex, Label } from '../../';

export interface CheckboxGroupProps extends FlexboxProps, SpaceProps {
  name: string;
  options: any[];
  optionKey: ((option: any) => string) | string;
  optionLabel: ((option: any) => string) | string;
  optionValue: ((option: any) => string) | string;
}

export const CheckboxGroup: React.FC<CheckboxGroupProps> = ({
  name,
  options,
  optionKey,
  optionLabel,
  optionValue,
  ...rest
}) => {
  return (
    <Flex justifyContent='flex-start' mt={1} flexWrap='wrap' {...rest}>
      {options.map(option => {
        const key = typeof optionKey === 'function' ? optionKey(option) : option[optionKey];
        const label = typeof optionLabel === 'function' ? optionLabel(option) : option[optionLabel];
        const value = typeof optionValue === 'function' ? optionValue(option) : option[optionValue];
        return (
          <Flex justifyContent='flex-start' key={key} flexBasis='33%'>
            <Field id={key} type='checkbox' name={name} value={value} />
            <Label ml={1} htmlFor={key}>
              {label}
            </Label>
          </Flex>
        );
      })}
    </Flex>
  );
};
