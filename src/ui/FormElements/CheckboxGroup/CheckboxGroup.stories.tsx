import React from 'react';
import { Formik } from 'formik';

import { CheckboxGroup } from './CheckboxGroup';

const options = [
  { id: 'option-1', value: 'one' },
  { id: 'option-2', value: 'two' },
  { id: 'option-3', value: 'three' },
];

const countries = [
  { id: 'ctry-1', value: 'RO', label: 'Romania' },
  { id: 'ctry-2', value: 'SW', label: 'Sweden' },
  { id: 'ctry-3', value: 'UK', label: 'United Kingdom' },
];

export const Default = () => (
  <Formik initialValues={{}} onSubmit={console.log}>
    {({ values }) => (
      <div>
        <CheckboxGroup
          name='options'
          options={options}
          optionKey={o => o.id}
          optionLabel={o => o.value}
          optionValue={o => o.value}
        />
        <CheckboxGroup
          name='country'
          optionKey={'id'}
          optionValue='value'
          optionLabel='label'
          options={countries}
        />

        <h5>values:</h5>
        <code>{JSON.stringify(values)}</code>
      </div>
    )}
  </Formik>
);

export default {
  title: 'Form Elements|CheckboxGroup',
  component: CheckboxGroup,
};
