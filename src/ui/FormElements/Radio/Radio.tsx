import React from 'react';

import { Label } from '../../Typography';
import { FlexProps, Flex } from '../../Flex';

export interface RadioProps extends FlexProps {
  id?: string;
  name?: string;
  label?: string;
  onChange?(e: any): any;
}

export const Radio: React.FunctionComponent<RadioProps> = ({ id, label, name }) => {
  return (
    <Flex>
      <input id={id} name={name} type='radio' />
      <Label ml={1} htmlFor={id}>
        {label}
      </Label>
    </Flex>
  );
};
