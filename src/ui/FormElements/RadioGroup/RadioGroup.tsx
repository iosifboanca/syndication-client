import React from 'react';
import { Field } from 'formik';
import { FlexboxProps, SpaceProps } from 'styled-system';

import { Flex, Label } from '../../';

export interface RadioGroupProps extends FlexboxProps, SpaceProps {
  name: string;
  options: any[];
  itemBasis?: string | number;
  optionKey: ((option: any) => string) | string;
  optionLabel: ((option: any) => string) | string;
  optionValue: ((option: any) => string) | string;
}

export const RadioGroup: React.FC<RadioGroupProps> = ({
  name,
  options,
  optionKey,
  optionLabel,
  optionValue,
  itemBasis,
  ...rest
}) => {
  return (
    <Flex justifyContent='flex-start' mt={1} {...rest}>
      {options.map(option => {
        const key = typeof optionKey === 'function' ? optionKey(option) : option[optionKey];
        const label = typeof optionLabel === 'function' ? optionLabel(option) : option[optionLabel];
        const value = typeof optionValue === 'function' ? optionValue(option) : option[optionValue];
        return (
          <Flex justifyContent='flex-start' flexBasis={itemBasis} key={key}>
            <Field id={key} type='radio' name={name} value={value} />
            <Label ml={1} htmlFor={key}>
              {label}
            </Label>
          </Flex>
        );
      })}
    </Flex>
  );
};
