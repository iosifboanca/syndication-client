import { FieldValidator } from 'formik';

const emailRegex = new RegExp(
  /^(([^<>()[\].,;:\s@"]+(\.[^<>()[\].,;:\s@"]+)*)|(".+"))@(([^<>()[\].,;:\s@"]+\.)+[^<>()[\].,;:\s@"]{2,})$/i,
);

export const isRequired: FieldValidator = value => {
  if (value === null || value === undefined) {
    return 'Required';
  }

  if (typeof value === 'string' && value.trim() === '') {
    return 'Required';
  }

  if (Array.isArray(value) && !value.length) {
    return 'Required';
  }
};

export const isEmail: FieldValidator = value => {
  if (!value) return;
  return emailRegex.test(value) ? undefined : 'Invalid email';
};
