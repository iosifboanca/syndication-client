import React from "react";

import { FormFieldProps } from "../CommonTypes";
import { Textarea as Input } from "./Textarea.styles";

type ResizeOptions = "both" | "none" | "vertical" | "horizontal";

export interface TextareaProps extends FormFieldProps {
  id?: string;
  placeholder?: string;
  resize?: ResizeOptions;
}

export const Textarea: React.FunctionComponent<TextareaProps> = ({
  status,
  resize,
  ...rest
}) => {
  return <Input as="textarea" status={status} resize={resize} {...rest} />;
};

Textarea.defaultProps = {
  resize: "both"
};
