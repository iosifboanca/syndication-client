import styled from "styled-components";

import { Input } from "../CommonStyles";

import { TextareaProps } from "./Textarea";

export const Textarea = styled(Input)<TextareaProps>`
  resize: ${({ resize }) => resize};
`;
