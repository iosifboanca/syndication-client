import React from 'react';
import { Formik } from 'formik';

import { Button } from '../../Button';
import { FormField } from './FormField';

function isRequiredOne(value: any) {
  if (value.length < 8) {
    return 'Required one';
  }
}
function isRequiredTwo(value: any) {
  if (value.length < 12) {
    return 'Required two';
  }
}

const validateFns = [isRequiredOne, isRequiredTwo];

function submit() {}
const initial = { email: 'alexandru.munt@gmail.com', password: '' };

export const Default: React.FC = () => (
  <Formik initialValues={initial} onSubmit={submit}>
    {({ handleSubmit }) => (
      <div>
        <FormField required label='Email' name='email' validate={validateFns} />
        <FormField label='Password' name='password' />
        <Button variant='secondary' size='medium' onClick={handleSubmit}>
          Submit
        </Button>
      </div>
    )}
  </Formik>
);

export default {
  title: 'Form Elements|FormField',
  component: FormField,
};
