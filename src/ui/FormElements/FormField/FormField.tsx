import React, { PureComponent } from 'react';
import { get } from 'lodash';
import { Field, FieldProps, FormikProps, FieldAttributes, FieldValidator } from 'formik';
import { FlexboxProps, LayoutProps, SpaceProps } from 'styled-system';

import { Input } from '../Input';
import { Flex } from '../../Flex';
import { Label, Text } from '../../Typography';

export interface FormFieldProps extends FlexboxProps, LayoutProps, SpaceProps {
  name: string;
  label?: string;
  component?: any;
  required?: boolean;
  validate?: FieldValidator | FieldValidator[];
}

const hasError = (form: FormikProps<FieldAttributes<any>>, name: string) => {
  return (form.submitCount > 0 || get(form.touched, name)) && get(form.errors, name);
};

export class FormField extends PureComponent<FormFieldProps> {
  validateFn: FieldValidator = (value: any) =>
    Array.isArray(this.props.validate)
      ? this.props.validate.reduce<ReturnType<FieldValidator>>((_, fn) => fn(value), '')
      : typeof this.props.validate === 'function'
      ? this.props.validate(value)
      : undefined;

  render() {
    const { name, label, required, component: Component = Input, ...rest } = this.props;

    return (
      <Field name={name} validate={this.validateFn}>
        {({ field, form }: FieldProps) => {
          const error = hasError(form, name);
          return (
            <Flex alignSelf='stretch' vertical {...rest}>
              <Label required={required} htmlFor={field.name} flexGrow={1}>
                {label}
              </Label>
              <Component name={field.name} status={error ? 'warning' : 'none'} {...field} />
              <Flex minHeight={6} justifyContent='flex-start'>
                {error && <Text variant='warning'>{error}</Text>}
              </Flex>
            </Flex>
          );
        }}
      </Field>
    );
  }
}
