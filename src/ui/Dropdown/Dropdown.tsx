import React, { useCallback, useState, useEffect, useRef } from 'react';
import styled, { css } from 'styled-components';

import { Icon } from '../Icon';
import { Flex } from '../Flex';
import { th, ellipsis } from '../Theme';
import { regular, regularItalic } from '../Typography';

interface DropdownProps {
  value?: any;
  options: any[];
  maxItems?: number;
  optionId?: string;
  optionLabel?: string;
  placeholder?: string;
  clearOptionId?: string;
  onChange(value: any): void;
}

export const Dropdown: React.FunctionComponent<DropdownProps> = ({
  value,
  options,
  optionId,
  onChange,
  maxItems,
  placeholder,
  optionLabel,
  clearOptionId,
}) => {
  const ref: React.MutableRefObject<any> = useRef(null);
  const [opened, setOpen] = useState(false);
  const [selectedValue, select] = useState(value);

  const openMenu = useCallback(() => setOpen(true), [setOpen]);
  const closeMenu = useCallback(() => setOpen(false), [setOpen]);

  useEffect(() => {
    select(value);
  }, [value]);

  const handleSelect = useCallback(
    (value: any) => () => {
      const isClearOption = value[optionId as string] === clearOptionId;

      onChange(isClearOption ? undefined : value);
      select(isClearOption ? undefined : value);
    },
    [onChange, clearOptionId, optionId],
  );

  const handleClickOutside = useCallback(
    (event: MouseEvent): void => {
      if (!opened) return;

      if (ref.current && !ref.current.contains(event.target)) {
        closeMenu();
      }
    },
    [opened, closeMenu],
  );

  const handleEscape = useCallback(
    (event: KeyboardEvent) => {
      if (event.key === 'Escape' && opened) {
        closeMenu();
      }
    },
    [opened, closeMenu],
  );

  useEffect(() => {
    document.addEventListener('click', handleClickOutside);
    document.addEventListener('keydown', handleEscape);

    return () => {
      document.removeEventListener('click', handleClickOutside);
      document.removeEventListener('keydown', handleEscape);
    };
  }, [handleClickOutside, handleEscape]);

  return (
    <Root>
      <MenuOpener onClick={openMenu} ref={ref} selected={selectedValue}>
        {selectedValue ? selectedValue[optionLabel as string] : placeholder}
      </MenuOpener>
      <MenuIcon color='colors.textPrimary' name={opened ? 'caretUp' : 'caretDown'} />
      {opened && (
        <OptionsContainer numberOfItems={Math.min(options.length, maxItems as number)}>
          {options.map(option => (
            <MenuOption
              key={option[optionId as string]}
              onMouseDown={handleSelect(option)}
              isClearOption={option[optionId as string] === clearOptionId}
            >
              {option[optionLabel as string]}
            </MenuOption>
          ))}
        </OptionsContainer>
      )}
    </Root>
  );
};

Dropdown.defaultProps = {
  optionId: 'id',
  optionLabel: 'label',
  placeholder: 'Select...',
  maxItems: 6,
};

interface MenuOption {
  isClearOption: boolean;
}

interface OptionsContainer {
  numberOfItems: number;
}

interface MenuOpener {
  selected: any;
}

// #region styles
const OPTION_HEIGHT = 8;

const MenuIcon = styled(Icon)`
  position: absolute;
  top: calc(${th('gridUnit')} * 2);
  right: calc(${th('gridUnit')} * 2);
`;

const containerHeight = ({ numberOfItems }: OptionsContainer) => css`
  height: calc(${th('gridUnit')} * ${numberOfItems} * ${OPTION_HEIGHT});
`;

const optionStyle = () => css`
  cursor: pointer;
  height: calc(${th('gridUnit')} * ${OPTION_HEIGHT});
  padding: 0 calc(${th('gridUnit')} * 2);
  padding-top: ${th('gridUnit')};
  width: 100%;
`;

const Root = styled(Flex)`
  display: table;
  table-layout: fixed;
  position: relative;
`;

const MenuOpener = styled.div<MenuOpener>`
  border: 1px solid ${th('colors.furniture')};
  border-radius: ${th('gridUnit')};
  font-size: ${th('fontSizes.textRegular')};
  padding-right: calc(${th('gridUnit')} * 8);

  ${optionStyle};
  ${ellipsis};

  ${({ selected }) =>
    selected
      ? css`
          color: ${th('colors.textPrimary')};
          ${regular}
        `
      : css`
          color: ${th('colors.textSecondary')};
          ${regularItalic}
        `};

  &:hover {
    border-color: ${th('colors.textPrimary')};
  }
`;

const OptionsContainer = styled.div<OptionsContainer>`
  background-color: ${th('colors.white')};
  border-radius: ${th('gridUnit')};
  box-shadow: 0 0 ${th('gridUnit')} rgba(36, 36, 36, 0.8);

  overflow-y: auto;
  position: absolute;
  top: calc(${th('gridUnit')} * 10);
  width: 100%;
  z-index: 2;

  ${containerHeight};
  ${regular};
`;

const menuOption = ({ isClearOption }: MenuOption) => {
  return isClearOption
    ? css`
        color: ${th('colors.textSecondary')};
        border-bottom: 1px solid ${th('colors.furniture')};

        ${regularItalic};
      `
    : null;
};

const MenuOption = styled.div<MenuOption>`
  font-size: ${th('fontSizes.textRegular')};
  ${optionStyle};
  ${ellipsis};
  ${menuOption};

  &:hover {
    background-color: ${th('colors.background')};
  }

  &:first-child {
    border-top-left-radius: ${th('gridUnit')};
    border-top-right-radius: ${th('gridUnit')};
  }

  &:last-child {
    border-bottom-left-radius: ${th('gridUnit')};
    border-bottom-right-radius: ${th('gridUnit')};
  }
`;
// #endregion
