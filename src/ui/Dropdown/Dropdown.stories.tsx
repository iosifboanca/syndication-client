import React from 'react';
import { action } from '@storybook/addon-actions';

import { Dropdown } from './Dropdown';

const options = [
  { id: 'opt-1', value: 'value-1', message: 'Value One' },
  { id: 'opt-2', value: 'value-2', message: 'Value Two' },
  { id: 'opt-3', value: 'value-3', message: 'Value Three' },
  { id: 'opt-4', value: 'value-4', message: 'A Very Very Long Label Here' },
];

export const Default = () => (
  <Dropdown onChange={action('Option selected')} optionLabel='message' options={options} />
);

export const WithInitialValue = () => (
  <Dropdown
    value={options[1]}
    onChange={action('Option selected')}
    optionLabel='message'
    options={options}
  />
);

export const WithClearOption = () => (
  <Dropdown
    clearOptionId='none'
    optionLabel='message'
    onChange={action('Option selected')}
    options={[{ id: 'none', value: null, message: 'None' }, ...options]}
  />
);

export default {
  title: 'Components|Dropdown',
  component: Dropdown,
};
