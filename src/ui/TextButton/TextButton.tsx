import React from "react";
import { FlexboxProps, SpaceProps } from "styled-system";

import { TextButton as Root } from "./TextButton.styles";

export interface TextButtonProps extends FlexboxProps, SpaceProps {
  disabled?: boolean;
  children?: React.ReactNode;
  onClick?(e: React.MouseEvent<HTMLElement>): void;
}

export const TextButton: React.FunctionComponent<TextButtonProps> = ({
  children,
  onClick,
  ...rest
}) => {
  return (
    <Root onClick={onClick} {...rest}>
      {children}
    </Root>
  );
};
