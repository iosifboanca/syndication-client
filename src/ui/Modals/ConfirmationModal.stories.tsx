import React from 'react';
import { action } from '@storybook/addon-actions';

import { ConfirmationModal } from './ConfirmationModal';

export const Default = () => (
  <ConfirmationModal
    title='Confirmation modal'
    subtitle='Are you sure you want to do it?'
    onAccept={action('onAccept')}
    onCancel={action('onCancel')}
  />
);

export default {
  title: 'Modals|Confirmation Modal',
  component: ConfirmationModal,
};
