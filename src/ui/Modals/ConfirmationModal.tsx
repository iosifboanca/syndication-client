import React from 'react';
import styled from 'styled-components';

import { Button } from '../Button';
import { Flex } from '../Flex';
import { th } from '../Theme';
import { Title, Text } from '../Typography';

interface Props {
  onCancel(): void;
  onAccept(): void;
  title: string;
  loading?: boolean;
  subtitle?: string;
  acceptButtonLabel?: string;
  cancelButtonLabel?: string;
  error?: string | undefined;
}

export const ConfirmationModal: React.FC<Props> = ({
  title,
  error,
  loading,
  subtitle,
  onAccept,
  onCancel,
  acceptButtonLabel = 'Accept',
  cancelButtonLabel = 'Cancel',
}) => {
  return (
    <Root vertical width={500} p={5} alignItems='center'>
      <Title variant='primary' textAlign='center' whiteSpace='normal'>
        {title}
      </Title>
      {subtitle && <Text mt={5}>{subtitle}</Text>}
      {error && (
        <Text variant='warning' mt={3} textAlign='center'>
          {error}
        </Text>
      )}

      <Flex mt={5}>
        <Button size='large' variant='outline' mr={3} onClick={onCancel}>
          {cancelButtonLabel}
        </Button>
        <Button size='large' variant='primary' ml={3} onClick={onAccept} loading={loading}>
          {acceptButtonLabel}
        </Button>
      </Flex>
    </Root>
  );
};

// #region styles
const Root = styled(Flex)`
  background-color: ${th('colors.white')};
  border-radius: ${th('gridUnit')};
  border: 1px solid ${th('colors.furniture')};
`;
// #endregion
