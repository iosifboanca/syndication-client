import styled from 'styled-components';
import { space, SpaceProps } from 'styled-system';

import { th } from '../Theme';
import { Flex } from '../Flex';

export const Table = styled(Flex)`
  border-radius: ${th('gridUnit')};
  flex: 1;
  height: 100%;
`;

export const ScrollContainer = styled.div<SpaceProps>`
  flex-grow: 1;
  overflow-y: auto;
  height: 0;
  min-height: 0;
  width: 100%;

  ${space};
`;

export const THead = styled(Flex)`
  border-top-left-radius: ${th('gridUnit')};
  border-top-right-radius: ${th('gridUnit')};
  border: 1px solid ${th('colors.furniture')};
  min-height: calc(${th('gridUnit')} * 9);
`;

export const BorderedRow = styled(Flex)`
  border: 1px solid ${th('colors.furniture')};
  border-top: none;
  cursor: pointer;
  min-height: calc(${th('gridUnit')} * 8);

  &:hover {
    background-color: ${th('colors.background')};
  }

  &:last-child {
    border-bottom-left-radius: ${th('gridUnit')};
    border-bottom-right-radius: ${th('gridUnit')};
  }
`;
