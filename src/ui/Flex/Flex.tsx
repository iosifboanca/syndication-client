import {
  color,
  space,
  layout,
  flexbox,
  position,
  ColorProps,
  SpaceProps,
  LayoutProps,
  FlexboxProps,
  PositionProps,
} from 'styled-system';
import styled, { css } from 'styled-components';

export interface FlexProps
  extends LayoutProps,
    ColorProps,
    SpaceProps,
    FlexboxProps,
    PositionProps {
  inline?: boolean;
  vertical?: boolean;
}

const verticalFn = ({ vertical }: FlexProps) => {
  if (vertical) {
    return css`
      align-items: flex-start;
      flex-direction: column;
      justify-content: flex-start;
    `;
  }

  return css`
    align-items: center;
    flex-direction: row;
    justify-content: center;
  `;
};

export const Flex = styled.div<FlexProps>`
  display: ${({ inline }) => (inline ? 'inline-flex' : 'flex')};
  box-sizing: border-box;
  width: 100%;

  ${verticalFn};
  ${color};
  ${space};
  ${layout};
  ${flexbox};
  ${position};
`;
