import React from 'react';
import { FlexboxProps, LayoutProps, SpaceProps } from 'styled-system';

import { Separator as Root } from './Separator.styles';

type SeparatorDirection = 'vertical' | 'horizontal';
type SeparatorFraction = number | 'auto';

export interface SeparatorProps extends FlexboxProps, LayoutProps, SpaceProps {
  color?: string;
  fraction?: SeparatorFraction;
  direction?: SeparatorDirection;
}

export const Separator: React.FC<SeparatorProps> = props => <Root {...props} />;

Separator.defaultProps = {
  fraction: 100,
  direction: 'vertical',
  color: 'colors.furniture',
};
