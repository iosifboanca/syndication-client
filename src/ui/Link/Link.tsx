import React from 'react';
import styled from 'styled-components';
import { space, SpaceProps } from 'styled-system';
import { Link as RouterLink, LinkProps } from 'react-router-dom';

import { semibold } from '../Typography';
import { lighten, darken, th } from '../Theme';

interface Props extends LinkProps, SpaceProps {}

export const Link: React.FunctionComponent<Props> = ({ to, children, ...rest }) => {
  return (
    <StyledLink to={to} {...rest}>
      {children}
    </StyledLink>
  );
};

// #region styles
const StyledLink = styled(RouterLink)<Props>`
  color: ${th('colors.actionSecondary')};
  font-size: ${th('fontSizes.buttonSmall')};
  line-height: ${th('fontSizes.lineHeight')};

  &&:hover {
    color: ${lighten('colors.actionSecondary', 20)};
  }

  &:visited {
    color: ${darken('colors.actionSecondary', 20)};
  }

  ${semibold};
  ${space};
`;
// #endregion
