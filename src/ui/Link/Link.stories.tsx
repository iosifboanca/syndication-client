import React from 'react';
import { BrowserRouter } from 'react-router-dom';

import { Link } from './Link';
import { Flex } from '../Flex';

export const Default = () => (
  <BrowserRouter>
    <Link to='/a-different-route' ml={15}>link here</Link>
  </BrowserRouter>
);

export const FlexAsLink = () => (
  <BrowserRouter>
    <Flex ml={10} as={Link} to='/a-oute' justifyContent="flex-start">
      <div>I'm a Flex that works like a link</div>
      <div>twoook</div>
    </Flex>
  </BrowserRouter>
);

export default {
  title: 'Components|Link',
  component: Link,
};
