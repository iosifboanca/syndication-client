import React, { Fragment } from 'react';

import { Flex, Separator, Title } from 'src/ui';

import { HindawiLogo } from './Logo';
import { Header as Root } from './Header.styles';

export interface HeaderProps {
  title?: string;
  onClick?(): void;
}

export const Header: React.FunctionComponent<HeaderProps> = ({ children, title, onClick }) => {
  return (
    <Root justifyContent='space-between'>
      <Flex justifyContent='flex-start' flex={0} pl={5}>
        <HindawiLogo onClick={onClick} />
        {title && (
          <Fragment>
            <Separator height={6} mx={3} fraction={150} />
            <Title>{title}</Title>
          </Fragment>
        )}
      </Flex>
      {children}
    </Root>
  );
};
