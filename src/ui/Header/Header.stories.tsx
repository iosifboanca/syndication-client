import React from 'react';
import { action } from '@storybook/addon-actions';

import { Flex } from '../';
import { Header } from './Header';

export const Default = () => <Header onClick={action('Clicking the logo')} />;

export const WithTitle = () => (
  <Header onClick={action('Clicking the logo')} title='Section Title' />
);

export const WithChildren = () => (
  <Header onClick={action('Clicking the logo')} title='Section Title'>
    <Flex mr={2} justifyContent='flex-end'>
      I am a child
    </Flex>
  </Header>
);

export default {
  title: 'Components|Header',
  component: Header,
};
