import styled from 'styled-components';

import { th } from '../Theme';
import { Flex } from '../Flex';
import { HeaderProps } from './Header';

export const Header = styled(Flex)<HeaderProps>`
  box-shadow: 0px 2px 3px rgba(25, 102, 141, 0.188406);
  background-color: ${th('colors.white')};
  height: calc(${th('gridUnit')} * 15);
  min-height: calc(${th('gridUnit')} * 15);
`;
