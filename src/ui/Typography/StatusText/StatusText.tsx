import React, { useMemo } from 'react';
import styled from 'styled-components';

import { Text, TextProps, TextTypes } from '..';

export interface StatusTextProps extends TextProps {
  status: any;
  title?: string;
  infoStatuses?: string[];
  warningStatuses?: string[];
}

export const StatusText: React.FunctionComponent<StatusTextProps> = ({
  status,
  infoStatuses,
  warningStatuses,
  children,
  ...rest
}) => {
  const variant: TextTypes = useMemo(() => {
    if (warningStatuses && warningStatuses.includes(status)) {
      return 'warning';
    }

    if (infoStatuses && infoStatuses.includes(status)) {
      return 'info';
    }

    return 'success';
  }, [status, infoStatuses, warningStatuses]);

  return (
    <SText variant={variant} {...rest}>
      {children}
    </SText>
  );
};

// #region styles
const SText = styled(Text)`
  font-weight: bold;
`;
// #endregion
