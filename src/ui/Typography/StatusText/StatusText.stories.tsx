import React from 'react';
import { select } from '@storybook/addon-knobs';

import { StatusText } from './StatusText';

export const Default = () => (
  <StatusText
    infoStatuses={['draft']}
    warningStatuses={['error']}
    status={select('Status', ['draft', 'pending', 'error'], 'pending')}
  >
    STATUS
  </StatusText>
);

export default {
  title: 'Typography|StatusText',
  component: StatusText,
};
