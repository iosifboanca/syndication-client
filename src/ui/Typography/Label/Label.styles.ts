import styled, { css } from 'styled-components';
import { space, flexbox } from 'styled-system';

import { th } from '../../Theme';
import { LabelProps } from './Label';
import * as fontTypes from '../fontTypes';

const labelColor = ({ variant }: LabelProps) => {
  switch (variant) {
    case 'success':
      return css`
        color: ${th('colors.actionPrimary')};
      `;
    case 'info':
      return css`
        color: ${th('colors.info')};
      `;
    case 'warning':
      return css`
        color: ${th('colors.warning')};
      `;
    case 'regular':
    default:
      return css`
        color: ${th('colors.textPrimary')};
      `;
  }
};

export const Label = styled.label<LabelProps>`
  font-size: ${th('fontSizes.labelRegular')};
  line-height: ${th('fontSizes.lineHeight')};

  ${labelColor};
  ${fontTypes.bold};
  ${space};
  ${flexbox};

  &::after {
    content: ${({ required }) => (required ? '"*"' : '')};
    color: ${th('colors.warning')};
  }
`;
