import React from 'react';
import { SpaceProps, FlexboxProps } from 'styled-system';

import { Label as Root } from './Label.styles';

type LabelTypes = 'regular' | 'success' | 'info' | 'warning';

export interface LabelProps extends FlexboxProps, SpaceProps {
  children?: React.ReactNode;
  variant?: LabelTypes;
  required?: boolean;
  htmlFor?: string;
}

export const Label: React.FunctionComponent<LabelProps> = ({ children, ...rest }) => (
  <Root {...rest}>{children}</Root>
);
