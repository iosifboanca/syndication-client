import React from 'react';
import { SpaceProps, TypographyProps } from 'styled-system';

import { Text as Root } from './Text.styles';
import { FormFieldStatus } from '../../FormElements/CommonTypes';

export type TextTypes = FormFieldStatus | 'primary' | 'secondary' | 'success' | 'warning' | 'info';

export type TextSizes = 'small' | 'normal';
export type TextWordBreak = 'normal' | 'break-all';

export interface TextProps extends SpaceProps, TypographyProps {
  children?: React.ReactNode;
  as?: 'span' | 'p';
  variant?: TextTypes;
  size?: TextSizes;
  dangerouslySetInnerHTML?: any;
  title?: string;
  wordBreak?: TextWordBreak;
}

export const Text: React.FunctionComponent<TextProps> = ({ children, ...rest }) => (
  <Root {...rest}>{children}</Root>
);

Text.defaultProps = {
  as: 'span',
  variant: 'primary',
  size: 'normal',
};
