import styled, { css } from 'styled-components';
import { space, typography } from 'styled-system';

import { th } from '../../Theme';
import { TextProps } from './Text';
import * as fontTypes from '../fontTypes';

const textColor = ({ variant }: TextProps) => {
  switch (variant) {
    case 'secondary':
      return css`
        color: ${th('colors.textSecondary')};
      `;
    case 'success':
      return css`
        color: ${th('colors.actionPrimary')};
      `;
    case 'info':
      return css`
        color: ${th('colors.info')};
      `;
    case 'warning':
      return css`
        color: ${th('colors.warning')};
      `;
    case 'primary':
    default:
      return css`
        color: ${th('colors.textPrimary')};
      `;
  }
};

const textSize = ({ size }: TextProps) => {
  switch (size) {
    case 'small':
      return css`
        font-size: ${th('fontSizes.textMessage')};
      `;
    case 'normal':
    default:
      return css`
        font-size: ${th('fontSizes.textRegular')};
      `;
  }
};

const wordBreak = ({ wordBreak = 'normal' }: TextProps) => css`
  word-break: ${wordBreak};
`;

export const Text = styled.span<TextProps>`
  line-height: ${th('fontSizes.lineHeight')};

  ${textSize};
  ${textColor};
  ${fontTypes.regular};
  ${wordBreak};

  ${space};
  ${typography};
`;
