import { capitalize } from 'lodash';
import styled, { css } from 'styled-components';
import { space, typography } from 'styled-system';

import { th } from '../../Theme';
import { TitleProps } from './Title';
import * as fontTypes from '../fontTypes';

const fontSize = ({ variant }: TitleProps) => {
  return css`
    font-size: ${th(`fontSizes.title${capitalize(variant)}`)};
  `;
};

const ellipsis = ({ ellipsis }: TitleProps) => {
  if (ellipsis) {
    return css`
      white-space: nowrap;
      text-overflow: ellipsis;
      overflow: hidden;
    `;
  }
};

const setColor = ({ color }: TitleProps) => {
  switch (color) {
    case 'light':
      return css`
        color: ${th('colors.textPrimary')};
      `;
    case 'dark':
      return css`
        color: ${th('colors.white')};
      `;
    default:
      return css``;
  }
};

export const Title = styled.h1<TitleProps>`
  line-height: 1.3;
  margin-block-end: 0;
  margin-block-start: 0;
  white-space: ${({ whiteSpace }) => whiteSpace};
  text-transform: ${({ upper }: TitleProps) => (upper ? 'uppercase' : 'none')};

  ${ellipsis};
  ${fontSize};
  ${fontTypes.bold};
  ${space};
  ${typography};
  ${setColor};
`;
