import React from 'react';
import { SpaceProps, TypographyProps } from 'styled-system';

import { Headings, Titles, Color } from './TitleTypes';

import { Title as Root } from './Title.styles';

type WhiteSpace = 'normal' | 'nowrap' | 'initial';

export interface TitleProps extends SpaceProps, TypographyProps {
  as?: Headings;
  children?: React.ReactNode;
  upper?: boolean;
  ellipsis?: boolean;
  variant?: Titles;
  color?: Color;
  whiteSpace?: WhiteSpace;
  onClick?(e: React.MouseEvent<any>): void;
}

export const Title: React.FunctionComponent<TitleProps> = ({ children, ...rest }) => {
  return <Root {...rest}>{children}</Root>;
};

Title.defaultProps = {
  as: 'h1',
  upper: false,
  variant: 'primary',
  color: 'light',
  whiteSpace: 'nowrap',
};
