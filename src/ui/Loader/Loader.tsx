import React from "react";
import { SpaceProps } from "styled-system";

import { Loader as Root } from "./Loader.styles";

export interface LoaderProps extends SpaceProps {
  size?: number;
}

export const Loader: React.FunctionComponent<LoaderProps> = props => (
  <Root aria-label="loader" {...props} />
);

Loader.defaultProps = {
  size: 3
};
