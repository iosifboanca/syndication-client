import React from 'react';
import { select } from '@storybook/addon-knobs';

import { DateParser } from './DateParser';

export const Default = () => (
  <DateParser
    date={new Date()}
    locale={select('Locale', ['en-US', 'en-GB', 'ro', 'de-AT', 'fr', 'hi', 'zh-Hans-CN'], 'en-US')}
  >
    {date => <span>{date}</span>}
  </DateParser>
);

export const ShowMinutes = () => (
  <DateParser
    date={new Date()}
    showMinutes
    locale={select('Locale', ['en-US', 'en-GB', 'ro', 'de-AT', 'fr', 'hi', 'zh-Hans-CN'], 'en-US')}
  >
    {date => <span>{date}</span>}
  </DateParser>
);

export const DifferentLocale = () => (
  <DateParser
    showMinutes
    date={new Date()}
    locale={select('Locale', ['en-US', 'en-GB', 'ro', 'de-AT', 'fr', 'hi', 'zh-Hans-CN'], 'en-US')}
  >
    {date => <span>{date}</span>}
  </DateParser>
);

export default {
  component: DateParser,
  title: 'Components|DateParser',
};
