import React, { Fragment, useMemo } from 'react';

interface Props {
  date?: Date;
  locale?: string;
  noDateLabel?: string;
  showMinutes?: boolean;
  children(parsedDate: string): JSX.Element;
}

const options = {
  year: 'numeric',
  month: '2-digit',
  day: '2-digit',
};

const withTime = {
  ...options,
  hour: '2-digit',
  minute: '2-digit',
};

export const DateParser: React.FC<Props> = ({
  date,
  showMinutes,
  locale = 'en-GB',
  noDateLabel = 'No date',
  children,
}) => {
  const parsedDate = useMemo(() => {
    if (!date) return noDateLabel;

    return date.toLocaleDateString(locale, showMinutes ? withTime : options);
  }, [locale, date, showMinutes, noDateLabel]);

  return <Fragment>{children(parsedDate)}</Fragment>;
};
