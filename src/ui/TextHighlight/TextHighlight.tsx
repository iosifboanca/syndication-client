import React, { useContext, useMemo, useCallback } from 'react';
import { get } from 'lodash';
import styled, { ThemeContext } from 'styled-components';

import { Text } from '../Typography';

interface Props {
  children?: string;
  keywords: string[];
  highlightColor?: string;
}

export const TextHighlight: React.FC<Props> = ({
  children,
  keywords,
  highlightColor = 'actionPrimary',
}) => {
  const theme = useContext(ThemeContext);

  const regex = useMemo(() => {
    const dynamicRegexString = keywords.reduce(
      (acc, el, index) => `${acc}${el}${index === keywords.length - 1 ? '' : '|'}`,
      '',
    );
    return new RegExp(dynamicRegexString, 'g');
  }, [keywords]);

  const color = get(theme.colors, highlightColor, highlightColor);
  const mapper = useCallback((match: string) => `<span style="color: ${color}">${match}</span>`, [
    color,
  ]);

  if (typeof children !== 'string')
    throw new Error(
      'TextHighlight component has no children or the children are not the required type (string).',
    );

  const parsed = children.toString().replace(regex, mapper);

  return <CustomText dangerouslySetInnerHTML={{ __html: parsed }} />;
};

const CustomText = styled(Text)`
  word-break: break-all;
`;
