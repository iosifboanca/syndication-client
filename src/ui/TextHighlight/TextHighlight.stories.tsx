import * as React from 'react';
import { array, text, select } from '@storybook/addon-knobs';

import { TextHighlight } from './TextHighlight';

export const Default = () => <TextHighlight keywords={['hi', 'ho']}>abc hi def ho</TextHighlight>;

export const ChangeHighlightColor = () => (
  <TextHighlight
    keywords={['hi', 'ho']}
    highlightColor={select(
      'Highlight color',
      ['red', 'green', 'warning', 'statusInvite', 'statusApproved'],
      'red',
    )}
  >
    abc hi def ho
  </TextHighlight>
);

export const ChangeKeywords = () => (
  <TextHighlight keywords={array('Keywords', ['hi'])}>
    {text('Text', 'abc hi def ho')}
  </TextHighlight>
);

export default {
  title: 'Components|TextHighlight',
  component: TextHighlight,
};
