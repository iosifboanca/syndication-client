import React from 'react';
import { SpaceProps, LayoutProps, FlexboxProps } from 'styled-system';

import { Button as Root } from './Button.styles';
import { Loader } from '../Loader';

type ButtonSizes = 'large' | 'medium' | 'small';
type ButtonVariants = 'primary' | 'secondary' | 'outline';

export interface ButtonProps extends SpaceProps, LayoutProps, FlexboxProps {
  type?: 'button' | 'submit' | 'reset' | undefined;
  loading?: boolean;
  disabled?: boolean;
  size?: ButtonSizes;
  onClick?(e: any): void;
  variant?: ButtonVariants;
  children?: React.ReactNode;
}

export const Button: React.FunctionComponent<ButtonProps> = ({
  children,
  onClick,
  disabled,
  loading,
  ...rest
}) => {
  return (
    <Root disabled={disabled || loading} onClick={onClick} {...rest}>
      {!disabled && loading ? <Loader size={4} /> : children}
    </Root>
  );
};

Button.defaultProps = {
  size: 'large',
  loading: false,
  variant: 'primary',
  disabled: false,
};
