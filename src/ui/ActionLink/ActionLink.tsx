import React from 'react';
import { FlexboxProps, LayoutProps, SpaceProps } from 'styled-system';

import { ActionLink as Root } from './ActionLink.styles';

type ActionType = 'action' | 'link';

export interface ActionLinkProps extends FlexboxProps, LayoutProps, SpaceProps {
  link?: string;
  error?: boolean;
  type?: ActionType;
  children?: React.ReactNode;
  onClick?(e: React.MouseEvent<HTMLElement>): void;
}

export const ActionLink: React.FC<ActionLinkProps> = ({ children, link, onClick, ...rest }) => {
  return (
    <Root href={link} onClick={onClick} {...rest}>
      {children}
    </Root>
  );
};
