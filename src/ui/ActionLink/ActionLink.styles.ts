import { space } from 'styled-system';
import styled, { css } from 'styled-components';

import { ActionLinkProps } from './ActionLink';
import { th, darken, lighten } from '../Theme';
import { semibold, bold, regular } from '../Typography/fontTypes';

const linkType = ({ type }: ActionLinkProps) => {
  switch (type) {
    case 'action':
      return css`
        ${bold}
      `;
    case 'link':
      return css`
        ${semibold}
      `;
    default:
      return css``;
  }
};

export const ActionLink = styled.a<ActionLinkProps>`
  color: ${({ error }) => (error ? th('colors.warning') : th('colors.actionSecondary'))};
  line-height: 18px;
  font-size: 13px;

  ${linkType};
  ${regular};
  ${space};

  &:hover {
    color: ${({ error }) =>
      error ? darken('colors.warning', 20) : lighten('colors.actionSecondary', 10)};
    cursor: pointer;
  }
`;
