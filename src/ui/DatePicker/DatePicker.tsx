import React, { useMemo, useState, useEffect, useCallback, useRef } from 'react';
import styled, { css } from 'styled-components';
import Calendar, { OnChangeDateCallback } from 'react-calendar';

import { th } from '../Theme';
import { Flex } from '../Flex';
import { Icon } from '../Icon';
import { regular, regularItalic, bold } from '../Typography';

interface Props {
  locale?: string;
  placeholder?: string;
  selectRange?: boolean;
  selectDate: OnChangeDateCallback;
  value?: Date | Date[] | undefined;
}

const parseOptions = {
  year: 'numeric',
  month: '2-digit',
  day: '2-digit',
};

export const DatePicker: React.FunctionComponent<Props> = ({
  value,
  selectDate,
  selectRange,
  placeholder,
  locale = 'en-GB',
}) => {
  const ref: React.MutableRefObject<any> = useRef(null);
  const containerRef: React.MutableRefObject<any> = useRef(null);

  const [opened, setOpen] = useState(false);
  const [selectedDate, setValue] = useState(value);

  useEffect(() => {
    setValue(value);
  }, [value]);

  const openPicker = useCallback(() => setOpen(true), []);
  const closePicker = useCallback(() => setOpen(false), []);

  const handleClickOutside = useCallback(
    (event: MouseEvent): void => {
      if (!opened) return;

      if (
        ref.current &&
        !ref.current.contains(event.target) &&
        containerRef.current &&
        !containerRef.current.contains(event.target)
      ) {
        closePicker();
      }
    },
    [opened, closePicker],
  );

  const handleEscape = useCallback(
    (event: KeyboardEvent) => {
      if (event.key === 'Escape' && opened) {
        closePicker();
      }
    },
    [opened, closePicker],
  );

  const selectValue = useCallback(
    (values: Date | Date[]) => {
      setValue(values);
      selectDate(values);
      closePicker();
    },
    [closePicker, selectDate],
  );

  useEffect(() => {
    document.addEventListener('click', handleClickOutside);
    document.addEventListener('keydown', handleEscape);

    return () => {
      document.removeEventListener('click', handleClickOutside);
      document.removeEventListener('keydown', handleEscape);
    };
  }, [handleClickOutside, handleEscape]);

  const parsedValue = useMemo(() => {
    if (!selectedDate) {
      return placeholder || 'Please select a date...';
    }

    if (Array.isArray(selectedDate)) {
      return `${selectedDate[0].toLocaleDateString(
        locale,
        parseOptions,
      )} - ${selectedDate[1].toLocaleDateString(locale, parseOptions)}`;
    }
    return selectedDate.toLocaleDateString(locale, parseOptions);
  }, [locale, selectedDate, placeholder]);

  return (
    <Root title={parsedValue}>
      <Opener onClick={openPicker} ref={ref} selected={1}>
        {parsedValue}
      </Opener>
      <MenuIcon color='colors.textPrimary' name={'calendar'} />
      {opened && (
        <PickerContainer ref={containerRef}>
          <StyledCalendar selectRange={selectRange} onChange={selectValue} value={selectedDate} />
        </PickerContainer>
      )}
    </Root>
  );
};

interface Opener {
  selected: any;
}

// #region styles
const OPTION_HEIGHT = 8;

const Root = styled(Flex)`
  position: relative;
  table-layout: fixed;
  position: relative;
`;

const MenuIcon = styled(Icon)`
  position: absolute;
  top: calc(${th('gridUnit')} * 2);
  right: calc(${th('gridUnit')} * 2);
`;

const optionStyle = () => css`
  cursor: pointer;
  height: calc(${th('gridUnit')} * ${OPTION_HEIGHT});
  padding-left: calc(${th('gridUnit')} * 2);
  width: 100%;
`;

const Opener = styled.div<Opener>`
  align-items: center;
  display: flex;
  border: 1px solid ${th('colors.furniture')};
  border-radius: ${th('gridUnit')};
  font-size: ${th('fontSizes.textRegular')};

  ${optionStyle};

  ${({ selected }) =>
    selected
      ? css`
          color: ${th('colors.textPrimary')};
          ${regular}
        `
      : css`
          color: ${th('colors.textSecondary')};
          ${regularItalic}
        `};

  &:hover {
    border-color: ${th('colors.textPrimary')};
  }
`;

const PickerContainer = styled.div`
  border-radius: ${th('gridUnit')};

  position: absolute;
  top: calc(${th('gridUnit')} * 10);
  width: 100%;
  z-index: 2;

  ${regular};
`;

const StyledCalendar = styled(Calendar)`
  &.react-calendar {
    width: 228px;
  }

  /* Header */
  .react-calendar__navigation {
    background-color: ${th('colors.background')};
    height: calc(${th('gridUnit')} * 10);
    margin: 0;
    padding: 0;
  }

  .react-calendar__navigation__prev2-button,
  .react-calendar__navigation__prev-button,
  .react-calendar__navigation__next-button,
  .react-calendar__navigation__next2-button {
    display: flex;
    justify-content: center;
    font-size: 20px;

    padding: 0;
    width: calc(${th('gridUnit')} * 6);
    min-width: calc(${th('gridUnit')} * 6);
  }

  .react-calendar__navigation__label {
    color: ${th('colors.textPrimary')};
    font-size: ${th('fontSizes.textRegular')};
    padding: 0;

    ${bold};
  }

  /* Weekdays */
  .react-calendar__month-view__weekdays {
    align-items: center;
    background-color: ${th('colors.background')};
    display: flex;
    color: ${th('colors.textPrimary')};
    height: calc(${th('gridUnit')} * 6);
    text-decoration: none;

    ${regular};

    abbr {
      text-decoration: none;
    }
  }

  .react-calendar__month-view__weekdays__weekday {
    align-items: center;
    display: flex;
    justify-content: center;
    height: calc(${th('gridUnit')} * 6);
  }

  .react-calendar__month-view__days {
    background-color: ${th('colors.white')};
    min-height: calc(${th('gridUnit')} * 45);
    max-height: calc(${th('gridUnit')} * 53);
  }

  /* Days */
  .react-calendar__tile {
    color: ${th('colors.textPrimary')};
    height: calc(${th('gridUnit')} * 8);
    width: calc(${th('gridUnit')} * 8);

    z-index: 1;
  }

  .react-calendar__tile--active {
    border: 1px solid ${th('colors.actionPrimary')};
    background-color: transparent;
  }

  .react-calendar__tile--active:enabled:hover,
  .react-calendar__tile--active:enabled:focus {
    background-color: transparent;
  }

  .react-calendar__month-view__days__day {
    padding: 0;

    height: calc(${th('gridUnit')} * 8);
    width: calc(${th('gridUnit')} * 8);
  }
`;

// #endregion
