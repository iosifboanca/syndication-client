import React from 'react';
import { action } from '@storybook/addon-actions';
import { select } from '@storybook/addon-knobs';

import { DatePicker } from './DatePicker';

export const Default = () => (
  <DatePicker
    selectDate={action('Select date')}
    value={new Date()}
    locale={select('Locale', ['en-US', 'en-GB', 'ro', 'de-AT', 'fr', 'hi', 'zh-Hans-CN'], 'en-US')}
  />
);

export const DateRange = () => (
  <DatePicker
    selectRange
    selectDate={action('Select date range')}
    locale={select('Locale', ['en-US', 'en-GB', 'ro', 'de-AT', 'fr', 'hi', 'zh-Hans-CN'], 'en-US')}
  />
);

export const DateRangeWithPlaceholder = () => (
  <DatePicker
    selectRange
    placeholder='Select syndication period'
    selectDate={action('Select date range')}
    locale={select('Locale', ['en-US', 'en-GB', 'ro', 'de-AT', 'fr', 'hi', 'zh-Hans-CN'], 'en-US')}
  />
);

export default {
  title: 'Components|DatePicker',
  component: DatePicker,
};
