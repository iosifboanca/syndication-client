import styled, { css } from 'styled-components';
import { space, position } from 'styled-system';

import { th } from '../Theme';
import { StyledIconProps } from './Icon';

const iconSize = ({ size }: StyledIconProps) => css`
  width: calc(${th('gridUnit')} * ${size});
  height: calc(${th('gridUnit')} * ${size});
`;

export const Icon = styled.svg<StyledIconProps>`
  fill: ${({ color }: StyledIconProps) => th(color)};

  ${iconSize};
  ${space};
  ${position};
`;
