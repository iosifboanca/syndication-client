export interface ContactDTO {
  id?: string;
  name?: string;
  email: string;
}

export class Contact {
  id: string = '';
  name: string = '';
  email: string = '';

  constructor(data?: ContactDTO) {
    Object.assign(this, data);
  }

  get dto(): ContactDTO {
    return { ...this };
  }
}
