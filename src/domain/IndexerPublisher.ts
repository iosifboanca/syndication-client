import { TransferProtocol } from 'src/domain';
import { ContactDTO, Contact } from './Contact';

export interface FtpOptionsDTO {
  type: string;
  hostname: string;
  username: string;
  password: string;
  pathPattern: string;
}

export class FtpOptions {
  type: TransferProtocol = TransferProtocol.ftp;
  hostname: string = '';
  username: string = '';
  password: string = '';
  pathPattern: string = '';

  constructor(data: FtpOptionsDTO | undefined) {
    if (data) {
      Object.assign(this, data);
    }
  }

  get dto() {
    return { ...this };
  }
}

export interface HttpOptionsDTO {
  type: string;
  endpoint: string;
  accessToken: string;
}

export class HttpOptions {
  type: TransferProtocol = TransferProtocol.http;
  endpoint: string = '';
  accessToken: string = '';

  constructor(data: HttpOptionsDTO | undefined) {
    if (data) {
      Object.assign(this, data);
    }
  }

  get dto() {
    return { ...this };
  }
}

export interface IndexerPublisherDTO {
  indexerId: string;
  publisherId: string;
  contacts: ContactDTO[];
  ftpTransferOptions: FtpOptionsDTO;
  httpTransferOptions: HttpOptionsDTO;
}

export class IndexerPublisher {
  indexerId: string;
  publisherId: string;
  ftpTransferOptions: FtpOptions;
  httpTransferOptions: HttpOptions;
  contacts: Contact[];

  constructor({ contacts = [], ...data }: Partial<IndexerPublisherDTO>) {
    Object.assign(this, {
      ...data,
      contacts: contacts.map(c => new Contact(c)),
      ftpTransferOptions: new FtpOptions(data.ftpTransferOptions),
      httpTransferOptions: new HttpOptions(data.httpTransferOptions),
    });
  }

  get dto() {
    return {
      ...this,
      contacts: this.contacts ? this.contacts.map(c => c.dto) : [],
      ftpTransferOptions: this.ftpTransferOptions.dto,
      httpTransferOptions: this.httpTransferOptions.dto,
    };
  }
}
