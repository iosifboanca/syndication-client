export interface DatabaseDTO {
  id: string;
  indexerId?: string;
  name: string;
  observations: string;
  acceptanceCriteria: string;
}

export class Database {
  id: string;
  indexerId: string = '';
  name: string = '';
  observations: string = '';
  acceptanceCriteria: string = '';

  constructor(data: Partial<DatabaseDTO>) {
    Object.assign(this, data);
  }

  get dto() {
    return { ...this };
  }
}
