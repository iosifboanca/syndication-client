// tokens used for transfer options
export enum PathTokens {
  journalVolume = '{vol}',
  publisher = '{publisher}',
  journalCode = '{journalCode}',
  publicationYear = '{publicationYear}',
  fileName = '{fileName}',
  legacyJournalCode = '{legacyJournalCode}',
  repecJournalCode = '{journalCodeRepec}',
  syndicationDate = '{yyyy_mm_dd}',
  articleCustomId = '{customId}',
  metaSplit = '{folderSplit:meta}',
  pdfSplit = '{folderSplit:pdf}',
  specialIssueSplit = '{folderSplit:spIssue}',
  journalSN = '{journalSN}',
  journal_Name = '{journal_Name}',
  journalName = '{journalName}',
}

export const FTPTokens = {
  [PathTokens.publisher]: 'Publisher name',
  [PathTokens.journalCode]: 'Journal code',
  [PathTokens.legacyJournalCode]: 'Legacy journal code for Hindawi journals',
  [PathTokens.repecJournalCode]: 'Journal code in paths on RePEc FTP',
  [PathTokens.journalVolume]: 'Journal volume',
  [PathTokens.syndicationDate]: 'Syndication date',
  [PathTokens.fileName]: 'Original name of syndicated file',
  [PathTokens.articleCustomId]: 'Article custom ID',
  [PathTokens.specialIssueSplit]: 'Special Issue Custom ID vs Regular',
  [PathTokens.journalSN]: 'Journal serial number',
  [PathTokens.journal_Name]: 'Journal name (for Library of Congress)',
};

export const ZipTokens = {
  [PathTokens.journalCode]: 'Journal code',
  [PathTokens.publicationYear]: 'Article publication year',
  [PathTokens.fileName]: 'Original name of syndicated file',
  [PathTokens.articleCustomId]: 'Article custom ID',
  [PathTokens.metaSplit]: 'pdf-files vs xml-files',
  [PathTokens.pdfSplit]: 'metadata vs xml-files',
  [PathTokens.journalName]: 'Journal name',
};

// File type transfer options
export enum FileType {
  abs = 'abs',
  ref = 'ref',
  fullXml = 'fullXml',
  images = 'images',
  pdf = 'pdf',
  supplXml = 'supplXml',
  supplMaterials = 'supplMaterials',
  rdf = 'rdf',
  bbl = 'bbl',
  crossrefXml = 'crossrefXml',
  pmcPackage = 'pmcPackage',
}

export const FileLabels: Record<string, string> = {
  [FileType.abs]: 'ABS XML',
  [FileType.bbl]: 'BBL',
  [FileType.crossrefXml]: 'Crossref XML',
  [FileType.fullXml]: 'Full XML',
  [FileType.images]: 'Images',
  [FileType.pdf]: 'PDF',
  [FileType.pmcPackage]: 'PMC Package',
  [FileType.rdf]: 'RDF',
  [FileType.ref]: 'REF XML',
  [FileType.supplMaterials]: 'Supplementary Materials',
  [FileType.supplXml]: 'Supplementary XML',
};

interface TransferFileOption {
  value: FileType;
  label: string;
}

export const TransferFileOptions: TransferFileOption[] = [
  { value: FileType.fullXml, label: FileLabels[FileType.fullXml] },
  { value: FileType.images, label: FileLabels[FileType.images] },
  { value: FileType.supplMaterials, label: FileLabels[FileType.supplMaterials] },
  { value: FileType.abs, label: FileLabels[FileType.abs] },
  { value: FileType.pdf, label: FileLabels[FileType.pdf] },
  { value: FileType.supplXml, label: FileLabels[FileType.supplXml] },
  { value: FileType.ref, label: FileLabels[FileType.ref] },
  { value: FileType.rdf, label: FileLabels[FileType.rdf] },
  { value: FileType.pmcPackage, label: FileLabels[FileType.pmcPackage] },
  { value: FileType.bbl, label: FileLabels[FileType.bbl] },
  { value: FileType.crossrefXml, label: FileLabels[FileType.crossrefXml] },
];

// Zip transfer options
interface ZipOption {
  id: string;
  value: ZipType;
  label: string;
}

export enum ZipType {
  noZip = 'noZip',
  fullZip = 'fullZip',
  journalZip = 'journalZip',
  articleZip = 'articleZip',
}

export const ZipLabels = {
  [ZipType.noZip]: 'No zip',
  [ZipType.fullZip]: 'Full zip',
  [ZipType.journalZip]: 'Journal zip',
  [ZipType.articleZip]: 'Article zip',
};

export const ZipTypeOptions: ZipOption[] = [
  {
    id: ZipType.noZip,
    value: ZipType.noZip,
    label: ZipLabels[ZipType.noZip],
  },
  {
    id: ZipType.fullZip,
    value: ZipType.fullZip,
    label: ZipLabels[ZipType.fullZip],
  },
  {
    id: ZipType.journalZip,
    value: ZipType.journalZip,
    label: ZipLabels[ZipType.journalZip],
  },
  {
    id: ZipType.articleZip,
    value: ZipType.articleZip,
    label: ZipLabels[ZipType.articleZip],
  },
];

// Syndication Type
export enum SyndicationType {
  files = 'files',
  manifest = 'manifest',
}

// Syndication Options
export enum SyndicationOption {
  files = 'files',
  manifest = 'manifest',
}

export const syndicationOptions = [
  {
    id: SyndicationType.files,
    value: SyndicationType.files,
    label: SyndicationType.files.toUpperCase(),
  },
  {
    id: SyndicationType.manifest,
    value: SyndicationType.manifest,
    label: SyndicationType.manifest.toUpperCase(),
  },
];

// Transfer protocol
export enum TransferProtocol {
  ftp = 'ftp',
  http = 'http',
}

export const transferOptions = [
  {
    id: TransferProtocol.ftp,
    value: TransferProtocol.ftp,
    label: TransferProtocol.ftp.toUpperCase(),
  },
  {
    id: TransferProtocol.http,
    value: TransferProtocol.http,
    label: TransferProtocol.http.toUpperCase(),
  },
];

// Syndication Schedule
export enum SyndicationSchedule {
  daily = 'daily',
  weekly = 'weekly',
  monthly = 'monthly',
}

export const scheduleOptions = [
  {
    id: SyndicationSchedule.daily,
    value: SyndicationSchedule.daily,
    label: SyndicationSchedule.daily.toUpperCase(),
  },
  {
    id: SyndicationSchedule.weekly,
    value: SyndicationSchedule.weekly,
    label: SyndicationSchedule.weekly.toUpperCase(),
  },
  {
    id: SyndicationSchedule.monthly,
    value: SyndicationSchedule.monthly,
    label: SyndicationSchedule.monthly.toUpperCase(),
  },
];
