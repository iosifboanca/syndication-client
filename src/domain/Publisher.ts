export interface PublisherDTO {
  id: string;
  name: string;
}

export class Publisher implements PublisherDTO {
  id: string = '';
  name: string = '';

  constructor(data: PublisherDTO) {
    Object.assign(this, data);
  }

  get dto(): PublisherDTO {
    return { ...this };
  }
}
