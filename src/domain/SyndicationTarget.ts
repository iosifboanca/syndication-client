export enum SyndicationTargetStatus {
  pending = 'pending',
  pickedUp = 'pickedUp',
  running = 'running',
  failed = 'failed',
  success = 'success',
  canceled = 'canceled',
}

export interface SyndicationTargetDTO {
  id: string;
  startDate?: Date;
  endDate?: Date;
  status: SyndicationTargetStatus;
  metadataRaw: string;
  output?: SyndicationTargetOutputDTO[];
}

export enum OutputEntryLevel {
  info = 'info',
  warn = 'warn',
  error = 'error',
}

export interface SyndicationTargetOutputDTO {
  level: OutputEntryLevel;
  timestamp: string;
  message: string[];
}

export class SyndicationTargetOutput {
  level: OutputEntryLevel;
  timestamp: Date;
  message: string[];

  constructor(data: SyndicationTargetOutputDTO) {
    Object.assign(this, {
      ...data,
      timestamp: new Date(data.timestamp),
    });
  }
}

export class SyndicationTargetMetadata {
  errors: Record<string, string> = {};
  resolved: Record<string, string> = {};

  constructor(metadata = '{}') {
    Object.assign(this, JSON.parse(metadata));
  }
}

export class SyndicationTarget {
  id: string;
  endDate?: Date;
  startDate?: Date;
  status: SyndicationTargetStatus;
  output: SyndicationTargetOutput[];
  metadata: SyndicationTargetMetadata;

  constructor({ output = [], metadataRaw, ...data }: Partial<SyndicationTargetDTO>) {
    Object.assign(this, data, {
      startDate: data.startDate ? new Date(data.startDate) : undefined,
      endDate: data.endDate ? new Date(data.endDate) : undefined,
      output: output.map(o => new SyndicationTargetOutput(o)),
      metadata: new SyndicationTargetMetadata(metadataRaw),
    });
  }

  get dto() {
    return {
      ...this,
    };
  }
}
