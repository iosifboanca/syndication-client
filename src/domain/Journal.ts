export type JournalDatabaseRecord = Record<string, JournalDatabase>;
export type JournalRecord = Record<string, Journal>;

export enum JournalDatabaseStatus {
  underReview = 'underReview',
  rejected = 'rejected',
  rejectedFinal = 'rejectedFinal',
  covered = 'covered',
  reviewHold = 'reviewHold',
  ceasedCoverage = 'ceasedCoverage',
}

export enum JournalDBActive {
  active = 'active',
  inactive = 'inactive',
}

export interface JournalDatabaseDTO {
  databaseId: string;
  journalId: string;
  acceptanceText?: string;
  active?: boolean;
  status?: JournalDatabaseStatus;
  acceptanceDate?: Date;
  coverageDate?: Date;
}

export class JournalDatabase {
  databaseId: string;
  journalId: string;
  active: boolean;
  acceptanceText: string = '';
  status: JournalDatabaseStatus = JournalDatabaseStatus.underReview;
  acceptanceDate: Date;
  coverageDate: Date;

  constructor(data: Partial<JournalDatabaseDTO>) {
    Object.assign(this, {
      ...data,
      acceptanceText: data.acceptanceText ? data.acceptanceText : '',
      acceptanceDate: data.acceptanceDate ? new Date(data.acceptanceDate) : new Date(),
      coverageDate: data.coverageDate ? new Date(data.coverageDate) : new Date(),
    });
  }

  get dto() {
    return {
      ...this,
      acceptanceDate: this.acceptanceDate.toISOString(),
      coverageDate: this.coverageDate.toISOString(),
    };
  }
}

export interface JournalCustomFieldsDTO {
  legacyJournalCode?: string;
  journalCodeRepec?: string;
  journalSN?: string;
}

export interface JournalDTO {
  id: string;
  publisherId: string;
  name: string;
  code: string;
  issn?: string;
  printIssn?: string;
  active: boolean;
  customFields: JournalCustomFieldsDTO;
  databasePairs: JournalDatabaseDTO[];
}

class JournalCustomFields {
  legacyJournalCode: string = '';
  journalCodeRepec: string = '';
  journalSN: string = '';

  constructor(data: JournalCustomFieldsDTO) {
    Object.assign(this, {
      legacyJournalCode: data.legacyJournalCode === null ? '' : data.legacyJournalCode,
      journalCodeRepec: data.journalCodeRepec === null ? '' : data.journalCodeRepec,
      journalSN: data.journalSN === null ? '' : data.journalSN,
    });
  }

  get dto() {
    return {
      ...this,
    };
  }
}

export class Journal {
  id: string = '';
  publisherId: string = '';
  name: string = '';
  code: string = '';
  issn?: string = '';
  active: boolean = false;
  printIssn: string;
  databasePairs: JournalDatabase[];
  customFields: JournalCustomFields;

  constructor({ databasePairs = [], customFields = {}, printIssn, ...data }: JournalDTO) {
    Object.assign(this, {
      ...data,
      printIssn: printIssn === null ? '' : printIssn,
      customFields: new JournalCustomFields(customFields),
      databasePairs: databasePairs.map(db => new JournalDatabase(db)),
    });
  }

  get dto() {
    return {
      ...this,
      customFields: this.customFields.dto,
      databasePairs: this.databasePairs.map(db => db.dto),
    };
  }
}
