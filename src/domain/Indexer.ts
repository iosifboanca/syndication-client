import {
  Contact,
  ContactDTO,
  Database,
  DatabaseDTO,
  IndexerPublisher,
  IndexerPublisherDTO,
  ZipType,
  FileType,
  SyndicationOption,
  SyndicationSchedule,
  SyndicationType,
  TransferProtocol,
} from 'src/domain';

export class SyndicationFileOptions {
  files: FileType[];
  zipPath: string = '';
  zipType: ZipType = ZipType.noZip;
  type: SyndicationOption.files = SyndicationOption.files;

  constructor(data: Partial<SyndicationFileOptions> | undefined) {
    if (!data) {
      Object.assign(this);
    } else {
      this.files = data.files || [];
      this.zipPath = data.zipPath || '';
      this.zipType = data.zipType || ZipType.noZip;
    }
  }
}

export class SyndicationManifestOptions {
  type: SyndicationOption.manifest = SyndicationOption.manifest;
}

export interface IndexerDTO {
  id?: string;
  name: string;
  active?: boolean;
  code?: string;
  contact?: ContactDTO;
  owner?: string;
  acceptanceCriteria?: string;
  observations?: string;
  publisherCount?: number;

  databases?: DatabaseDTO[];
  publisherPairs?: IndexerPublisherDTO[];

  syndicationSchedule?: SyndicationSchedule;
  syndicationType?: SyndicationType;
  transferProtocol?: TransferProtocol;
  syndicationFileOptions?: SyndicationFileOptions;
  syndicationManifestOptions?: SyndicationManifestOptions;
}

export class Indexer {
  id: string;
  name: string = '';
  active: boolean = false;
  code: string = '';
  owner: string = '';
  observations: string = '';
  acceptanceCriteria: string = '';
  syndicationType: SyndicationType;
  publisherCount: number = 0;

  contact?: Contact;
  transferProtocol: TransferProtocol;
  syndicationSchedule: SyndicationSchedule;
  syndicationFileOptions: SyndicationFileOptions;

  databases: Database[];
  publisherPairs: IndexerPublisher[];

  constructor({ databases = [], publisherPairs = [], ...data }: IndexerDTO) {
    Object.assign(this, {
      ...data,
      contact: new Contact(data.contact),
      databases: databases.map(db => new Database(db)),
      observations: data.observations ? data.observations : '',
      publisherPairs: publisherPairs.map(pair => new IndexerPublisher(pair)),
      syndicationFileOptions: new SyndicationFileOptions(data.syndicationFileOptions),
    });
  }

  get dto() {
    const { publisherPairs, publisherCount, ...indexer } = this;

    return indexer;
  }
}
