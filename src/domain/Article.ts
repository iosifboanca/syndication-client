import { SyndicationTargetStatus, SyndicationTarget } from './SyndicationTarget';

export interface ArticleDTO {
  id: string;
  journalId: string;
  customId: string;
  specialIssueId: string;
  title: string;
  doi: string;
  publicationDate: Date;
}

export enum ArticleStatus {
  SUCCESS = 'success',
  FAILED = 'failed',
  RESOLVED = 'resolved',
  UNKNOWN = 'unknown',
}

const unknownStatuses = [
  SyndicationTargetStatus.pending,
  SyndicationTargetStatus.pickedUp,
  SyndicationTargetStatus.running,
  SyndicationTargetStatus.canceled,
];

interface FilterOption {
  id: string;
  value: ArticleStatus;
  label: string;
}

export const filterByStatusOptions = [
  { id: 'none', value: undefined, label: 'None' },
  { id: 'success', value: ArticleStatus.SUCCESS, label: 'Success' },
  { id: 'failed', value: ArticleStatus.FAILED, label: 'Failed' },
  { id: 'resolved', value: ArticleStatus.RESOLVED, label: 'Resolved' },
  { id: 'unknown', value: ArticleStatus.UNKNOWN, label: 'Unknown' },
];

export class Article {
  id: string = '';
  doi: string = '';
  title: string = '';
  customId: string = '';
  publicationDate: Date;
  journalId: string = '';
  specialIssueId: string = '';

  constructor(data: ArticleDTO) {
    Object.assign(this, { ...data, publicationDate: new Date(data.publicationDate) });
  }

  status(target?: SyndicationTarget): ArticleStatus {
    if (!target || unknownStatuses.includes(target.status)) {
      return ArticleStatus.UNKNOWN;
    }

    if (target.status === SyndicationTargetStatus.failed) {
      return ArticleStatus.FAILED;
    }

    if (target.metadata.errors[this.id] && !target.metadata.resolved[this.id]) {
      return ArticleStatus.FAILED;
    }

    if (target.metadata.resolved[this.id]) {
      return ArticleStatus.RESOLVED;
    }

    return ArticleStatus.SUCCESS;
  }

  tooltip(target?: SyndicationTarget): string | undefined {
    if (!target) return '';

    const status = this.status(target);

    if (status === ArticleStatus.RESOLVED) {
      return target.metadata.resolved[this.id];
    }

    if (status === ArticleStatus.FAILED) {
      return target.metadata.errors[this.id];
    }

    return '';
  }

  get dto() {
    return { ...this, publicationDate: this.publicationDate.toISOString() };
  }
}
