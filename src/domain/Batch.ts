import { capitalize } from 'lodash';

import {
  Article,
  Indexer,
  Publisher,
  ArticleDTO,
  IndexerDTO,
  PublisherDTO,
  SyndicationTarget,
  SyndicationTargetDTO,
} from 'src/domain';

export enum BatchType {
  auto = 'auto',
  manual = 'manual',
  replica = 'replica',
}

export enum BatchStatus {
  draft = 'draft',
  pending = 'pending',
  commited = 'committed',
  inProgress = 'inProgress',
  success = 'success',
  error = 'error',
}

enum BatchStatusDTO {
  draft = 'draft',
  pending = 'pending',
  commited = 'commited',
  committed = 'commited',
  inProgress = 'inProgress',
  success = 'success',
  error = 'error',
}

export interface CloneBatchInput {
  batchId: string;
  targetDate: string;
}
export interface ResyndicationInput {
  targetDate: string;
  syndicationTargetId: string;
  articleIds: string[];
}

export interface BatchFilters {
  limit?: number;
  offset?: number;
  type?: BatchType;
  indexerId?: string;
  publisherId?: string;
  status?: BatchStatus;
  endTargetDate?: Date;
  startTargetDate?: Date;
}

export const batchTypeOptions = [
  { id: 'none', value: undefined, name: 'None' },
  { id: BatchType.auto, value: BatchType.auto, name: capitalize(BatchType.auto) },
  { id: BatchType.manual, value: BatchType.manual, name: capitalize(BatchType.manual) },
  { id: BatchType.replica, value: BatchType.replica, name: capitalize(BatchType.replica) },
];

export const batchStatusOptions = [
  { id: 'none', value: undefined, name: 'None' },
  { id: BatchStatus.commited, value: BatchStatus.commited, name: capitalize(BatchStatus.commited) },
  { id: BatchStatus.draft, value: BatchStatus.draft, name: capitalize(BatchStatus.draft) },
  { id: BatchStatus.error, value: BatchStatus.error, name: capitalize(BatchStatus.error) },
  {
    id: BatchStatus.inProgress,
    value: BatchStatus.inProgress,
    name: capitalize(BatchStatus.inProgress),
  },
  { id: BatchStatus.pending, value: BatchStatus.pending, name: capitalize(BatchStatus.pending) },
  { id: BatchStatus.success, value: BatchStatus.success, name: capitalize(BatchStatus.success) },
];

export interface BatchDTO {
  id: string;
  indexerId: string;
  publisherId: string;
  targetDate: Date;
  completionDate: Date;
  type: BatchType;
  indexer: IndexerDTO;
  parentId?: string;
  status: BatchStatusDTO;
  publisher: PublisherDTO;
  articles?: ArticleDTO[];
  syndicationTargets: SyndicationTargetDTO[];
}

export class Batch {
  id: string;
  indexerId: string;
  publisherId: string;
  targetDate: Date;
  completionDate: Date;
  type: BatchType;
  status: BatchStatus;
  indexer: Indexer;
  publisher: Publisher;
  parentId: string;
  articles: Article[];
  syndicationTargets: SyndicationTarget[];

  constructor({ articles = [], syndicationTargets = [], ...data }: BatchDTO) {
    Object.assign(this, {
      ...data,
      status: BatchStatus[data.status],
      indexer: new Indexer(data.indexer),
      targetDate: new Date(data.targetDate),
      publisher: new Publisher(data.publisher),
      articles: articles.map(a => new Article(a)),
      syndicationTargets: syndicationTargets.map(t => new SyndicationTarget(t)),
    });
  }

  get dto() {
    return {
      ...this,
      status: BatchStatusDTO[this.status],
      indexer: this.indexer.dto,
      publisher: this.publisher.dto,
      articles: this.articles.map(a => a.dto),
      targetDate: this.targetDate.toISOString(),
      syndicationTargets: this.syndicationTargets.map(s => s.dto),
    };
  }
}
