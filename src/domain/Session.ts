export enum SessionRole {
  ADMIN = 'ADMIN',
  SUPPORT = 'SUPPORT',
  ANONYMOUS = 'ANONYMOUS',
  NONE = 'NONE',
}

export interface SessionDTO {
  name: string;
  email: string;
  roles: SessionRole[];
  token: string;
}

export class Session implements SessionDTO {
  name: string;
  email: string;
  roles: SessionRole[];
  token: string;

  constructor(data: SessionDTO) {
    this.name = data.name;
    this.email = data.email;
    this.roles = data.roles;
    this.token = data.token;
  }

  get dto(): SessionDTO {
    return { ...this };
  }

  get isLoggedIn(): boolean {
    return !!this.email && !!this.token;
  }
}

export const emptySession = new Session({
  name: '',
  email: '',
  roles: [SessionRole.ANONYMOUS],
  token: '',
});
