export * from './Article';
export * from './Batch';
export * from './Publisher';
export * from './Session';
export * from './Database';
export * from './Journal';
export * from './Contact';
export * from './Indexer';
export * from './SyndicationTarget';
export * from './IndexerPublisher';
export * from './TransferOptions';

export interface PaginatedCollection<T> {
  items: T[];
  hasMore: boolean;
}
