import 'styled-components';
import { Theme } from '../src/ui/Theme';

declare global {
  interface Window {
    __REDUX_DEVTOOLS_EXTENSION__: any;
    __REDUX_DEVTOOLS_EXTENSION_COMPOSE__: any;
    env: {
      NODE_ENV: string;
      APP_URL: string;
      GQL_ROOT: string;
      API_ROOT: string;
      KEYCLOAK_URL: string;
      KEYCLOAK_REALM: string;
      KEYCLOAK_CLIENT_ID: string;
      SSO_ENABLED: boolean;
    };
    toggleFeature(feature: string): void;
  }
}

declare interface NodeModule {
  hot?: { accept: (path: string, callback: () => void) => void };
}

declare interface System {
  import<T = any>(module: string): Promise<T>;
}
declare var System: System;

declare const process: any;
declare const require: any;

declare module 'styled-components' {
  export interface DefaultTheme extends Theme {}
}
